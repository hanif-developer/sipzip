<?php 
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Auto Loader
 */

// Set default timezone
date_default_timezone_set('Asia/Jakarta');
// Set constant is where folder located
define('ROOT', dirname(__FILE__) . '/');
define('APP', dirname(__FILE__) . '/app/');
define('ASSET', dirname(__FILE__) . '/asset/');
define('CONFIG', dirname(__FILE__) . '/config/');
define('CORE', dirname(__FILE__) . '/core/');
define('OAUTH', dirname(__FILE__) . '/oauth/');
define('TEMPLATE', dirname(__FILE__) . '/template/');
define('UPLOAD', dirname(__FILE__) . '/upload/');
define('VENDOR', dirname(__FILE__) . '/vendor/'); // Composer
define('COOKIE_EXP', (3600 * 24)); // 24 Jam / 1 Hari
// Set constant INDEX_FILE is the default file name
define('INDEX_FILE', basename(__FILE__));
// Set constant TIME_LOAD is the first time loader
define('TIME_LOAD', microtime(true));
define('MEMORY_LOAD', memory_get_usage());
// // Running Application
error_reporting(0);

// Redirect HTTPS
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'on') {
    $redirect = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header('Location:'.$redirect);
}

// Load Composer Engine
// require_once VENDOR . 'autoload.php';
require_once 'Application.php';
?>