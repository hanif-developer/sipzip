<?php 

return [
    'application' => [
        'project' => 'publish', // Set Default Project (Base Url)
    ],
    'settings' => [ // List Of Setting
        'web' => [
            'title' => 'SIPZIP',
            'author' => 'Hanif Muhammad',
            'description' => 'Sistem Informasi Pembangunan Zona Intergritas Polri',
            'keywords' => 'polres, polri',
            'version' => '2021.09.1.0.0',
            'ic_logo' => '/asset/images/ic_logo.png',
            'ic_maskot' => '/asset/images/ic_maskot.png',
            'ic_presisi' => '/asset/images/ic_presisi.png',
            'bg_login' => '/asset/images/bg_login.jpeg',
            'bg_form_login' => '/asset/images/bg_form_login.png',
        ],
        'upload' => [
            'path_upload' => '/upload/image/',
            'default_image' => '/upload/default/no-image.png',
            'image_female' => '/upload/default/no-user-female.png',
            'image_male' => '/upload/default/no-user-male.png',
            'file_type' => 'jpeg|jpg|png|pdf',
            'max_size' => 100*(1024*1024),
        ],
        'socket' => base64_encode(json_encode(
            [
                'server' => 'https://adolbots.herokuapp.com',
                'script' => 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.1/socket.io.js',
                'token' => 't0khbh6g9ukek2b0hmnbtreqh0', // Token Akses ke Web Socket
                'room' => 'sipzip', // Default Room
            ]
        )),
        'navbar' => [
            ['text' => 'Dashboard', 'icon' => '<i class="icon-graph"></i>', 'url' => []],
            ['text' => 'LKE', 'icon' => '<i class="icon-note"></i>', 'url' => ['lke']],
            // ['text' => 'Menu', 'icon' => '<i class="icon-grid"></i>', 'url' => 'javascript:;', 'sub' => [
            //     ['text' => 'Sub Menu', 'icon' => '', 'url' => ['menu', 'submenu']],
            // ], 'rule' => []],
        ]
    ],
    'projects' => [ // List Of Projects
        'api' => [
            'path' => 'backend',
            'controller' => 'v1', // Set Default Controller
        ],
        'publish' => [
            'path' => 'frontend',
            'controller' => 'main', // Set Default Controller
        ],

    ],
    'templates' => [ // List Of Templates
        'bootstrap' => [
            'basepath' => '',
            'favicon' => '',
            'css' => [
                'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css',
            ],
            'js' => [
                'https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js',
            ],
        ],
        'highdmin' => [
            'basepath' => 'highdmin-13/',
            'favicon' => '/#',
            'meta' => [],
            'css' => [
                // Plugins
                'plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css',
                'plugins/bootstrap-select/css/bootstrap-select.min.css',
                'plugins/bootstrap-fileupload/bootstrap-fileupload.css',
                'plugins/dropzone/dropzone.css',
                'plugins/select2/css/select2.min.css',
                'plugins/switchery/switchery.min.css',
                'plugins/sweet-alert/sweetalert2.min.css',
                'plugins/jquery-toastr/jquery.toast.min.css',
                'plugins/datatables/dataTables.bootstrap4.min.css',

                // Core
                'assets/css/bootstrap.min.css',
                'assets/css/icons.css',
                'assets/css/style.css',
            ],
            'js' => [
                // Core
                'assets/js/jquery.min.js',
                'assets/js/bootstrap.bundle.min.js',
                'assets/js/waves.js',
                'assets/js/jquery.slimscroll.js',

                // plugins
                'plugins/jquery-knob/jquery.knob.js',
                'plugins/switchery/switchery.min.js',
                'plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js',
                'plugins/select2/js/select2.min.js',
                'plugins/bootstrap-select/js/bootstrap-select.js',
                'plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js',
                'plugins/bootstrap-maxlength/bootstrap-maxlength.js',
                'plugins/bootstrap-fileupload/bootstrap-fileupload.js',
                'plugins/dropzone/dropzone.js',

                'plugins/sweet-alert/sweetalert2.min.js',
                'plugins/jquery-toastr/jquery.toast.min.js',
                // 'plugins/highcharts/highcharts.js',
                // 'plugins/highcharts/exporting.js',
                // 'plugins/highcharts/export-data.js',
                'plugins/highcharts/stock/highstock.js',
                'plugins/highcharts/stock/data.js',
                'plugins/datatables/jquery.dataTables.min.js',
                'plugins/datatables/dataTables.bootstrap4.min.js',
                // 'https://code.highcharts.com/stock/highstock.js',
                // 'https://code.highcharts.com/stock/modules/data.js',

                // App js
                'assets/js/jquery.core.js',
                'assets/js/jquery.app.js',
                // 'assets/pages/jquery.dashboard.init.js',
                'assets/pages/jquery.form-advanced.init.js',
            ],
        ],
    ],
    'fcm' => [
        'topic' => '',
        'apikey' => '',
    ],
    'oauth' => [
        'user_auth' => [
            'endpoint' => '/user/auth',
            'service' => 'UserAuth',
            'cookie' => 'UserAuth',
            'jwt_access_token_secret' => $_ENV['JWT_ACCESS_TOKEN'] ?? '5c7ac2a0-4bda-4480-840e-313729c034f5', // UUID v4
            'jwt_refresh_token_secret' => $_ENV['JWT_REFRESH_TOKEN'] ?? 'a00afe0dc856d580d02080cfb335e93350cecfc906a1a91cc1dcce4e2aa92d6a', // Random Key
        ]
    ]
];

?>