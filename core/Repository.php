<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Repository.php
 */

namespace core;
use core\exception\{NotFoundException, DatabaseException};

class Repository {

    private $config = [];
    private $database = [];
    private $source = null;
    private $db = null;
    private $table = null;
    private $id = null;
    public $model = [];

    public function __construct($connection, $table, $id) {
        if (empty($connection)) {
            throw new DatabaseException('The params connection cannot be empty.');
        }
        if (empty($table)) {
            throw new DatabaseException('The params table cannot be empty.');
        }
        if (empty($id)) {
            throw new DatabaseException('The params id cannot be empty.');
        }

        $this->config = Config::Load('main');
        $this->database = Config::Load('database');
        if (isset($this->database[$connection]) && !empty($this->database[$connection])) {
            $this->source = $this->database[$connection];
            $this->table = $table;
            $this->id = $id;
        }
        else {
            throw new DatabaseException('Configuration: '.$connection.' Not Found');
        }
    }

    public function getConfig() {
        return $this->config;
    }
    
    public function getSettings() {
        return $this->config['settings'];
    }

    public function getSource() {
        return $this->source;
    }

    public function getTable() {
        return $this->table;
    }

    public function getID() {
        return $this->id;
    }

    protected function setDB($db) {
        $this->db = $db;
    }

    public function getDB() {
        return $this->db;
    }
    
    protected function setModel(array $model) {
        $this->model = $model;
    }

    public function getModel() {
        return $this->model;
    }

    public function filterParams(array $params, array $input) {
        return Helper::filterParams($params, $input);
    }

    public function setRandomID(int $size = 10) {
        return Helper::createRandomID($size);
	}

	public function setDateID() {
        return Helper::createDateID();
	}

    public function encrypt(string $text) {
        return Helper::encrypt($text);
	}

    public function decrypt(string $text) {
        return Helper::decrypt($text);
	}

    public function getDate() {
        return date('Y-m-d');
	}

    public function getDateTime() {
        return date('Y-m-d H:i:s');
	}

    public function getQuery(string $query) {
        throw new DatabaseException('Please code getId()');
    }

    public function getAll() {
        throw new DatabaseException('Please code getAll()');
    }

    public function getById(string $id) {
        throw new DatabaseException('Please code getId()');
    }

    // public function getPage(array $data, int $page, int $size) {
    public function getPage(array $query, array $params = [], int $page, int $size) {
        throw new DatabaseException('Please code getPage()');
    }

    public function save(array $data) {
        throw new DatabaseException('Please code save()');
    }

    public function delete(array $data) {
        throw new DatabaseException('Please code delete()');
    }

}
?>