<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 13 September 2021
 * @package 	: core system
 * @Description : SSH2.php
 */

namespace core;
use core\exception\{ConnectionException, ValidationException};

class SSH2 {

    private static $connection;

    /**
     * $options = [
     * 'host' = '',
     * 'port' = '',
     * 'user' = '',
     * 'pass' = '',
     * ]
     */
    public function __construct($options) {
        foreach ($options as $opt => $value) { $this->$opt = $value; }
        return $this;
    }

    // Disconnect Callback
    final function server_disconnect($reason, $message, $language) {
        throw new ConnectionException("Server disconnected with reason code [%d] and message: %sn", $reason, $message);
    }

    final function disconnect() {
        @ssh2_disconnect(self::$connection);
    }

    // Connect Server
    final function connect() {
        $callbacks = ['disconnect' => 'server_disconnect'];
        self::$connection = @ssh2_connect($this->host, $this->port, null, $callbacks);
        if (self::$connection === false) {
            throw new ConnectionException('Failed to connect to host: '.$this->host.' on port: '.$this->port);
            return false;
        }
        
        return true;
    }

    // Check Username & Password
    final function authenticate() {
        $auth = @ssh2_auth_password(self::$connection, $this->user, $this->pass);
        if ($auth === false) {
            throw new ValidationException('Failed to authenticate user: '.$this->user.' on host: '.$this->host);
            return false;
        }

        return true;
    }

    public function exec($command) {
        set_time_limit(0);
        while (ob_end_flush()); // end all output buffers if any
        
        $stream = @ssh2_exec(self::$connection, $command);
        $error = @ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
        
        // Enable Blocking
        @stream_set_blocking($stream, true);
        @stream_set_blocking($error, true);

        // Grab Response
        $result['success'] = true;
        $result['data'] = stream_get_contents($stream);

        $error = stream_get_contents($error);
        if ($error) {
            $result['success'] = false;
            $result['data'] = $error;
        }
        
        return $result;
    }

    /*
    echo '<pre class="scroll">';
    $ssh->ping('ping -c 3 google.com', function($data) {
        echo $data;
    }, function($result) {
        $this->showArray($result);
    });
    echo '</pre>';
    */
    // Masih Bug, jika ada kesalahan command (belum bisa handle error response)
    public function ping($command, $progress, $done) {
        set_time_limit(0);
        while (ob_end_flush()); // end all output buffers if any

        echo $command.PHP_EOL;
        $result = '';
        $stream = @ssh2_exec(self::$connection, $command);
        
        while (!feof($stream)) {
            $data = fread($stream, 4096);
            $result .= $data;
            $progress($data);
            @flush();
        }

        $done($result);
    }

    // Download via SCP
    public function download($source, $destination) {
        set_time_limit(0);
        $result = @ssh2_scp_recv(self::$connection, $source, $destination);
        return $result;
    }

    // Upload via SCP
    public function upload($source, $destination, $mode = 0644) {
        set_time_limit(0);
        $result = @ssh2_scp_send(self::$connection, $source, $destination);
        return $result;
    }

    public function getConnection() {
        return self::$connection;
    }
}
?>