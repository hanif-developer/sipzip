<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : WebApp.php
 */

namespace core;
use core\component\html;
use core\exception\{
    NotFoundException, 
    ConnectionException, 
    ValidationException
};

class WebApp extends Frameduz {

    private $viewParams = [
        'view' => '', 
        'code' => '', 
        'data' => [], 
        'template' => '', 
        'body' => '', 
        'script' => [],
        'controller' => ''
    ];
    
    public function __construct() {
        parent::__construct();
        $this->settings = (object) $this->getSettingList();
        $this->templates = (object) $this->getTemplateList();
        $this->html = new HTML();
        $this->requestMapping('GET', '/fzstyle', function() {
            ob_start();
            require_once CORE . '/component/style.css';
            $code = ob_get_clean();
            $this->showView(['code' => $code], Helper::$contentType['css']);
        });

        $this->requestMapping('GET', '/fzscript', function() {
            ob_start();
            require_once CORE . '/component/script.js';
            $code = ob_get_clean();
            $this->showView(['code' => $code], Helper::$contentType['js']);
        });
    }

    protected function showView(array $params = [], $contentType = '') {
        $params = $this->filterParams($this->viewParams, $params);
        $PathController = $this->getPathController();
        $Controller = !empty($params['controller']) ? $params['controller'] : $this->getController();
        // $Controller = 'main';
        $view = $PathController . '/view/' . $Controller . '/' . $params['view'] . '.' . $Controller . '.php';
        $code = $params['code'];

        // echo 'Project: '.$this->getProjectName().'<br>';
        // echo 'PathController: '.$PathController.'<br>';
        // echo 'Controller: '.$Controller.'<br>';
        // echo 'View: '.$params['view'].', Template: '.$params['template'].'<br>';
        // die;

        extract($params['data'], EXTR_SKIP);
        unset($params['data']);

        if (!empty($params['view'])) {
            if (!file_exists(APP . $view)) {
                // echo get_parent_class($Controller);
                // $PathController = $this->Url->getPathController();
                // $activeCtrl = 'app\\' . $PathController . '\\controller\\' . $this->Url->getControllerName();
                // echo get_parent_class($activeCtrl);
                // echo get_called_class();

                throw new NotFoundException('View: '.$view.' Not Found');
                return false;
            }
    
            ob_start();
            require_once APP . $view;
            $params['code'] = ob_get_clean();
        }
        
        $this->createTemplate($params, $contentType);
    }

    public function showError(string $message, $code = 404) {
        $code = <<<EOF
            <h1>Error: $code </h1>
            <h3>$message</h3>
            <a class="fzlink" href="javascript:history.back()">< Go Back</a>
        EOF;

        $this->showView(['code' => $code, 'body' => 'class="fzbody"', 'template' => 'frameduz']);
    }

    public function showArray($data) {
        echo '<pre style="background: #f1f1f1;padding: 15px; border-radius: 10px;">',print_r($data, 1),'</pre>';
    }

    public function createTemplate(array $params = [], $contentType = '') {
        $params = $this->filterParams($this->viewParams, $params);
        $template = $params['template'];
        $body = $params['body'];
        $code = $params['code'];
        $basepath = '/';
        $favicon = '#';
        $css = [
            $this->createLink([$this->getProject(), $this->getController(), 'fzstyle'])
        ];
        $js = [
            $this->createLink([$this->getProject(), $this->getController(), 'fzscript'])
        ];

        if ($contentType) {
            header('Content-Type: '.$contentType);
        }

        if ($template) {
            if (isset($this->templates->{$template}) && !empty($template)) {
                $template = $this->templates->{$template};
                $basepath = $template['basepath'] ?? $basepath;
                $basepath = $this->getBaseUrl() . '/template/' . $basepath;
                $favicon = $template['favicon'] ?? $favicon;
                $css = array_merge($template['css'], $css);
                $js = array_merge($template['js'], $js);
            }

            // Map CSS & JS to HTML script
            $css = array_map(function($v) {
                return '<link rel="stylesheet" href="'.$v.'" />'; 
            }, $css);
            
            $js = array_merge($js, $params['script']);
            $js = array_map(function($v) {
                return '<script src="'.$v.'"></script>'; 
            }, $js);
            $css = implode("\n\t", $css);
            $js = implode("\n", $js);

            // Show Template
            $html = <<<EOF
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>{$this->settings->web['title']}</title>
                <base href="$basepath">
                <link rel="shortcut icon" href="$favicon" type="image/x-icon">
                $css
            </head>
            <body $body>
            $code
            $js
            </body>
            </html>
            EOF;

            echo $html;
        }
        else {
            echo $code;
        }
    }

}
?>