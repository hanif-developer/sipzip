<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Helper.php
 */

namespace core;

class Helper {

    public static $contentType = [
        'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'jpe' => 'image/jpeg',
		'gif' => 'image/gif',
		'png' => 'image/png',
		'bmp' => 'image/bmp',
		'tif' => 'image/tiff',
		'tiff' => 'image/tiff',
		'ico' => 'image/x-icon',
		'asf' => 'video/asf',
		'asx' => 'video/asf',
		'wax' => 'video/asf',
		'wmv' => 'video/asf',
		'wmx' => 'video/asf',
		'avi' => 'video/avi',
		'divx' => 'video/divx',
		'flv' => 'video/x-flv',
		'mov' => 'video/quicktime',
		'qt' => 'video/quicktime',
		'mpeg' => 'video/mpeg',
		'mpg' => 'video/mpeg',
		'mpe' => 'video/mpeg',
		'txt' => 'text/plain',
		'asc' => 'text/plain',
		'c' => 'text/plain',
		'cc' => 'text/plain',
		'h' => 'text/plain',
		'csv' => 'text/csv',
		'tsv' => 'text/tab-separated-values',
		'rtx' => 'text/richtext',
		'css' => 'text/css',
		'htm' => 'text/html',
		'html' => 'text/html',
		'mp3' => 'audio/mpeg',
		'm4a' => 'audio/mpeg',
		'm4b' => 'audio/mpeg',
		'mp4' => 'video/mp4',
		'm4v' => 'video/mp4',
		'ra' => 'audio/x-realaudio',
		'ram' => 'audio/x-realaudio',
		'wav' => 'audio/wav',
		'ogg' => 'audio/ogg',
		'oga' => 'audio/ogg',
		'ogv' => 'video/ogg',
		'mid' => 'audio/midi',
		'midi' => 'audio/midi',
		'wma' => 'audio/wma',
		'mka' => 'audio/x-matroska',
		'mkv' => 'video/x-matroska',
		'rtf' => 'application/rtf',
		'js' => 'application/javascript',
		'pdf' => 'application/pdf',
		'doc' => 'application/msword',
		'docx' => 'application/msword',
		'pot' => 'application/vnd.ms-powerpoint',
		'pps' => 'application/vnd.ms-powerpoint',
		'ppt' => 'application/vnd.ms-powerpoint',
		'pptx' => 'application/vnd.ms-powerpoint',
		'ppam' => 'application/vnd.ms-powerpoint',
		'pptm' => 'application/vnd.ms-powerpoint',
		'sldm' => 'application/vnd.ms-powerpoint',
		'ppsm' => 'application/vnd.ms-powerpoint',
		'potm' => 'application/vnd.ms-powerpoint',
		'wri' => 'application/vnd.ms-write',
		'xla' => 'application/vnd.ms-excel',
		'xls' => 'application/vnd.ms-excel',
		'xlsx' => 'application/vnd.ms-excel',
		'xlt' => 'application/vnd.ms-excel',
		'xlw' => 'application/vnd.ms-excel',
		'xlam' => 'application/vnd.ms-excel',
		'xlsb' => 'application/vnd.ms-excel',
		'xlsm' => 'application/vnd.ms-excel',
		'xltm' => 'application/vnd.ms-excel',
		'mdb' => 'application/vnd.ms-access',
		'mpp' => 'application/vnd.ms-project',
		'docm' => 'application/vnd.ms-word',
		'dotm' => 'application/vnd.ms-word',
		'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml',
		'sldx' => 'application/vnd.openxmlformats-officedocument.presentationml',
		'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml',
		'potx' => 'application/vnd.openxmlformats-officedocument.presentationml',
		'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml',
		'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml',
		'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml',
		'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml',
		'onetoc' => 'application/onenote',
		'onetoc2' => 'application/onenote',
		'onetmp' => 'application/onenote',
		'onepkg' => 'application/onenote',
		'swf' => 'application/x-shockwave-flash',
		'class' => 'application/java',
		'tar' => 'application/x-tar',
		'zip' => 'application/zip',
		'gz' => 'application/x-gzip',
		'gzip' => 'application/x-gzip',
		'exe' => 'application/x-msdownload',
		// openoffice formats
		'odt' => 'application/vnd.oasis.opendocument.text',
		'odp' => 'application/vnd.oasis.opendocument.presentation',
		'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
		'odg' => 'application/vnd.oasis.opendocument.graphics',
		'odc' => 'application/vnd.oasis.opendocument.chart',
		'odb' => 'application/vnd.oasis.opendocument.database',
		'odf' => 'application/vnd.oasis.opendocument.formula',
		// wordperfect formats
		'wp' => 'application/wordperfect',
		'wpd' => 'application/wordperfect',
		// php formats
		'php' => 'application/x-httpd-php',
		'php4' => 'application/x-httpd-php',
		'php3' => 'application/x-httpd-php',
		'phtml' => 'application/x-httpd-php',
		'phps' => 'application/x-httpd-php-source',
		//Database Table formats
		'sql' => 'application/x-sql'
    ];

    public static $responseMessage = [
        'code' => 200,
        'status' => 'success',
        'message' => '',
        'data' => null,
    ];

	public static function createDateID() {
        return date('YmdHis');
	}

    public static function createRandomID(int $size = 10) {
        return substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", $size)), 0, $size);
	}

    public static function createRandomKey(int $size = 32) {
        return bin2hex(random_bytes($size));
	}

    public static function UUIDv4($data = null) {
        /**
         * This function requires PHP 7 or later because of the use of the function random_bytes on line #3.
         * If you're using PHP 5 or earlier and have the openssl extension installed, you could use openssl_random_pseudo_bytes(16), instead of random_bytes(16) on line #3.
         * Credits go to this answer on Stackoverflow for this solution.
         * An example usage of this function would look like this:
         * $myuuid = UUIDv4();
         * echo $myuuid;
         */
        
         // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
    
        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    
        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public static function base64UrlEncode(string $text) {
        return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($text));
    }

	public static function encrypt($string) {
		$output = false;
		$encrypt_method = 'AES-256-CBC';
		$secret_key1 = '0398a34c-6599-4aa5-a8c5-1cb8b36a6179';
		$secret_key2 = 'c63aa130-b24b-4e39-b5ec-fb1f23f001e5';
		$key1 = hash('sha256', $secret_key1);
		$key2 = substr(hash('sha256', $secret_key2), 0, 16);
		$output = base64_encode(openssl_encrypt(($string), $encrypt_method, $key1, 0, $key2));
		return $output;
	}
	
	public static function decrypt($string) {
		$output = false;
		$encrypt_method = 'AES-256-CBC';
		$secret_key1 = '0398a34c-6599-4aa5-a8c5-1cb8b36a6179';
		$secret_key2 = 'c63aa130-b24b-4e39-b5ec-fb1f23f001e5';
		$key1 = hash('sha256', $secret_key1);
		$key2 = substr(hash('sha256', $secret_key2), 0, 16);
		$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key1, 0, $key2);
		return $output;
	}

	public static function dateFormat($strdate, $options) {
		$monthname = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$dayname = array('Sun' => 'Minggu', 'Mon' => 'Senin', 'Tue' => 'Selasa', 'Wed' => 'Rabu', 'Thu' => 'Kamis', 'Fri' => 'Jumat', 'Sat' => 'Sabtu');
		
		$D = date('D', strtotime($strdate));
        $d = date('d', strtotime($strdate));
        $m = date('m', strtotime($strdate));
		$M = date('M', strtotime($strdate));
        $y = date('Y', strtotime($strdate));
		$w = date('H:i:s', strtotime($strdate));
		$t = date('H:i a', strtotime($strdate));

		switch ($options) {
			case 'time' : return $t; break;
			case 'day' : return $dayname[$D]; break;
			case 'short_date' : return date('d/m/Y', strtotime($tgl)); break;
			case 'long_date' : return intval($d).' '.$monthname[$m-1].' '.$y; break;
			case 'short_date_time' : return date('d/m/Y H:i:s', strtotime($tgl)); break;
			case 'long_date_time' : return intval($d).' '.$monthname[$m-1].' '.$y.' ['.$w.']'; break;
			case 'date_month' : return intval($d).' '.$M; break;
		}
	}

    public static function filterParams(array $params, array $input) {
        foreach ($params as $key => $value) { if(isset($input[$key]) && !empty($input[$key])) $params[$key] = $input[$key]; }
		return $params;
    }

	// JSON RAW
    public static function getBody(array $model = []) {
        $body = file_get_contents('php://input');
        $body = json_decode($body, true) ?? []; // ?? check if null
        $params = $model ?: $body; // ?: check if empty
        return self::filterParams($params, $body);
    }

    // HEADERS
    public static function getHeaders(array $model = []) {
        // $header = apache_request_headers();
		$header = getallheaders();
        $header = array_change_key_case($header, CASE_LOWER);
        $params = $model ?: $header; // ?: check if empty
        return self::filterParams($params, $header);
    }

    // GET
    public static function getParams(array $model = []) {
        $params = $model ?: $_GET; // ?: check if empty
        return self::filterParams($params, $_GET);
    }

    // POST
    public static function getFields(array $model = []) {
        $params = $model ?: $_POST; // ?: check if empty
        return self::filterParams($params, $_POST);
    }

    // FILES
    public static function getFiles(array $model = []) {
        $params = $model ?: $_FILES; // ?: check if empty
        return self::filterParams($params, $_FILES);
    }

    public static function showResponse(array $responseMessage) {
        header('HTTP/1.1 '.$responseMessage['code']);
        header('Content-Type:application/json');
        echo json_encode($responseMessage);
    }

    public static function errorResponse(string $message = 'Request Not Found', int $code = 404) {
        self::$responseMessage['code'] = $code;
        self::$responseMessage['status'] = 'error';
        self::$responseMessage['message'] = $message;
        self::showResponse(self::$responseMessage);
    }

	public static function formatCharts($contents, $labels, $values) {
        $statistik = [
            'labels' => [],
            'values' => []
        ];
        
        // Data Original
        foreach ($contents as $key => $val) {
            $statistik['labels'][$key] = $val[$labels];
            $statistik['values'][$key] = intval($val[$values]);
        }

        /**
         * Urutkan data berdasarkan kolom $values
         * array_column => Ambil data pada kolom tertentu
         * array_multisort => urutkan data array, berdasarkan kolom
         */
        $data_ordered = $contents;
        $order_by_jumlah = array_column($data_ordered, $values);
        array_multisort($order_by_jumlah, SORT_DESC, $data_ordered);
        foreach ($data_ordered as $key => $val) {
            $statistik['labels'][$key] = $val[$labels];
            $statistik['values'][$key] = intval($val[$values]);
        }

        return [
            'statistik' => $statistik,
        ];
    }

}
?>