<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Singleton.php
 */

namespace core;

class Singleton {
    
    protected static $instance = null;

    protected function __construct() {}

    public static function getInstance() {
        if (!isset(static::$instance)) {
            static::$instance = new Static();
        }

        return static::$instance;
    }

}
?>