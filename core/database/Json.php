<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Json.php
 */

namespace core\database;
use core\Repository;
use core\exception\{NotFoundException, DatabaseException};

class Json extends Repository {

    public function __construct($connection, $table, $id) {
        parent::__construct($connection, $table, $id);
    }

    private function connect() {
        if (is_null($this->getDB())) {
            $file = ASSET.$this->getSource()['path'];
            if (file_exists($file)) {
                $json = file_get_contents($file);
                $json = json_decode($json, true);
                $db = $json ?? [];
                $this->setDB($db);
            }
        }
    }

    private function disconnect() {
        $this->setDB(null);
    }

    private function execute() {
        $result = 0;
        if (!is_null($this->getDB())) {
            $file = ASSET.$this->getSource()['path'];
            $json = json_encode($this->getDB(), JSON_PRETTY_PRINT);
            $result = file_put_contents($file, $json);
        }
        $this->disconnect();
        return $result;
    }

    public function getQuery(string $query, $data = []) {
        $result = $this->getAll();
        $value = array_filter($result, function($item) use ($query, $data) {
            // Default status value
            $status = (strtoupper($query) == 'AND') ? true : false;
            foreach ($data as $key => $value) {
                $match = preg_match('/'.$value.'/i', $item[$key]);
                if (strtoupper($query) == 'AND') {
                    $status = $status && $match;
                }
                else {
                    $status = $status || $match;
                }
            }
            return $status;
        });

        $params = [];
        foreach ($data as $key => $val) {
            array_push($params, $key.'="'.$val.'"');
        }

        return [
            'value' => $value,
            'count' => count($value),
            'query' => implode(' '.$query.' ', $params),
        ];
    }

    public function getAll() {
        $this->connect();
        $db = $this->getDB();
        $result = $db[$this->getTable()] ?? [];
        $this->disconnect();
        return $result;
    }

    public function getById(string $id) {
        $result = $this->getAll();
        $index = array_search($id, array_column($result, $this->getID()));
        return ($index !== false) ? $result[$index] : null;
    }

    public function getByFields(array $params, $operation = 'AND') {
        $result = $this->getQuery($operation, $params);
        return ($result['count'] > 0) ? current($result['value']) : null;
    }

    public function getAllById(string $id) {
        // $result = $this->getAll();
        // $index = array_search($id, array_column($result, $this->getID()));
        // return ($index !== false) ? $result[$index] : null;
    }


    // public function getPage(array $data, int $page, int $size) {
    public function getPage(string $query, array $params = [], int $page, int $size) {
        $data = $this->getQuery($query, $params);
        $cursor = ($page - 1) * $size;
        $total = $data['count'];
        $contents = array_slice($data['value'], $cursor, $size);
        $result['number'] = $cursor + 1;
		$result['page'] = $page;
		$result['size'] = $size;
		$result['total'] = $total;
		$result['totalpages'] = ceil($total / $size);
		$result['contents'] = $contents;
		$result['query'] = $data['query'];
        return $result;
    }

    public function save(array $data) {
        $result = $this->getAll();
        $index = array_search($data[$this->getID()], array_column($result, $this->getID()));
        $data = $this->filterParams($this->getModel(), $data);
        $json_query = '';
        $this->connect();
        if ($index !== false) { // Mode Edit
            $result[$index] = $data;
            $json_query = 'update';
        } else { // Mode Add
            array_push($result, $data);
            $json_query = 'add';
        }

        $success = 0;
        $message = '';
        $db = $this->getDB();
        $db[$this->getTable()] = $result;
        $this->setDB($db);
        $result = $this->execute();
        if ($result) {
            $success = 1;
        }
        
        return [
            'success' => $success,
            'messages' => $message,
            'query' => $json_query,
            'data' => $data,
        ];
    }
    
    public function delete(array $data) {
        $result = $this->getAll();
        $json_query = '';
        $index = array_search($data[$this->getID()], array_column($result, $this->getID()));
        $this->connect();
        if ($index !== false) {
            unset($result[$index]);
            $json_query = 'delete';
        }
        
        $success = 0;
        $message = '';
        $db = $this->getDB();
        $db[$this->getTable()] = array_values($result);
        $this->setDB($db);
        $result = $this->execute();
        if ($result) {
            $success = 1;
        }
        
        return [
            'success' => $success,
            'messages' => $message,
            'query' => $json_query,
            'data' => $data,
        ];
    }

}
?>