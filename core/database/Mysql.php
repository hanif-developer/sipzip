<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Mysql.php
 */

namespace core\database;
use core\Repository;
use core\exception\{NotFoundException, DatabaseException};
use PDO, PDOException;

class Mysql extends Repository {

    public function __construct($connection, $table, $id) {
        parent::__construct($connection, $table, $id);
    }

    private function connect() {
        if (is_null($this->getDB())) {
            $dsn = implode(';', [
                'host='.$this->getSource()['host'],
                'port='.$this->getSource()['port'],
                'dbname='.$this->getSource()['dbname'],
            ]);

            $user = $this->getSource()['user'];
            $password = $this->getSource()['password'];
            $options[PDO::ATTR_PERSISTENT] = $this->getSource()['persistent'];
            $options[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET sql_mode="TRADITIONAL"';

            try {
                $db = new PDO($this->getSource()['driver'].':'.$dsn, $user, $password, $options);
                $this->setDB($db);
            } catch (PDOexception $err) {
                throw new DatabaseException($err->getMessage());
            }
        }
    }

    private function disconnect() {
        $this->setDB(null);
    }

    public function createQueryParams($params, $operation) {
        $conditions = array_map(function($v) use ($operation) {
            return ($operation == 'LIKE') ? "($v LIKE ?)" : "($v = ?)";
            // return ($operation == 'LIKE') ? "($v LIKE ?)" : "($v =: $v)";
        }, array_keys($params));

        $params = array_map(function($v) use ($operation) {
            return ($operation == 'LIKE') ? '%'.$v.'%' : $v;
        }, array_values($params));

        return [$conditions, $params];
    }

    public function createInsertParams($params) {
        $conditions = array_map(function($v) {
            return "$v = ?";
        }, array_keys($params));

        $params = array_map(function($v) {
            return $v;
        }, array_values($params));

        return [$conditions, $params];
    }

    public function createSqlQuery(string $sql_query, array $params = null) {
		if (!empty($params)) {
			$indexed = $params == array_values($params);
			foreach ($params as $k=>$v) {
				if (is_object($v)) {
					if ($v instanceof \DateTime) $v = $v->format('Y-m-d H:i:s');
					else continue;
				}
				else if (is_string($v)) $v="'$v'";
				else if ($v === null) $v='NULL';
				else if (is_array($v)) $v = implode(',', $v);
	
				if ($indexed) {
					$sql_query = preg_replace('/\?/', $v, $sql_query, 1);
				}
				else {
					if ($k[0] != ':') $k = ':'.$k;
					$sql_query = str_replace($k, $v, $sql_query);
				}
			}
		}
        
        $sql_query .= ';';
        return preg_replace('/\s+/', ' ', $sql_query);
	}

    public function getQuery(string $query, array $data = []) {
        set_time_limit(0);
		if(is_null($this->getDB())) $this->connect();
		$sql_stat = $this->getDB()->prepare($query);
		$sql_stat->execute($data);
		$sql_value = $sql_stat->fetchAll(PDO::FETCH_ASSOC);
		$sql_count = $sql_stat->rowCount();		
		$sql_query = $this->createSqlQuery($query, $data);
		$this->disconnect();

        return [
            'value' => $sql_value,
            'count' => $sql_count,
            'query' => $sql_query,
        ];
    }

    // Handle null, 0 value
    public function filterModel(array $params, array $input) {
        // foreach ($params as $key => $value) { $params[$key] = $input[$key]; }
        foreach ($params as $key => $value) { if(isset($input[$key])) $params[$key] = $input[$key]; }
		return $params;
    }

    public function getModel() {
		$model = parent::getModel();
		$result = $this->getQuery('SHOW COLUMNS FROM '.$this->getTable());
        $fields = [];
        foreach ($result['value'] as $key => $value) {
            $fields[$value['Field']] = '';
        }

        if (!empty($model)) {
            $fields = $this->filterModel($fields, $model);
        }
        
        return $fields;
    }

    public function getAll() {
        $result = $this->getQuery('SELECT * FROM '.$this->getTable());
        return $result['value'];
        
    }

    public function getById(string $id) {
        $result = $this->getQuery('SELECT * FROM '.$this->getTable().' WHERE '.$this->getID().' = ?', [$id]);
        return ($result['count'] > 0) ? current($result['value']) : null;
    }

    public function getByFields(array $params, $operation = 'AND') {
        list($conditions, $params) = $this->createQueryParams($params, '=');
        $where = ($params) ? ' WHERE '.implode(' '.$operation.' ', $conditions) : '';
        $query = 'SELECT * FROM '.$this->getTable().$where;
        $result = $this->getQuery($query, $params);
        // return $result['query'];
        return ($result['count'] > 0) ? current($result['value']) : null;
    }

    public function getAllById(string $id) {
        $result = $this->getQuery('SELECT * FROM '.$this->getTable().' WHERE '.$this->getID().' = ?', [$id]);
        return $result['value'];
    }

    public function getAllByFields(array $params, $operation = 'AND') {
        list($conditions, $params) = $this->createQueryParams($params, '=');
        $where = ($params) ? ' WHERE '.implode(' '.$operation.' ', $conditions) : '';
        $query = 'SELECT * FROM '.$this->getTable().$where;
        $result = $this->getQuery($query, $params);
        // return $result['query'];
        return $result['value'];
    }

    // public function getPage(array $data, int $page, int $size) {
    public function getPage(array $query, array $params = [], int $page, int $size) {
        // Get Query Total
        $data_total = $this->getQuery($query['total'], $params);
        $total = ($data_total['count'] > 0) ? $data_total['value'][0]['total'] : 0;

        // Get Query Value
        $cursor = ($page - 1) * $size;
        $data_value = $this->getQuery($query['value'].' LIMIT '.$cursor.', '.$size, $params);
        $contents = $data_value['value'];
        
        $result['number'] = $cursor + 1;
		$result['page'] = $page;
		$result['size'] = $size;
		$result['total'] = $total;
		$result['totalpages'] = ceil($total / $size);
		$result['contents'] = $contents;
		$result['query'] = $data_value['query'];
        return $result;
    }

    public function save(array $data) {
        $data = $this->filterParams($this->getModel(), $data);
        if(is_null($this->getDB())) $this->connect();
        list($conditions, $params) = $this->createInsertParams($data);
        $value = implode(', ', $conditions);
        $query = 'INSERT INTO '.$this->getTable().' SET '.$value.' ON DUPLICATE KEY UPDATE '.$value;
        $params = array_merge($params, $params);
        $success = 0;
        $message = '';
        $db = $this->getDB();
        try {
            $db->beginTransaction();
            $sql_stat = $db->prepare($query);
            $success = $sql_stat->execute($params);
            $message = $sql_stat->errorInfo();
            $db->commit();
        } catch (\Throwable $err) {
            $db->rollback();
            throw new DatabaseException($err->getMessage());
        }
        
        $sql_query = $this->createSqlQuery($query, $params);
		$this->disconnect();

        return [
            'success' => $success,
            'message' => $message[2],
            // 'messages' => $message,
            'query' => $sql_query,
            'data' => $data,
        ];
    }
    
    public function delete(array $data) {
        if(is_null($this->getDB())) $this->connect();
        $query = 'DELETE FROM '.$this->getTable().' WHERE '.$this->getID().' = ? ';
        $params = [$data[$this->getID()]];
        $success = 0;
        $message = '';
        $db = $this->getDB();
        try {
            $db->beginTransaction();
            $sql_stat = $db->prepare($query);
            $success = $sql_stat->execute($params);
            $message = $sql_stat->errorInfo();
            $db->commit();
        } catch (\Throwable $err) {
            $db->rollback();
            throw new DatabaseException($err->getMessage());
        }
        
        $sql_query = $this->createSqlQuery($query, $params);
		$this->disconnect();

        return [
            'success' => $success,
            'message' => $message[2],
            // 'messages' => $message,
            'query' => $sql_query,
            'data' => $data,
        ];
    }

    /**
     * Format: ABC.###
     */
    public function setSequenceID($code, $number = 0) {
        if (empty($number)) {
            $result = $this->getQuery('SELECT * FROM '.$this->getTable().' ORDER BY '.$this->getID().' DESC LIMIT 1');
            if ($result['count'] > 0) {
                $data = current($result['value']);
                $data = explode('.', $data[$this->getID()]);
                $number = intval($data[1]) + 1;
            }
            else {
                $number = 1;
            }
        }
        
        return $this->formatCode($code, $number);
    }

    private function formatCode($code, $number) {
        $code = explode('.', $code); // 0: format, 1: id number
        if (strlen($number) > strlen($code[1])) {
            $max_id = $code[0].'.'.sprintf('%0'.strlen($code[1]).'s', ($number - 1));
            throw new DatabaseException('Maximum ID Number: '.$max_id);
        }

        return $code[0].'.'.sprintf('%0'.strlen($code[1]).'s', $number);
    }

}
?>