<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Url.php
 */

namespace core;

class Url {

    private $Config = '';
    private $reqUrl = '';
    private $baseUrl = '';
    private $activeUrl = '';
    private $ProjectName = '';
    private $PathController = '';
    private $ControllerName = '';
	private $defaultProject = '';
    private $defaultPathController = '';
    private $defaultController = '';
    
    public function __construct(){
		$selfArr = explode('/', rtrim($_SERVER['PHP_SELF'], '/'));
		$selfKey = array_search(INDEX_FILE, $selfArr);
		$this->baseUrl = $this->isHttps() ? 'https://':'http://';
		$this->baseUrl .= $_SERVER['HTTP_HOST'] . implode('/', array_slice($selfArr, 0, $selfKey));
		$this->activeUrl = $this->isHttps() ? 'https://':'http://';
		$this->activeUrl .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		
		$this->Config = Config::Load('main');
		$this->defaultProject =  $this->Config['application']['project'];
		$this->defaultPathController = $this->Config['projects'][$this->defaultProject]['path'];
		$this->defaultController = $this->Config['projects'][$this->defaultProject]['controller'];
		
		$this->ProjectName =  $this->defaultProject;
		$this->PathController = $this->defaultPathController;
		$this->ControllerName = $this->defaultController;
		
        $url = trim($_SERVER['REQUEST_URI'], '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);
        if(isset($url[0]) && !empty($url[0])){
            // Check Project Name
            if(array_key_exists($url[0], $this->Config['projects'])){ // Jika sebuah project
                $this->ProjectName = $url[0];
				unset($url[0]);
                $this->PathController = $this->Config['projects'][$this->ProjectName]['path'];
                $this->ControllerName = isset($url[1]) ? $url[1] : $this->Config['projects'][$this->ProjectName]['controller'];
				unset($url[1]);
            }
            else{ // is controller
                $this->ProjectName =  $this->defaultProject;
                $this->PathController = $this->defaultPathController;
                $this->ControllerName = isset($url[0]) ? $url[0] : $this->ControllerName;
				unset($url[0]);
            }

        }

		$this->reqUrl = ['/', $this->getProject(), $this->getController(), implode('/', $url)];
		$this->reqUrl = implode('/', $this->reqUrl);
		$this->reqUrl = preg_replace('#/+#','/', $this->reqUrl); // remove duplicate /
		$this->reqUrl = rtrim($this->reqUrl, '/');
		// echo 'Url: '.$this->reqUrl;die;

		// echo 'PathController: '.$this->PathController.'<br>';
		// echo 'ProjectName: '.$this->ProjectName.'<br>';
		// echo 'ControllerName: '.$this->getControllerName().'<br>';
		// echo 'Controller: '.$this->getController().'<br>';
	}

	public function isHttps() {
		if (isset($_SERVER['HTTPS'])) {
			if(strtolower($_SERVER['HTTPS']) == 'on') return true;
			if($_SERVER['HTTPS'] == '1') return true;
		}
		elseif (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == '443')) {
			return true;
        }
        elseif (isset($_SERVER['HTTP_X_FORWARDED_PORT']) && ($_SERVER['HTTP_X_FORWARDED_PORT'] == '443')) {
			return true;
		}
		return false;
	}

    public function getConfig() {
		return $this->Config;
	}

    public function getBaseUrl() {
		return $this->baseUrl;
	}

    public function getActiveUrl() {
		return $this->activeUrl;
	}

	public function getReqUrl() {
		return  $this->reqUrl;
	}
	
	public function getProjectName() {
		return strtolower($this->ProjectName);
	}

	public function getControllerName() {
		return strtolower($this->ControllerName);
	}
	
    public function getPathController() {
		return strtolower($this->PathController);
	}

	public function getDefaultProject() {
		return strtolower($this->defaultProject);
	}

    public function getDefaultPathController() {
		return strtolower($this->defaultPathController);
	}
	
	public function getDefaultController() {
		return strtolower($this->defaultController);
	}

	public function getProject() {
		return ($this->getProjectName() == $this->getDefaultProject()) ? '' : $this->getProjectName();
	}
	
	public function getController() {
		$PathController = $this->getPathController();
		$PathController = 'app\\' . $PathController . '\\controller\\';
		
		$ctrl = $PathController . $this->getControllerName();
		if (class_exists($ctrl)) {
			return $this->getControllerName();
		}
		else {
			return $this->getDefaultController();
		}
	}
	
}
?>