<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Fcm.php
 */

namespace core;

class Fcm {
    
    private $title = 'Test';
    private $message = 'Test kirim notifikasi dari server';
    private $image = '';
    private $payload = '';
    private $result = [];

    public $config = [];
	
	public function __construct(){
        $config = Config::Load('main');
        $this->config = $config['fcm'];
	}

    public function setTitle($title) {
        $this->title = $title;
    }
 
    public function setMessage($message) {
        $this->message = $message;
    }
 
    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }
 
    public function setPayload($payload) {
        $this->payload = $payload;
    }

    public function getPush() {
        $result['title'] = $this->title;
        $result['message'] = $this->message;
        $result['image'] = $this->image;
        $result['payload'] = empty($this->payload) ? new \stdClass() : $this->payload;
        $result['timestamp'] = date('Y-m-d G:i:s');
        return $result;
    }

    /**
     * sending push message to single user by firebase reg id or 
     * multiple users by firebase registration ids
     */
    public function send($to, $message) {
        $fields = (\is_array($to)) ? ['registration_ids' => $to] : ['to' => $to];
        $fields['data'] = ['data' => $message];

        return $this->sendPushNotification($fields);
    }
 
    // Sending message to a topic by topic name
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => ['data' => $message],
        );

        return $this->sendPushNotification($fields);
    }
 
    // function makes curl request to firebase servers
    private function sendPushNotification($fields) {
        $result = $this->sendMessagePost([
            'url' => 'https://fcm.googleapis.com/fcm/send',
            'header' => [
                'Authorization: key=' . $this->config['apikey'],
                'Content-Type: application/json'
            ],
            'fields' => \json_encode($fields)
        ]);

        return \json_decode($result, true);
    }

    private function sendMessagePost($data){
        /**
         * Params:
         * $data['url']
         * $data['header']
         * $data['fields']
         */
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $data['url']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $data['header']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data['fields']);
        $result = curl_exec($ch);           
        if($result === FALSE){
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

}
?>