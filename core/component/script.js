/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: Component WebApp
 * @Description : fzscript.js
 */

// console.clear();
console.group("<?= $this->settings->web['title'] ?>");
console.log("Author:", "<?= $this->settings->web['author'] ?>");
console.log("Description:", "<?= $this->settings->web['description'] ?>");
console.groupEnd();

Frameduz = function (id) {
    modul = $(id);
    table = {
        query: modul.find(".fztable-query"),
        content: modul.find(".fztable-content"),
        contentTbody: modul.find(".fztable-content .table tbody"),
        contentTbodyRows: modul.find(".fztable-content .table tbody tr"),
        paging: modul.find(".fztable-paging ul"),
        pagingItem: modul.find(".fztable-paging ul li"),
        pageSize: modul.find(".fztable-pagesize"),
        totalData: modul.find(".total-data"),
        loader: this.createObjectLoader(modul, "fztable"),
        empty: this.createObjectEmpty(modul, "fztable").hide(),
    }

    list = {
        query: modul.find(".fzlist-query"),
        content: modul.find(".fzlist-content"),
        contentBody: modul.find(".fzlist-content .list-body"),
        contentItem: modul.find(".fzlist-content .list-item"),
        paging: modul.find(".fzlist-paging ul"),
        pagingItem: modul.find(".fzlist-paging ul li"),
        pageSize: modul.find(".fzlist-pagesize"),
        totalData: modul.find(".total-data"),
        loader: this.createObjectLoader(modul, "fzlist"),
        empty: this.createObjectEmpty(modul, "fzlist").hide(),
    }

    this.modul = modul;
    this.table = table;
    this.list = list;
    this.form = modul.find(".fzform-content").clone().show();
}

// Create Object Loader 
Frameduz.prototype.createObjectLoader = function (modul, clazz) {
    const cls_content = clazz+"-content";
    const cls_loader = clazz+"-loader";
    let loader = $("<div>");

    if (modul.find("."+cls_content).length > 0) {
        if (modul.find("."+cls_loader).length > 0) {
            loader = modul.find("."+cls_loader);
        }
        else {
            loader = $("<div>").addClass(cls_loader).html('<div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div>');
            modul.find("."+cls_content).parent().append(loader);
            // modul.append(loader);
        }    
    }
    
    return loader;
}

// Create Object Empty 
Frameduz.prototype.createObjectEmpty = function (modul, clazz) {
    const cls_content = clazz+"-content";
    const cls_empty = clazz+"-empty";
    let empty = $("<div>");

    if (modul.find("."+cls_content).length > 0) {
        if (modul.find("."+cls_empty).length > 0) {
            empty = modul.find("."+empty);
        }
        else {
            // empty = $("<div>").addClass(cls_empty).html('<div class="alert alert-danger alert-dismissable"><p><strong>Data tidak ditemukan !</strong></p></div>');
            empty = $("<div>").addClass(cls_empty);
            modul.find("."+cls_content).parent().append(empty);
            // modul.append(empty);
        }    
    }
    
    return empty;
}

// Load Table
Frameduz.prototype.loadTable = function (params) {
    const app = this;
    app.table.loader.show();
    app.table.empty.hide();
    app.table.content.hide();
    // app.table.paging.hide();
    app.table.paging.addClass("hidden");
    app.table.paging.html("");
    // app.table.pageSize.hide();
    app.table.pageSize.addClass("hidden");
    setTimeout(function () {
        app.sendRequest("POST", {
            url: params.url,
            header: params.header,
            data: params.data,
            onSuccess: function (response) {
                // console.log(response);
                if (typeof params.onLoad === "function") params.onLoad(response.data);
                app.createTable(response.data, params);
            },
            onError: function (error) {
                // console.log(error);
                if (typeof params.onError === "function") {
                    params.onError(error);
                }
                else {
                    alert(error.message);
                }
            }
        });
    }, 500);
}

// Load List
Frameduz.prototype.loadList = function (params) {
    const app = this;
    app.list.loader.show();
    app.list.empty.hide();
    app.list.content.hide();
    // app.list.paging.hide();
    app.list.paging.addClass("hidden");
    app.list.paging.html("");
    // app.list.pageSize.hide();
    app.list.pageSize.addClass("hidden");
    setTimeout(function () {
        app.sendRequest("POST", {
            url: params.url,
            header: params.header,
            data: params.data,
            onSuccess: function (response) {
                // console.log(response);
                if (typeof params.onLoad === "function") params.onLoad(response.data);
                app.createList(response.data, params);
            },
            onError: function (error) {
                // console.log(error);
                if (typeof params.onError === "function") {
                    params.onError(error);
                }
                else {
                    alert(error.message);
                }
            }
        });
    }, 500);
}

// Create Table
Frameduz.prototype.createTable = function (data, params) {
    const app = this;
    app.table.loader.hide();
    app.table.empty.hide();

    let query = data.query;
    let contents = data.contents;
    let number = data.number;
    let total = data.total;

    if (query != "") {
        app.table.query.html("<code>" + query + "</code>");
    }

    app.table.totalData.html(this.ribuan(total));
    if (total > 0) {
        // Create Table Contents
        app.table.contentTbody.html("");
        if (app.table.contentTbodyRows.length == 0) return;
        for (let rows in contents) {
            let row = app.table.contentTbodyRows.clone();
            let result = row.html().replace("{number}", number++);
            json = contents[rows];
            for (key in json) {
                var find = new RegExp("{" + key + "}", "g");
                result = result.replace(find, json[key]);
                row.html(result);
            }
            app.table.contentTbody.append(row);
        }
        app.table.content.show();
        if (typeof params.onShow === "function") {
            app.table.content.find("img").each(function (idx, img) {
                const _src = this.getAttribute("_src");
                this.setAttribute("src", _src);
            });
            params.onShow(app.table.content);
        }

        // Create Paging
        app.createPagging(app.table, data, params);
    }
    else {
        app.table.empty.show();
    }
}

// Create List
Frameduz.prototype.createList = function (data, params) {
    const app = this;
    app.list.loader.hide();
    app.list.empty.hide();

    let query = data.query;
    let contents = data.contents;
    let number = data.number;
    let total = data.total;

    if (query != "") {
        app.list.query.html("<code>" + query + "</code>");
    }

    app.list.totalData.html(this.ribuan(total));
    if (total > 0) {
        // Create List Contents
        app.list.contentBody.html("");
        if (app.list.contentItem.length == 0) return;
        for (let rows in contents) {
            let row = app.list.contentItem.clone();
            let result = row.html().replace("{number}", number++);
            json = contents[rows];
            for (key in json) {
                var find = new RegExp("{" + key + "}", "g");
                result = result.replace(find, json[key]);
                row.html(result);
            }
            app.list.contentBody.append(row);
        }
        app.list.content.show();
        if (typeof params.onShow === "function") {
            app.list.content.find("img").each(function (idx, img) {
                const _src = this.getAttribute("_src");
                this.setAttribute("src", _src);
            });
            params.onShow(app.list.content);
        }

        // Create Paging
        app.createPagging(app.list, data, params);
    }
    else {
        app.list.empty.show();
    }
}

// Create Pagging
Frameduz.prototype.createPagging = function (obj, data, params) {
    let page = parseInt(data.page);
    let size = data.size;
    let total = data.total;
    let totalpages = data.totalpages;

    obj.paging.html("");
    if (totalpages > 1) {
        const prev_page = (page > 1) ? page - 1 : 1;
        const next_page = (page < totalpages) ? page + 1 : totalpages;

        const btn_page = obj.pagingItem.html();
        const btn_first = obj.pagingItem.clone().html(btn_page.replace("{page}", "&laquo;")).find("a").attr("page-number", 1).parent();
        const btn_last = obj.pagingItem.clone().html(btn_page.replace("{page}", "&raquo;")).find("a").attr("page-number", totalpages).parent();
        const btn_prev = obj.pagingItem.clone().html(btn_page.replace("{page}", "&lsaquo;")).find("a").attr("page-number", prev_page).parent();
        const btn_next = obj.pagingItem.clone().html(btn_page.replace("{page}", "&rsaquo;")).find("a").attr("page-number", next_page).parent();
        const btn_dots = obj.pagingItem.clone().html(btn_page.replace("{page}", "...")).addClass("disabled");
        // const btn_active = obj.pagingItem.clone().addClass("active").html(btn_page.replace("{page}", page)).find("a").attr("page-number", "").parent();
        const btn_active = obj.pagingItem.clone().html(btn_page.replace("{page}", page)).find("a").attr("page-number", "").addClass("pagination__link--active").parent();

        if (page > 3) {
            obj.paging.append(btn_first);
            obj.paging.append(btn_prev);
            obj.paging.append(btn_dots);
        }

        for (let p = (page - 2); p < page; p++) {
            if (p < 1) continue;
            let pages = obj.pagingItem.clone().html(btn_page.replace("{page}", p)).find("a").attr("page-number", p).parent();
            obj.paging.append(pages);
        }

        obj.paging.append(btn_active);

        for (let p = (page + 1); p < (page + 3); p++) {
            if (p > totalpages) break;
            let pages = obj.pagingItem.clone().html(btn_page.replace("{page}", p)).find("a").attr("page-number", p).parent();
            obj.paging.append(pages);
        }

        if ((page + 2) < totalpages) {
            obj.paging.append(btn_dots);
        }

        if (page < (totalpages - 2)) {
            obj.paging.append(btn_next);
            obj.paging.append(btn_last);
        }

        // obj.paging.show();
        obj.paging.removeClass("hidden");
        obj.paging.find("a[page-number]").on("click", function (event) {
            event.preventDefault();
            const page_number = $(this).attr("page-number");
            if (page_number == "") return;
            if (typeof params.onPage === "function") params.onPage(page_number);
        })

    }

    // obj.pageSize.show();
    obj.pageSize.removeClass("hidden");
    if (obj.pageSize.find("#pagesize").length > 0) {
        obj.pageSize.find("#pagesize").off();
        obj.pageSize.find("#pagesize").on("change", function (event) {
            event.preventDefault();
            const page_size = this.value;
            if (typeof params.onSize === "function") params.onSize(page_size);
        })
    }
}

// Load Form
Frameduz.prototype.loadForm = function (params) {
    const app = this;
    const form_content = app.form.clone();
    form_content.attr("id", "form-input");
    setTimeout(function () {
        app.sendRequest("GET", {
            url: params.url,
            header: params.header,
            data: params.data,
            onSuccess: function (response) {
                // console.log(response);
                const data = response.data;
                if (typeof params.onLoad === "function") params.onLoad(data);
                if (typeof params.onShow === "function") params.onShow(form_content, data);
                form_content.on("submit", function (event) {
                    event.preventDefault();
                    if (typeof params.onSubmit === "function") params.onSubmit(form_content);
                });
            },
            onError: function (error) {
                // console.log(error);
                if (typeof params.onError === "function") {
                    params.onError(error);
                }
                else {
                    alert(error.message);
                }
            }
        });
    }, 500);
}

// Create Form
Frameduz.prototype.createForm = function (form_content, object) {
    $.each(object, (key, val) => form_content.find("span[data-form-object='" + key + "']").replaceWith(val));
}

// Create Progres
Frameduz.prototype.createProgress = function (obj) {
    const progress = $("<div>").addClass("fzprogress");
    const spinner = $("<div>").addClass("spinner");
    for (let i = 1; i <= 12; i++) {
        $("<div>").addClass("bar" + i).appendTo(spinner);
    }
    spinner.appendTo(progress);
    obj.css("position", "relative").prepend(progress);
    return progress;
}

// Create Dialog
Frameduz.prototype.createDialog = function (id) {
    const modal = $(id);
    modal.title = modal.find(".modal-title");
    modal.content = modal.find(".modal-content");
    modal.body = modal.find(".modal-body");
    modal.footer = modal.find(".modal-footer");
    modal.submit = modal.footer.find("button[type='submit']");
    modal.setSize = function (size) {
        modal.find(".modal-dialog").removeClass("modal-sm, modal-lg");
        if (size == "small") {
            modal.find(".modal-dialog").addClass("modal-sm");
        }
        else if (size == "large") {
            modal.find(".modal-dialog").addClass("modal-lg");
        }
    }

    return modal;
}

// Create Form Element
Frameduz.prototype.formel = {
    key: function (id, value) {
        return $("<input>").attr({ type: "hidden", id: id, name: id, value: value });
    },
    input: function (id, type, value) {
        return $("<input>").attr({ type: type, id: id, name: id, value: value });
    },
    textArea: function (id, value) {
        return $("<textarea>").attr({ id: id, name: id, rows: 5 }).html(value);
    },
    select: function (id, data, value) {
        var group = $("<div>");
        var select = $("<select>").attr({ id: id, name: id });
        var value = (value instanceof Array) ? value : [value];
        $.each(data, function (key, val) {
            const desc = val.desc || "";
            const img = val.img || "";
            var option = $("<option>").attr({ value: key, description: desc, image: img, selected: ($.inArray(key, value) != -1) }).text(val.text)
            select.append(option);
        });
        group.append(select);
        return group.children();
    },
    radio: function (id, data, value) {
        var group = $("<div>");
        $.each(data, function (key, val) {
            var radio = $("<input>").attr({ type: "radio", id: id, name: id, value: key, checked: (key == value) });
            var label = $("<label>").attr("style", "color: #000; cursor: pointer; margin: 10px;").append(radio).append(" " + val.text);
            group.append(label);
        });
        return group.children();
    },
    checkbox: function (id, data, value) {
        var group = $("<div>");
        var value = (value instanceof Array) ? value : [value];
        $.each(data, function (key, val) {
            var check = $("<input>").attr({ type: "checkbox", id: id, name: id, value: val, checked: ($.inArray(val, value) != -1) });
            var label = $("<label>").attr("style", "color: #000; cursor: pointer; margin: 5px;").append(check).append(" " + val);
            group.append(label);
        });
        return group.children();
    },
    uploadImage: function (id, image, mimes, desc) {
        var group = $("<div>");
        var preview = $("<div>").attr({ class: "image-preview" }).html('<img src="' + image + '" class="img-responsive thumbnail" alt="image" width="100%">');
        var file = $("<input>").attr({ type: "file", id: id, name: id, class: "file-image", style: "display: none;", accept: mimes });
        var button = $("<label>").attr({ for: id, class: "btn btn-block btn-sm btn-dark", style: "cursor: pointer; margin-top: 10px;" }).html("UPLOAD");
        var desc = $("<small>").attr({ class: "help-block" }).html(desc);
        group.append(preview).append(file).append(button).append(desc);
        return group.html();
    },
    imagePreview: function (obj) {
        if (obj.files && obj.files[0]) {
            var reader = new FileReader();
            var preview = $(obj).siblings(".image-preview");
            reader.onload = function (e) {
                preview.html('<img src="' + e.target.result + '" class="img-responsive thumbnail" alt="image" width="100%">');
            };
            reader.readAsDataURL(obj.files[0]);
        }
    },
}

// Active Menu by Url
Frameduz.prototype.activeMenu = function (obj) {
    var macthUrl = [];
    var pageUrl = window.location.toString();
    $(obj).each(function (k, v) {
        if (pageUrl.match(this.href)) macthUrl.push(this);
    });
    var element = $(macthUrl[macthUrl.length - 1]);
    element.addClass("active").parent().addClass("active").parent().addClass("show").parent().addClass("active open");
}

// Ajax Request json, form-urlencoded, query params
Frameduz.prototype.sendRequest = function (type, params) {
    $.ajax({
        type: type,
        url: params.url,
        data: params.data,
        headers: params.header,
        dataType: "json",
        async: false,
        success: params.onSuccess,
        error: function (e) {
            // console.log(e);
            if (e.status !== 200) {
                if (typeof params.onError === "function") params.onError(e.responseJSON);
            }
        }
    });
}

// Ajax Request Multipart (FormData/Files)
Frameduz.prototype.sendMultipart = function (type, params) {
    $.ajax({
        type: type,
        url: params.url,
        enctype: "multipart/form-data",
        data: new FormData(params.data),
        headers: params.header,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        datatype: "json",
        xhr: function () {
            var jxhr = null;
            if (window.ActiveXObject)
                jxhr = new window.ActiveXObject("Microsoft.XMLHTTP");
            else
                jxhr = new window.XMLHttpRequest();

            if (jxhr.upload) {
                jxhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        let percent = Math.round((evt.loaded / evt.total) * 100);
                        if (typeof params.onProgress === "function") params.onProgress(percent);
                    }
                }, false);
            }
            return jxhr;
        },
        success: params.onSuccess,
        error: function (e) {
            // console.log(e);
            if (e.status !== 200) {
                if (typeof params.onError === "function") params.onError(e.responseJSON);
            }
        }
    });
}

/**
 * Common Library
 */
Frameduz.prototype.validate = function (e) {
    /**
     * 8 = backspace
     * 9 = tab
     * 13 = enter
     * 46 = delete
     * 188 = comma
     * 190 = period(titik)
     * 37 = left arrow
     * 39 = right arrow
     * ref. https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
     */
    if (e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 13 || e.keyCode == 46 || e.keyCode == 188 || e.keyCode == 190 || e.keyCode == 37 || e.keyCode == 39) return true;
    else if (e.keyCode >= 48 && e.keyCode <= 57) return true;
    else return false;
}

Frameduz.prototype.ribuan = function (s) {
    var reverse = s.toString().split("").reverse().join(""),
        ribuan = reverse.match(/\d{1,3}/gi);
    ribuan = ribuan.join(".").split("").reverse().join("");
    return ribuan;
}

/**
 * JQuery Prototype convert to json object
 */

function serializeObject() {

    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push": /^$/,
            "fixed": /^\d+$/,
            "named": /^[a-zA-Z0-9_]+$/
        };

    this.build = function (base, key, value) {
        base[key] = value;
        return base;
    };

    this.push_counter = function (key) {
        if (push_counters[key] === undefined) {
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function () {
        // Skip invalid keys
        if (!patterns.validate.test(this.name)) { return; }
        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while ((k = keys.pop()) !== undefined) {
            // Adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
            // Push
            if (k.match(patterns.push)) {
                merge = self.build([], self.push_counter(reverse_key), merge);
            }
            // Fixed
            else if (k.match(patterns.fixed)) {
                merge = self.build([], k, merge);
            }
            // Named
            else if (k.match(patterns.named)) {
                merge = self.build({}, k, merge);
            }
        }
        json = $.extend(true, json, merge);
    });

    return json;
};

// Add function in Jquery
$.fn.serializeObject = serializeObject;