<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Config.php
 */

namespace core;

class Config {
    
    private static $config = [];
    public static function Load($file) {
        if (!isset(self::$config[$file])) {
            $data = require_once CONFIG . $file . '.php';
            self::$config[$file] = $data;
        }

        return self::$config[$file];
    }

}
?>