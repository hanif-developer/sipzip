<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : RestApi.php
 */

namespace core;

class RestApi extends Frameduz {

    public function __construct() {
        parent::__construct();
        $this->settings = (object) $this->getSettingList();
    }

    protected function showResponse(array $responseMessage) {
        Helper::showResponse($responseMessage);
    }

    public function showError(string $message, $code = 404) {
        Helper::errorResponse($message, $code);
    }
    
}
?>