<?php 
namespace oauth;
use core\Helper;
use app\backend\repository\tabel\{TbSatker};

class UserAuth extends \core\OAuth {

    public function __construct() {
        parent::__construct('user_auth');
    }

    // Overide untuk proses login
    public function setValidate() {
        $this->tbSatker = new TbSatker();
        $fields = $this->getFields(['username' => '', 'password' => '']);
        list($username, $password) = array_values($fields);
        $params = [
            'id_satker' => $username,
            'password' => $this->tbSatker->encrypt($password),
        ];

        list($conditions, $params) = $this->tbSatker->createQueryParams($params, '=');
        $where = ($params) ? ' WHERE '.implode(' AND ', $conditions) : '';
        $query = 'SELECT * FROM `tb_satker` satker'.$where;
        $result = $this->tbSatker->getQuery($query, $params);
        if ($result['count'] > 0) {
            $data = current($result['value']);
            $data['user'] = $data['nama_satker'];
            // $data['query'] = $result['query'];
            
            $jwt_access_token = $this->jwtToken($data, $this->secret_key, '+30 days');
            $this->setCookieToken($jwt_access_token);
            Helper::$responseMessage['code'] = 200;
            Helper::$responseMessage['status'] = 'success';
            Helper::$responseMessage['message'] = 'Login success ..';
            Helper::$responseMessage['data'] = $data;
            $this->showResponse(Helper::$responseMessage);
        }
        else {
            Helper::$responseMessage['code'] = 400;
            Helper::$responseMessage['status'] = 'error';
            Helper::$responseMessage['message'] = 'Login failed !!';
            // Helper::$responseMessage['query'] = $result['query'];
            $this->showResponse(Helper::$responseMessage);
        }
    }

    /** Versi Simple */
    // public function setValidate() {
    //     $fields = $this->getFields(['username' => '', 'password' => '']);
    //     Helper::$responseMessage['data'] = $fields;
    //     list($username, $password) = array_values($fields);
    //     if ($username == 'admin' && $password == 'admin') {
    //         $jwt_access_token = $this->jwtToken(
    //             ['user' => 'Administrator', 'email' => 'admin@developer.pekalongankota.go.id'],
    //             $this->secret_key,
    //             '+1 days'
    //         );
            
    //         $this->setCookieToken($jwt_access_token);
    //         Helper::$responseMessage['code'] = 200;
    //         Helper::$responseMessage['status'] = 'success';
    //         Helper::$responseMessage['message'] = 'Login success ..';
    //     }
    //     else {
    //         Helper::$responseMessage['code'] = 400;
    //         Helper::$responseMessage['status'] = 'error';
    //         Helper::$responseMessage['message'] = 'Login failed !!';
    //     }

    //     $this->showResponse(Helper::$responseMessage);
    // }

    // public function mobileValidate() {
    //     $this->register = new Register();
    //     $fields = $this->getFields(['username' => '', 'password' => '']);
    //     list($username, $password) = array_values($fields);
    //     $params = [
    //         'nomer_handphone' => $username,
    //         'password' => $this->register->encrypt($password),
    //     ];

    //     list($conditions, $params) = $this->register->createQueryParams($params, '=');
    //     $where = ($params) ? ' WHERE '.implode(' AND ', $conditions) : '';
    //     $query = 'SELECT * FROM '.$this->register->getTable().$where;
    //     $result = $this->register->getQuery($query, $params);
    //     if ($result['count'] > 0) {
    //         $data = current($result['value']);
    //         $data['access_token'] = $this->jwtToken($data, $this->secret_key, '+30 days');
    //         unset($data['password']);
    //         Helper::$responseMessage['code'] = 200;
    //         Helper::$responseMessage['status'] = 'success';
    //         Helper::$responseMessage['message'] = 'Login success ..';
    //         Helper::$responseMessage['data'] = $data;
    //         $this->showResponse(Helper::$responseMessage);
    //     }
    //     else {
    //         throw new \core\exception\ValidationException('Login Gagal');
    //     }
    // }

}

?>