-- MySQL dump 10.13  Distrib 5.7.35, for Linux (x86_64)
--
-- Host: localhost    Database: db_sipzip
-- ------------------------------------------------------
-- Server version	5.7.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_jawaban`
--

DROP TABLE IF EXISTS `tb_jawaban`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_jawaban` (
  `id_jawaban` varchar(6) NOT NULL DEFAULT '',
  `pertanyaan_id` varchar(5) NOT NULL DEFAULT '',
  `nama_jawaban` varchar(255) NOT NULL DEFAULT '',
  `keterangan_jawaban` varchar(255) NOT NULL DEFAULT '',
  `point_jawaban` double NOT NULL,
  PRIMARY KEY (`id_jawaban`),
  KEY `pertanyaan_id` (`pertanyaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_jawaban`
--

LOCK TABLES `tb_jawaban` WRITE;
/*!40000 ALTER TABLE `tb_jawaban` DISABLE KEYS */;
INSERT INTO `tb_jawaban` VALUES ('A.001','Q.001','Y','Ya, jika Tim telah dibentuk di dalam unit kerja.',1),('A.002','Q.001','T','',0),('A.003','Q.002','A','a. Jika dengan prosedur/mekanisme yang jelas dan mewakili seluruh unsur dalam unit kerja;',1),('A.004','Q.002','B','b. Jika sebagian menggunakan prosedur yang mewakili sebagian besar unsur dalam unit kerja;',0.5),('A.005','Q.002','C','c. Jika tidak di seleksi.',0),('A.006','Q.003','Y','Ya, jika memiliki  rencana kerja pembangunan Zona Integritas.',1),('A.007','Q.003','T','',0),('A.008','Q.004','A','a. Jika semua target-target prioritas relevan dengan tujuan pembangunan WBK/WBBM;',1),('A.009','Q.004','B','b. Jika sebagian target-target prioritas relevan dengan tujuan pembangunan WBK/WBBM;',0.5),('A.010','Q.004','C','c. Jika tidak ada target-target prioritas yang relevan dengan tujuan pembangunan WBK/WBBM.',0),('A.011','Q.005','A','a. Jika telah dilakukan pengelolaan media/aktivitas interaktif yang efektif untuk menginformasikan pembangunan ZI kepada internal dan stakeholder secara berkala;',1),('A.012','Q.005','B','b. Jika pengelolaan media/aktivitas interaktif dilakukan secara terbatas dan tidak secara berkala;',0.5),('A.013','Q.005','C','c. Jika pengelolaan media/aktivitas interaktif belum dilakukan.',0),('A.014','Q.006','A','a. Jika semua kegiatan pembangunan telah dilaksanakan sesuai dengan rencana;',1),('A.015','Q.006','B','b. Jika sebagian besar kegiatan pembangunan telah dilaksanakan sesuai dengan rencana;',0.67),('A.016','Q.006','C','c. Jika sebagian kecil kegiatan pembangunan telah dilaksanakan sesuai dengan rencana;',0.33),('A.017','Q.006','D','d. Jika belum ada kegiatan pembangunan yang dilakukan sesuai dengan rencana.',0),('A.018','Q.007','A','a. Jika monitoring dan evaluasi melibatkan pimpinan dan dilakukan secara berkala;',1),('A.019','Q.007','B','b. Jika monitoring dan evaluasi melibatkan pimpinan tetapi tidak secara berkala;',0.67),('A.020','Q.007','C','c. Jika monitoring dan evaluasi tidak melibatkan pimpinan dan tidak secara berkala;',0.33),('A.021','Q.007','D','d. Jika tidak terdapat monitoring dan evaluasi terhadap pembangunan zona integritas.',0),('A.022','Q.008','A','a. Jika semua catatan/rekomendasi hasil monitoring dan evaluasi tim internal atas persiapan dan pelaksanaan kegiatan Unit WBK/WBBM telah ditindaklanjuti;',1),('A.023','Q.008','B','b. Jika sebagian besar catatan/rekomendasi hasil monitoring dan evaluasi tim internal atas persiapan dan pelaksanaan kegiatan Unit WBK/WBBM telah ditindaklanjuti;',0.67),('A.024','Q.008','C','c. Jika sebagian kecil catatan/rekomendasi hasil monitoring dan evaluasi tim internal atas persiapan dan pelaksanaan kegiatan Unit WBK/WBBM telah ditindaklanjuti;',0.33),('A.025','Q.008','D','d. Jika catatan/rekomendasi hasil monitoring dan evaluasi tim internal atas persiapan dan pelaksanaan kegiatan Unit WBK/WBBM belum ditindaklanjuti.',0),('A.026','Q.009','Y','Ya, jika pimpinan menjadi contoh pelaksanaan nilai-nilai organisasi.',1),('A.027','Q.009','T','',0),('A.028','Q.010','A','a. Jika agen perubahan telah ditetapkan dan berkontribusi terhadap perubahan pada unit kerjanya;',1),('A.029','Q.010','B','b. Jika agen perubahan telah ditetapkan namun belum berkontribusi terhadap perubahan pada unit kerjanya;',0.5),('A.030','Q.010','C','c. Jika belum terdapat agen perubahan.',0),('A.031','Q.011','A','a. Jika telah dilakukan upaya pembangunan budaya kerja dan pola pikir dan mampu mengurangi resistensi atas perubahan;',1),('A.032','Q.011','B','b. Jika telah dilakukan upaya pembangunan budaya kerja dan pola pikir tapi masih terdapat resistensi atas perubahan;',0.5),('A.033','Q.011','C','c. Jika belum terdapat upaya pembangunan budaya kerja dan pola pikir.',0),('A.034','Q.012','A','a. Jika semua anggota terlibat dalam pembangunan Zona Integritas menuju WBK/WBBM (mis: membuat yel-yel, slogan/motto, banner, poster dll ) dan usulan-usulan dari anggota diakomodasikan dalam keputusan;',1),('A.035','Q.012','B','b. Jika sebagian besar anggota terlibat dalam pembangunan Zona Integritas menuju WBK/WBBM (mis: membuat yel-yel, slogan/motto banner, poster dll );',0.67),('A.036','Q.012','C','c. Jika sebagian kecil anggota terlibat dalam pembangunan Zona Integritas menuju WBK/WBBM (mis: membuat yel-yel, slogan/motto banner, poster dll );',0.33),('A.037','Q.012','D','d. Jika belum ada anggota terlibat dalam pembangunan Zona Integritas menuju WBK/WBBM.',0),('A.038','Q.013','A','a. Jika semua SOP unit telah mengacu peta proses bisnis dan juga melakukan inovasi yang selaras;',1),('A.039','Q.013','B','b. Jika semua SOP unit telah mengacu peta proses bisnis;',0.67),('A.040','Q.013','C','c. Jika sebagian SOP unit telah mengacu peta proses bisnis;',0.33),('A.041','Q.013','D','d. Jika belum terdapat SOP unit yang mengacu peta proses bisnis.',0),('A.042','Q.014','A','a. Jika unit telah menerapkan seluruh SOP yang ditetapkan organisasi dan juga melakukan inovasi pada SOP yang diterapkan;',1),('A.043','Q.014','B','b. Jika unit telah menerapkan seluruh SOP yang ditetapkan organisasi;',0.75),('A.044','Q.014','C','c. Jika unit telah menerapkan sebagian besar SOP yang ditetapkan organisasi;',0.5),('A.045','Q.014','D','d. Jika unit telah menerapkan sebagian kecil SOP yang ditetapkan organisasi;',0.25),('A.046','Q.014','E','e. Jika unit belum menerapkan SOP yang telah ditetapkan organisasi.',0),('A.047','Q.015','A','a. Jika seluruh SOP utama telah dievaluasi dan telah ditindaklanjuti berupa perbaikan SOP atau usulan perbaikan SOP;',1),('A.048','Q.015','B','b. Jika sebagian besar SOP utama telah dievaluasi dan telah ditindaklanjuti berupa perbaikan SOP atau usulan perbaikan SOP;',0.75),('A.049','Q.015','C','c. Jika sebagian besar SOP utama telah dievaluasi tetapi belum ditindaklanjuti;',0.5),('A.050','Q.015','D','d. Jika sebagian kecil SOP utama telah dievaluasi;',0.25),('A.051','Q.015','E','e. Jika SOP belum pernah dievaluasi.',0),('A.052','Q.016','A','a. Jika unit memiliki sistem pengukuran kinerja (e-performance/e-sakip) yang menggunakan teknologi informasi dan juga melakukan inovasi;',1),('A.053','Q.016','B','b. Jika unit memiliki sistem pengukuran kinerja (e-performance/e-sakip) yang menggunakan teknologi informasi;',0.5),('A.054','Q.016','C','c. Jika belum memiliki sistem pengukuran kinerja (e-performance/e-sakip) yang menggunakan teknologi informasi.',0),('A.055','Q.017','A','a. Jika unit memiliki operasionalisasi manajemen SDM yang menggunakan teknologi informasi dan juga melakukan inovasi;',1),('A.056','Q.017','B','b. Jika unit memiliki operasionalisasi manajemen SDM yang menggunakan teknologi informasi secara terpusat;',0.5),('A.057','Q.017','C','c. Jika belum menggunakan teknologi informasi dalam operasionalisasi manajemen SDM.',0),('A.058','Q.018','A','a. Jika unit memberikan pelayanan kepada publik dengan menggunakan teknologi informasi terpusat/unit sendiri dan terdapat inovasi;',1),('A.059','Q.018','B','b. Jika unit memberikan pelayanan kepada publik dengan menggunakan teknologi informasi secara terpusat;',0.5),('A.060','Q.018','C','c. Jika belum memberikan pelayanan kepada publik dengan menggunakan teknologi informasi.',0),('A.061','Q.019','A','a. Jika laporan monitoring dan evaluasi terhadap pemanfaatan teknologi informasi dalam pengukuran kinerja unit, operasionalisasi SDM, dan pemberian layanan kepada publik sudah dilakukan secara berkala;',1),('A.062','Q.019','B','b. Jika laporan monitoring dan evaluasi terhadap pemanfaatan teknologi informasi dalam pengukuran kinerja unit, operasionalisasi SDM, dan pemberian layanan kepada publik sudah dilakukan tetapi tidak secara berkala;',0.5),('A.063','Q.019','C','c. Jika tidak terdapat monitoring dan evaluasi terhadap pemanfaatan teknologi informasi dalam pengukuran kinerja unit, operasionalisasi SDM, dan pemberian layanan kepada publik.',0),('A.064','Q.020','A','a. Jika sudah terdapat Pejabat Pengelola Informasi Publik (PPID) yang menyebarkan seluruh informasi yang dapat diakses secara mutakhir dan lengkap;',1),('A.065','Q.020','B','b. Jika sudah terdapat PPID yang menyebarkan sebagian informasi yang dapat diakses secara mutakhir dan lengkap;',0.5),('A.066','Q.020','C','c. Jika belum ada PPID dan belum melakukan penyebaran informasi publik.',0),('A.067','Q.021','A','a. Jika dilakukan monitoring dan evaluasi pelaksanaan kebijakan keterbukaan informasi publik dan telah ditindaklanjuti;',1),('A.068','Q.021','B','b. Jika monitoring dan evaluasi pelaksanaan kebijakan keterbukaan informasi publik telah dilakukan tetapi belum ditindaklanjuti;',0.5),('A.069','Q.021','C','c. Jika monitoring dan evaluasi pelaksanaan kebijakan keterbukaan informasi publik belum dilakukan.',0),('A.070','Q.022','Y','Ya, jika kebutuhan pegawai yang disusun oleh unit kerja mengacu kepada peta jabatan dan hasil analisis beban kerja untuk masing-masing jabatan.',1),('A.071','Q.022','T','',0),('A.072','Q.023','A','a. Jika semua penempatan pegawai hasil rekrutmen murni mengacu kepada kebutuhan pegawai yang telah disusun per jabatan;',1),('A.073','Q.023','B','b. Jika sebagian besar penempatan pegawai hasil rekrutmen murni mengacu kepada kebutuhan pegawai yang telah disusun per jabatan;',0.67),('A.074','Q.023','C','c. Jika sebagian kecil penempatan pegawai hasil rekrutmen murni mengacu kepada kebutuhan pegawai yang telah disusun per jabatan;',0.33),('A.075','Q.023','D','d. Jika penempatan pegawai hasil rekrutmen murni tidak mengacu kepada kebutuhan pegawai yang telah disusun per jabatan.',0),('A.076','Q.024','Y','Ya, jika sudah dilakukan monitoring dan evaluasi terhadap penempatan pegawai hasil rekrutmen untuk memenuhi kebutuhan jabatan dalam organisasi telah memberikan perbaikan terhadap kinerja unit kerja.',1),('A.077','Q.024','T','',0),('A.078','Q.025','Y','Ya, jika dilakukan mutasi pegawai antar jabatan sebagai wujud dari pengembangan karier pegawai.',1),('A.079','Q.025','T','',0),('A.080','Q.026','A','a. Jika semua mutasi pegawai antar jabatan telah memperhatikan kompetensi jabatan dan mengikuti pola mutasi yang telah ditetapkan organisasi dan juga unit kerja memberikan pertimbangan terkait hal ini;',1),('A.081','Q.026','B','b. Jika semua mutasi pegawai antar jabatan telah memperhatikan kompetensi jabatan dan mengikuti pola mutasi yang telah ditetapkan organisasi;',0.75),('A.082','Q.026','C','c. Jika sebagian besar mutasi pegawai antar jabatan telah memperhatikan kompetensi jabatan dan mengikuti pola mutasi yang telah ditetapkan organisasi;',0.5),('A.083','Q.026','D','d. Jika sebagian kecil semua mutasi pegawai antar jabatan telah memperhatikan kompetensi jabatan dan mengikuti pola mutasi yang telah ditetapkan organisasi;',0.25),('A.084','Q.026','E','e. Jika mutasi pegawai antar jabatan belum memperhatikan kompetensi jabatan dan mengikuti pola mutasi yang telah ditetapkan organisasi.',0),('A.085','Q.027','Y','Ya, jika sudah dilakukan monitoring dan evaluasi terhadap kegiatan mutasi yang telah dilakukan dalam kaitannya dengan perbaikan kinerja.',1),('A.086','Q.027','T','',0),('A.087','Q.028','Y','Ya, jika sudah dilakukan Training Need Analysis Untuk pengembangan kompetensi.',1),('A.088','Q.028','T','',0),('A.089','Q.029','A','a. Jika semua rencana pengembangan kompetensi pegawai mempertimbangkan hasil pengelolaan kinerja pegawai;',1),('A.090','Q.029','B','b. Jika sebagian besar rencana pengembangan kompetensi pegawai mempertimbangkan hasil pengelolaan kinerja pegawai;',0.67),('A.091','Q.029','C','c. Jika sebagian kecil rencana pengembangan kompetensi pegawai mempertimbangkan hasil pengelolaan kinerja pegawai;',0.33),('A.092','Q.029','D','d. Jika belum ada rencana pengembangan kompetensi pegawai yang mempertimbangkan hasil pengelolaan kinerja pegawai.',0),('A.093','Q.030','A','a. Jika persentase kesenjangan kompetensi pegawai dengan standar kompetensi yang ditetapkan sebesar <25%;',1),('A.094','Q.030','B','b. Jika persentase kesenjangan kompetensi pegawai dengan standar kompetensi yang ditetapkan sebesar >25%-50%;',0.67),('A.095','Q.030','C','c. Jika sebagian besar kompetensi pegawai dengan standar kompetensi yang ditetapkan untuk masing-masing jabatan >50%-75%;',0.33),('A.096','Q.030','D','d. Jika persentase kesenjangan kompetensi pegawai dengan standar kompetensi yang ditetapkan sebesar >75%-100%.',0),('A.097','Q.031','A','a. Jika seluruh pegawai di Unit Kerja telah memperoleh kesempatan/hak untuk mengikuti diklat maupun pengembangan kompetensi lainnya;',1),('A.098','Q.031','B','b. Jika sebagian besar pegawai di Unit Kerja telah memperoleh kesempatan/hak untuk mengikuti diklat maupun pengembangan kompetensi lainnya;',0.67),('A.099','Q.031','C','c. Jika sebagian kecil pegawai di Unit Kerja telah memperoleh kesempatan/hak untuk mengikuti diklat maupun pengembangan kompetensi lainnya;',0.33),('A.100','Q.031','D','d. Jika belum ada pegawai di Unit Kerja telah memperoleh kesempatan/hak untuk mengikuti diklat maupun pengembangan kompetensi lainnya.',0),('A.101','Q.032','A','a. Jika unit kerja melakukan upaya pengembangan kompetensi kepada seluruh pegawai;',1),('A.102','Q.032','B','b. Jika unit kerja melakukan upaya pengembangan kompetensi kepada sebagian besar pegawai;',0.67),('A.103','Q.032','C','c. Jika unit kerja melakukan upaya pengembangan kompetensi kepada sebagian kecil pegawai;',0.33),('A.104','Q.032','D','d. Jika unit kerja belum melakukan upaya pengembangan kompetensi kepada pegawai.',0),('A.105','Q.033','A','a. Jika monitoring dan evaluasi terhadap hasil pengembangan kompetensi dalam kaitannya dengan perbaikan kinerja telah dilakukan secara berkala;',1),('A.106','Q.033','B','b. Jika monitoring dan evaluasi terhadap hasil pengembangan kompetensi dalam kaitannya dengan perbaikan kinerja telah dilakukan namun tidak secara berkala;',0.5),('A.107','Q.033','C','c. Jika monitoring dan evaluasi terhadap hasil pengembangan kompetensi dalam kaitannya dengan perbaikan kinerja belum dilakukan.',0),('A.108','Q.034','A','a. Jika seluruh penetapan kinerja individu terkait dengan kinerja organisasi serta perjanjian kinerja selaras dengan sasaran kinerja pegawai (SKP);',1),('A.109','Q.034','B','b. Jika sebagian besar penetapan kinerja individu terkait dengan kinerja organisasi;',0.67),('A.110','Q.034','C','c. Jika sebagian kecil penetapan kinerja individu terkait dengan kinerja organisasi;',0.33),('A.111','Q.034','D','d. Jika belum ada penetapan kinerja individu terkait dengan kinerja organisasi.',0),('A.112','Q.035','A','a. Jika seluruh ukuran kinerja individu telah memiliki kesesuaian dengan indikator kinerja individu level diatasnya serta menggambarkan logic model;',1),('A.113','Q.035','B','b. Jika sebagian besar ukuran kinerja individu telah memiliki kesesuaian dengan indikator kinerja individu level diatasnya;',0.67),('A.114','Q.035','C','c. Jika sebagian kecil ukuran kinerja individu telah memiliki kesesuaian dengan indikator kinerja individu level diatasnya;',0.33),('A.115','Q.035','D','d. Jika ukuran kinerja individu belum memiliki kesesuaian dengan indikator kinerja individu level diatasnya.',0),('A.116','Q.036','A','a. Jika pengukuran kinerja individu dilakukan secara bulanan;',1),('A.117','Q.036','B','b. Jika pengukuran kinerja individu dilakukan secara triwulanan;',0.75),('A.118','Q.036','C','c. Jika pengukuran kinerja individu dilakukan secara semesteran;',0.5),('A.119','Q.036','D','d. Jika pengukuran kinerja individu dilakukan secara tahunan;',0.25),('A.120','Q.036','E','e. Jika pengukuran kinerja individu belum dilakukan.',0),('A.121','Q.037','Y','Ya, jika hasil hasil penilaian kinerja individu telah dijadikan dasar untuk pemberian reward (pengembangan karir individu, penghargaan dll).',1),('A.122','Q.037','T','',0),('A.123','Q.038','A','a. Jika unit kerja telah mengimplementasikan seluruh aturan disiplin/kode etik/kode perilaku yang ditetapkan organisasi dan juga membuat inovasi terkait aturan disiplin/kode etik/kode perilaku yang sesuai dengan karakteristik unit kerja;',1),('A.124','Q.038','B','b. Jika unit kerja telah mengimplementasikan seluruh aturan disiplin/kode etik/kode perilaku yang ditetapkan organisasi;',0.67),('A.125','Q.038','C','c. Jika unit kerja telah mengimplementasikan sebagian aturan disiplin/kode etik/kode perilaku yang ditetapkan organisasi;',0.33),('A.126','Q.038','D','d. Jika unit kerja belum mengimplementasikan aturan disiplin/kode etik/kode perilaku yang ditetapkan organisasi.',0),('A.127','Q.039','A','a. Jika data informasi kepegawaian unit kerja dapat diakses oleh pegawai dan dimutakhirkan setiap ada perubahan data pegawai;',1),('A.128','Q.039','B','b. Jika data informasi kepegawaian unit kerja dapat diakses oleh pegawai dan dimutakhirkan namun secara berkala;',0.5),('A.129','Q.039','C','c. Jika data informasi kepegawaian unit kerja belum dimutakhirkan.',0),('A.130','Q.040','A','a. Jika seluruh pimpinan unit kerja terlibat dalam penyusunan perencanaan;',1),('A.131','Q.040','B','b. Jika sebagian pimpinan unit kerja terlibat dalam penyusunan perencanaan;',0.5),('A.132','Q.040','C','c. Jika tidak ada keterlibatan pimpinan dalam penyusunan perencanaan.',0),('A.133','Q.041','A','a. Jika seluruh pimpinan unit kerja terlibat dalam penyusunan perjanjian kinerja;',1),('A.134','Q.041','B','b. Jika sebagian pimpinan unit kerja terlibat dalam penyusunan perjanjian kinerja;',0.5),('A.135','Q.041','C','c. Jika tidak ada keterlibatan pimpinan dalam penyusunan perjanjian kinerja.',0),('A.136','Q.042','A','a. Jika seluruh pimpinan unit kerja terlibat dalam pemantauan pencapaian kinerja dan menindaklanjuti hasil pemantauan;',1),('A.137','Q.042','B','b. Jika seluruh pimpinan unit kerja terlibat dalam pemantauan pencapaian kinerja tetapi tidak ada tindak lanjut hasil pemantauan;',0.67),('A.138','Q.042','C','c. Jika sebagian pimpinan unit kerja terlibat dalam pemantauan pencapaian kinerja;',0.33),('A.139','Q.042','D','d. Jika tidak ada keterlibatan pimpinan dalam memantau pencapaian kinerja.',0),('A.140','Q.043','Y','Ya, jika unit kerja memiliki dokumen perencanaan lengkap.',1),('A.141','Q.043','T','',0),('A.142','Q.044','Y','Ya, jika perencanaan telah berorientasi hasil.',1),('A.143','Q.044','T','',0),('A.144','Q.045','Y','Ya, jika unit kerja memiliki IKU.',1),('A.145','Q.045','T','',0),('A.146','Q.046','A','a. Jika seluruh indikator kinerja unit kerja telah SMART;',1),('A.147','Q.046','B','b. Jika sebagian besar indikator kinerja unit kerja telah SMART;',0.67),('A.148','Q.046','C','c. Jika sebagian kecil indikator kinerja unit kerja telah SMART;',0.33),('A.149','Q.046','D','d. Jika belum ada indikator kinerja unit kerja yang SMART.',0),('A.150','Q.047','Y','Ya, jika unit kerja telah menyusun laporan kinerja tepat waktu.',1),('A.151','Q.047','T','',0),('A.152','Q.048','A','a. Jika seluruh pelaporan kinerja telah memberikan informasi tentang kinerja;',1),('A.153','Q.048','B','b. Jika sebagian pelaporan kinerja belum memberikan informasi tentang kinerja;',0.5),('A.154','Q.048','C','c. Jika seluruh pelaporan kinerja belum memberikan informasi tentang kinerja.',0),('A.155','Q.049','Y','Ya, jika terdapat upaya peningkatan kapasitas SDM yang menangani akuntabilitas kinerja.',1),('A.156','Q.049','T','',0),('A.157','Q.050','A','a. Jika pengelolaan akuntabilitas kinerja dilaksanakan oleh seluruh SDM yang kompeten;',1),('A.158','Q.050','B','b. Jika pengelolaan akuntabilitas kinerja dilaksanakan oleh sebagian SDM yang kompeten;',0.5),('A.159','Q.050','C','c. Jika pengelolaan akuntabilitas kinerja belum dilaksanakan oleh seluruh SDM yang kompeten.',0),('A.160','Q.051','A','a. Jika public campaign telah dilakukan secara berkala;',1),('A.161','Q.051','B','b. Jika public campaign dilakukan tidak secara berkala;',0.5),('A.162','Q.051','C','c. Jika belum dilakukan public campaign.',0),('A.163','Q.052','A','a. Jika Unit Pengendalian Gratifikasi, pengendalian gratifikasi telah menjadi bagian dari prosedur;',1),('A.164','Q.052','B','b. Jika Unit Pengendalian Gratifikasi, upaya pengendalian gratifikasi telah mulai dilakukan;',0.67),('A.165','Q.052','C','c. Jika telah membentuk Unit Pengendalian Gratifikasi tetapi belum terdapat prosedur pengendalian;',0.33),('A.166','Q.052','D','d. Jika belum memiliki Unit Pengendalian Gratifikasi.',0),('A.167','Q.053','A','a. Jika unit kerja membangun seluruh lingkungan pengendalian sesuai dengan yang ditetapkan organisasi dan juga membuat inovasi terkait lingkungan pengendalian yang sesuai dengan karakteristik unit kerja;',1),('A.168','Q.053','B','b. Jika unit kerja membangun seluruh lingkungan pengendalian sesuai dengan yang ditetapkan organisasi;',0.75),('A.169','Q.053','C','c. Jika unit kerja membangun sebagian besar lingkungan pengendalian sesuai dengan yang ditetapkan organisasi;',0.5),('A.170','Q.053','D','d. Jika unit kerja membangun sebagian kecil lingkungan pengendalian sesuai dengan yang ditetapkan organisasi;',0.25),('A.171','Q.053','E','e. Jika unit kerja belum membangun lingkungan pengendalian.',0),('A.172','Q.054','A','a. Jika unit kerja melakukan penilaian risiko atas seluruh pelaksanaan kebijakan sesuai dengan yang ditetapkan organisasi dan juga membuat inovasi terkait lingkungan pengendalian yang sesuai dengan karakteristik unit kerja;',1),('A.173','Q.054','B','b. Jika unit kerja melakukan penilaian risiko atas seluruh pelaksanaan kebijakan sesuai dengan yang ditetapkan organisasi;',0.75),('A.174','Q.054','C','c. Jika melakukan penilaian risiko atas sebagian besar pelaksanaan kebijakan sesuai dengan yang ditetapkan organisasi;',0.5),('A.175','Q.054','D','d. Jika melakukan penilaian risiko atas sebagian kecil pelaksanaan kebijakan sesuai dengan yang ditetapkan organisasi;',0.25),('A.176','Q.054','E','e. Jika unit kerja belum melakukan penilaian resiko.',0),('A.177','Q.055','A','a. Jika unit kerja melakukan kegiatan pengendalian untuk meminimalisir resiko sesuai dengan yang ditetapkan organisasi dan juga membuat inovasi terkait kegiatan pengendalian untuk meminimalisir resiko yang sesuai dengan karakteristik unit kerja;',1),('A.178','Q.055','B','b. Jika unit kerja melakukan kegiatan pengendalian untuk meminimalisir resiko sesuai dengan yang ditetapkan organisasi;',0.5),('A.179','Q.055','C','c. Jika unit kerja belum melakukan kegiatan pengendalian untuk meminimalisir resiko.',0),('A.180','Q.056','A','a. Jika SPI telah diinformasikan dan dikomunikasikan kepada seluruh pihak terkait;',1),('A.181','Q.056','B','b. Jika SPI telah diinformasikan dan dikomunikasikan kepada sebagian pihak terkait;',0.5),('A.182','Q.056','C','c. Jika SPI belum diinformasikan dan dikomunikasikan kepada pihak terkait. ',0),('A.183','Q.057','A','a. Jika unit kerja mengimplementasikan seluruh kebijakan pengaduan masyarakat sesuai dengan yang ditetapkan organisasi dan juga membuat inovasi terkait pengaduan masyarakat yang sesuai dengan karakteristik unit kerja;',1),('A.184','Q.057','B','b. Jika unit kerja telah mengimplementasikan seluruh kebijakan pengaduan masyarakat sesuai dengan yang ditetapkan organisasi;',0.5),('A.185','Q.057','C','c. Jika unit kerja belum mengimplementasikan kebijakan pengaduan masyarakat.',0),('A.186','Q.058','A','a. Jika penanganan pengaduan masyarakat dimonitoring dan evaluasi secara berkala;',1),('A.187','Q.058','B','b. Jika penanganan pengaduan masyarakat dimonitoring dan evaluasi tetapi tidak secara berkala;',0.5),('A.188','Q.058','C','c. Jika penanganan pengaduan masyarakat belum di monitoring dan evaluasi.',0),('A.189','Q.059','A','a. Jika seluruh hasil evaluasi atas penanganan pengaduan telah ditindaklanjuti oleh unit kerja;',1),('A.190','Q.059','B','b. Jika sebagian hasil evaluasi atas penanganan pengaduan telah ditindaklanjuti oleh unit kerja;',0.5),('A.191','Q.059','C','c. Jika hasil evaluasi atas penanganan pengaduan belum ditindaklanjuti.',0),('A.192','Q.060','Y','Ya, jika Whistle Blowing System telah di internalisasi di unit kerja.',1),('A.193','Q.060','T','',0),('A.194','Q.061','A','a. Jika unit kerja menerapkan seluruh kebijakan Whistle Blowing System sesuai dengan yang ditetapkan organisasi dan juga membuat inovasi terkait pelaksanaan Whistle Blowing System yang sesuai dengan karakteristik unit kerja;',1),('A.195','Q.061','B','b. Jika unit kerja menerapkan kebijakan Whistle Blowing System sesuai dengan yang ditetapkan organisasi ;',0.5),('A.196','Q.061','C','c. Jika unit kerja belum menerapkan kebijakan Whistle Blowing System.',0),('A.197','Q.062','A','a. Jika penerapan Whistle Blowing System dimonitoring dan evaluasi secara berkala;',1),('A.198','Q.062','B','b. Jika penerapan Whistle Blowing System dimonitoring dan evaluasi tidak secara berkala;',0.5),('A.199','Q.062','C','c. Jika penerapan Whistle Blowing System belum di monitoring dan evaluasi.',0),('A.200','Q.063','A','a. Jika seluruh hasil evaluasi atas penerapan Whistle Blowing System telah ditindaklanjuti oleh unit kerja;',1),('A.201','Q.063','B','b. Jika sebagian hasil evaluasi atas penerapan Whistle Blowing System telah ditindaklanjuti oleh unit kerja;',0.5),('A.202','Q.063','C','c. Jika hasil evaluasi atas penerapan Whistle Blowing System belum ditindaklanjuti.',0),('A.203','Q.064','A','a. Jika sudah terdapat identifikasi/pemetaan benturan kepentingan tetapi pada seluruh tugas fungsi utama;',1),('A.204','Q.064','B','b. Jika sudah terdapat identifikasi/pemetaan benturan kepentingan tetapi pada sebagian besar tugas fungsi utama;',0.67),('A.205','Q.064','C','c. Jika sudah terdapat identifikasi/pemetaan benturan kepentingan tetapi pada sebagian kecil tugas fungsi utama;',0.33),('A.206','Q.064','D','d. Jika belum terdapat identifikasi/pemetaan benturan kepentingan dalam tugas fungsi utama.',0),('A.207','Q.065','A','a. Jika penanganan Benturan Kepentingan disosialiasikan/diinternalisasikan ke seluruh layanan;',1),('A.208','Q.065','B','b. Jika penanganan Benturan Kepentingan disosialiasikan/diinternalisasikan ke sebagian besar layanan;',0.67),('A.209','Q.065','C','c. Jika penanganan Benturan Kepentingan disosialiasikan/diinternalisasikan ke sebagian kecil layanan;',0.33),('A.210','Q.065','D','d. Jika penanganan Benturan Kepentingan belum disosialiasikan/diinternalisasikan ke seluruh layanan.',0),('A.211','Q.066','A','a. Jika penanganan Benturan Kepentingan diimplementasikan ke seluruh layanan;',1),('A.212','Q.066','B','b. Jika penanganan Benturan Kepentingan diimplementasikan ke sebagian besar layanan;',0.67),('A.213','Q.066','C','c. Jika penanganan Benturan Kepentingan diimplementasikan ke sebagian kecil layanan;',0.33),('A.214','Q.066','D','d. Jika penanganan Benturan Kepentingan belum diimplementasikan ke seluruh layanan.',0),('A.215','Q.067','A','a. Jika penanganan Benturan Kepentingan dievaluasi secara berkala oleh unit kerja;',1),('A.216','Q.067','B','b. Jika penanganan Benturan Kepentingan dievaluasi tetapi tidak secara berkala oleh unit kerja;',0.5),('A.217','Q.067','C','c. Jika penanganan Benturan Kepentingan belum dievaluasi oleh unit kerja.',0),('A.218','Q.068','A','a. Jika seluruh hasil evaluasi atas Penanganan Benturan Kepentingan telah ditindaklanjuti oleh unit kerja;',1),('A.219','Q.068','B','b. Jika sebagian hasil evaluasi atas Penanganan Benturan Kepentingan telah ditindaklanjuti oleh unit kerja;',0.5),('A.220','Q.068','C','c. Jika belum ada hasil evaluasi atas Penanganan Benturan Kepentingan yang ditindaklanjuti unit kerja.',0),('A.221','Q.069','A','a. Jika unit kerja memiliki kebijakan standar pelayanan yang ditetapkan organisasi dan juga membuat inovasi terkait standar pelayanan yang sesuai dengan karakteristik unit kerja;',1),('A.222','Q.069','B','b. Jika unit kerja memiliki kebijakan standar pelayanan yang ditetapkan organisasi;',0),('A.223','Q.069','C','c. Jika unit kerja belum memiliki kebijakan standar pelayanan.',0.5),('A.224','Q.070','A','a. Jika unit kerja memaklumatkan seluruh standar pelayanan sesuai dengan yang ditetapkan organisasi dan juga membuat inovasi terkait maklumat standar pelayanan yang sesuai dengan karakteristik unit kerja;',1),('A.225','Q.070','B','b. Jika unit kerja memaklumatkan seluruh standar pelayanan sesuai dengan yang ditetapkan organisasi;',0.75),('A.226','Q.070','C','c. Jika unit kerja memaklumatkan sebagian besar standar pelayanan sesuai dengan yang ditetapkan organisasi;',0.5),('A.227','Q.070','D','d. Jika unit kerja telah memaklumatkan sebagian kecil standar pelayanan sesuai dengan yang ditetapkan organisasi;',0.25),('A.228','Q.070','E','e. Jika belum terdapat standar pelayanan yang telah dimaklumatkan.',0),('A.229','Q.071','A','a. Jika unit kerja menerapkan seluruh SOP sesuai dengan yang ditetapkan organisasi dan juga membuat inovasi terkait SOP yang sesuai dengan karakteristik unit kerja;',1),('A.230','Q.071','B','b. Jika unit kerja menerapkan seluruh SOP sesuai dengan yang ditetapkan organisasi;',0.75),('A.231','Q.071','C','c. Jika unit kerja menerapkan sebagian besar SOP sesuai dengan yang ditetapkan organisasi;',0.5),('A.232','Q.071','D','d. Jika unit kerja menerapkan sebagian kecil SOP sesuai dengan yang ditetapkan organisasi;',0.25),('A.233','Q.071','E','e. Jika unit kerja belum mempunyai SOP tentang pelaksanaan standar pelayanan.',0),('A.234','Q.072','A','a. Jika unit kerja melakukan reviu dan perbaikan atas standar pelayanan dan SOP sesuai dengan yang ditetapkan organisasi dan juga unit kerja berinisiatif melakukan reviu dan perbaikan atas standar pelayanan dan SOP;',1),('A.235','Q.072','B','b. Jika unit kerja melakukan reviu dan perbaikan atas standar pelayanan dan SOP sesuai dengan yang ditetapkan organisasi;',0.5),('A.236','Q.072','C','c. Jika unit kerja belum melakukan reviu dan perbaikan atas standar pelayanan dan SOP.',0),('A.237','Q.073','A','a. Jika sudah terdapat sosialisasi/pelatihan dalam upaya penerapan budaya pelayanan prima pada seluruh pegawai yang memberikan pelayanan;',1),('A.238','Q.073','B','b. Jika sudah terdapat sosialisasi/pelatihan dalam upaya penerapan budaya pelayanan prima pada sebagian besar pegawai yang memberikan pelayanan;',0.67),('A.239','Q.073','C','c. Jika sudah terdapat sosialisasi/pelatihan dalam upaya penerapan budaya pelayanan prima pada sebagian kecil pegawai yang memberikan pelayanan;',0.33),('A.240','Q.073','D','d. Jika belum terdapat sosialisasi/pelatihan dalam upaya penerapan Budaya Pelayanan Prima.',0),('A.241','Q.074','A','a. Jika informasi pelayanan dapat diakses melalui berbagai media (misal: papan pengumuman, website, media sosial, media cetak, media televisi, radio dsb);',1),('A.242','Q.074','B','b. Jika informasi pelayanan dapat diakses melalui beberapa media (misal: papan pengumuman, selebaran, dsb);',0.5),('A.243','Q.074','C','c. Jika informasi pelayanan belum dapat diakses melalui berbagai media.',0),('A.244','Q.075','A','a. Jika telah terdapat sistem sanksi/reward bagi pelaksana layanan serta pemberian kompensasi kepada penerima layanan bila layanan tidak sesuai standar dan sudah diimplementasikan;',1),('A.245','Q.075','B','b. Jika telah terdapat sistem sanksi/reward bagi pelaksana layanan serta pemberian kompensasi kepada penerima layanan bila layanan tidak sesuai standar ada namun belum diimplementasikan;',0.5),('A.246','Q.075','C','c. Jika belum terdapat sistem sanksi/reward bagi pelaksana layanan serta pemberian kompensasi kepada penerima layanan bila layanan tidak sesuai standar.',0),('A.247','Q.076','A','a. Jika seluruh pelayanan sudah dilakukan secara terpadu/terintegrasi;',1),('A.248','Q.076','B','b. Jika sebagian besar pelayanan sudah dilakukan secara terpadu/terintegrasi;',0.67),('A.249','Q.076','C','c. Jika sebagian kecil pelayanan sudah dilakukan secara terpadu/terintegrasi;',0.33),('A.250','Q.076','D','d. Jika tidak ada pelayanan yang dilakukan secara terpadu/terintegrasi. ',0),('A.251','Q.077','A','a. Jika unit kerja telah memiliki inovasi pelayanan yang berbeda dengan unit kerja lain dan mendekatkan pelayanan dengan masyarakat serta telah direplikasi;',1),('A.252','Q.077','B','b. Jika unit kerja telah memiliki inovasi pelayanan yang berbeda dengan unit kerja lain dan mendekatkan pelayanan dengan masyarakat;',0.75),('A.253','Q.077','C','c. Jika unit kerja memiliki inovasi yang merupakan replikasi dan pengembangan dari inovasi yang sudah ada;',0.5),('A.254','Q.077','D','d. Jika unit kerja telah memiliki inovasi akan tetapi merupakan pelaksanaan inovasi dari instansi pemerintah;',0.25),('A.255','Q.077','E','e. Jika unit kerja belum memiliki inovasi pelayanan.',0),('A.256','Q.078','A','a. Jika survey kepuasan masyarakat terhadap pelayanan dilakukan secara berkala;',1),('A.257','Q.078','B','b. Jika survey kepuasan masyarakat terhadap pelayanan tidak berkala;',0.5),('A.258','Q.078','C','c. Jika belum ada survey kepuasan masyarakat terhadap pelayanan.',0),('A.259','Q.079','A','a. Jika hasil survei kepuasan masyarakat dapat diakses melalui berbagai media (misal: papan pengumuman, website, media sosial, media cetak, media televisi, radio dsb);',1),('A.260','Q.079','B','b. Jika hasil survei kepuasan masyarakat dapat diakses melalui beberapa media (misal: papan pengumuman, selebaran, dsb);',0.5),('A.261','Q.079','C','c. Jika hasil survei kepuasan masyarakat belum dapat diakses melalui berbagai media.',0),('A.262','Q.080','A','a. Jika dilakukan tindak lanjut atas seluruh hasil survey kepuasan masyarakat;',1),('A.263','Q.080','B','b. Jika dilakukan tindak lanjut atas sebagian besar hasil survey kepuasan masyarakat;',0.67),('A.264','Q.080','C','c. Jika dilakukan tindak lanjut atas sebagian kecil hasil survey kepuasan masyarakat;',0.33),('A.265','Q.080','D','d. Jika belum dilakukan tindak lanjut atas hasil survey kepuasan masyarakat.',0);
/*!40000 ALTER TABLE `tb_jawaban` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_kategori`
--

DROP TABLE IF EXISTS `tb_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_kategori` (
  `id_kategori` varchar(5) NOT NULL DEFAULT '',
  `nama_kategori` varchar(255) NOT NULL DEFAULT '',
  `point_kategori` double NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_kategori`
--

LOCK TABLES `tb_kategori` WRITE;
/*!40000 ALTER TABLE `tb_kategori` DISABLE KEYS */;
INSERT INTO `tb_kategori` VALUES ('K.01','Komponen Pengungkit',60),('K.02','Komponen Hasil',40);
/*!40000 ALTER TABLE `tb_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_komponen`
--

DROP TABLE IF EXISTS `tb_komponen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_komponen` (
  `id_komponen` varchar(6) NOT NULL DEFAULT '',
  `kategori_id` varchar(5) NOT NULL DEFAULT '',
  `nama_komponen` varchar(255) NOT NULL DEFAULT '',
  `point_komponen` double NOT NULL,
  PRIMARY KEY (`id_komponen`),
  KEY `kategori_id` (`kategori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_komponen`
--

LOCK TABLES `tb_komponen` WRITE;
/*!40000 ALTER TABLE `tb_komponen` DISABLE KEYS */;
INSERT INTO `tb_komponen` VALUES ('C.001','K.01','Manajemen Perubahan',8),('C.002','K.01','Penataan Tatalaksana',7),('C.003','K.01','Penataan Sistem Manajemen SDM',10),('C.004','K.01','Penguatan Akuntabilitas',10),('C.005','K.01','Penguatan Pengawasan',15),('C.006','K.01','Peningkatan Kualitas Pelayanan',10),('C.007','K.02','Pemerintah Yang Bersih dan Bebas KKN',20),('C.008','K.02','Kualitas Pelayanan Publik',20);
/*!40000 ALTER TABLE `tb_komponen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_penilaian`
--

DROP TABLE IF EXISTS `tb_penilaian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_penilaian` (
  `id_penilaian` varchar(6) NOT NULL DEFAULT '',
  `komponen_id` varchar(5) NOT NULL DEFAULT '',
  `nama_penilaian` varchar(255) NOT NULL DEFAULT '',
  `point_penilaian` double NOT NULL,
  PRIMARY KEY (`id_penilaian`),
  KEY `komponen_id` (`komponen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_penilaian`
--

LOCK TABLES `tb_penilaian` WRITE;
/*!40000 ALTER TABLE `tb_penilaian` DISABLE KEYS */;
INSERT INTO `tb_penilaian` VALUES ('P.001','C.001','Tim Kerja',1),('P.002','C.001','Rencana Pembangunan Zona Integritas',2),('P.003','C.001','Pemantauan dan Evaluasi Pembangunan WBK/WBBM',2),('P.004','C.001','Perubahan pola pikir dan budaya kerja',3),('P.005','C.002','Prosedur operasional tetap (SOP) kegiatan utama',2),('P.006','C.002','E-Office',4),('P.007','C.002','Keterbukaan Informasi Publik',1),('P.008','C.003','Perencanaan kebutuhan pegawai sesuai dengan kebutuhan organisasi',0.5),('P.009','C.003','Pola Mutasi Internal',1),('P.010','C.003','Pengembangan pegawai berbasis kompetensi',2.5),('P.011','C.003','Penetapan kinerja individu',4),('P.012','C.003','Penegakan aturan disiplin/kode etik/kode perilaku pegawai',1.5),('P.013','C.003','Sistem Informasi Kepegawaian',0.5),('P.014','C.004','Keterlibatan pimpinan',5),('P.015','C.004','Pengelolaan Akuntabilitas Kinerja',5),('P.016','C.005','Pengendalian Gratifikasi',3),('P.017','C.005','Penerapan SPIP',3),('P.018','C.005','Pengaduan Masyarakat',3),('P.019','C.005','Whistle-Blowing System',3),('P.020','C.005','Penanganan Benturan Kepentingan',3),('P.021','C.006','Standar Pelayanan',3),('P.022','C.006','Budaya Pelayanan Prima',4),('P.023','C.006','Penilaian kepuasan terhadap pelayanan',3),('P.024','C.007','Nilai Survey Persepsi Korupsi (Survei Eksternal)',15),('P.025','C.007','Persentase temuan hasil pemeriksaan (Internal dan eksternal) yang ditindaklanjuti',5),('P.026','C.008','Nilai Persepsi Kualitas Pelayanan (Survei Eksternal)',20);
/*!40000 ALTER TABLE `tb_penilaian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pertanyaan`
--

DROP TABLE IF EXISTS `tb_pertanyaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pertanyaan` (
  `id_pertanyaan` varchar(6) NOT NULL DEFAULT '',
  `penilaian_id` varchar(5) NOT NULL DEFAULT '',
  `nama_pertanyaan` varchar(255) NOT NULL DEFAULT '',
  `point_pertanyaan` double DEFAULT NULL,
  PRIMARY KEY (`id_pertanyaan`),
  KEY `penilaian_id` (`penilaian_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pertanyaan`
--

LOCK TABLES `tb_pertanyaan` WRITE;
/*!40000 ALTER TABLE `tb_pertanyaan` DISABLE KEYS */;
INSERT INTO `tb_pertanyaan` VALUES ('Q.001','P.001','Apakah unit kerja telah membentuk tim untuk melakukan pembangunan Zona Integritas?',1),('Q.002','P.001','Apakah penentuan anggota Tim dipilih melalui prosedur/mekanisme yang jelas?',1),('Q.003','P.002','Apakah terdapat dokumen rencana kerja pembangunan Zona Integritas menuju WBK/WBBM?',1),('Q.004','P.002','Apakah dalam dokumen pembangunan terdapat target-target prioritas yang relevan dengan tujuan pembangunan WBK/WBBM?',1),('Q.005','P.002','Apakah terdapat mekanisme atau media untuk mensosialisasikan pembangunan WBK/WBBM?',1),('Q.006','P.003','Apakah seluruh kegiatan pembangunan sudah dilaksanakan sesuai dengan rencana?',1),('Q.007','P.003','Apakah terdapat monitoring dan evaluasi terhadap pembangunan Zona Integritas?',1),('Q.008','P.003','Apakah hasil Monitoring dan Evaluasi telah ditindaklanjuti?',1),('Q.009','P.004','Apakah pimpinan berperan sebagai role model dalam pelaksanaan Pembangunan WBK/WBBM?',1),('Q.010','P.004','Apakah sudah ditetapkan agen perubahan?',1),('Q.011','P.004','Apakah telah dibangun budaya kerja dan pola pikir di lingkungan organisasi?',1),('Q.012','P.004','Apakah anggota organisasi terlibat dalam pembangunan Zona Integritas menuju WBK/WBBM?',1),('Q.013','P.005','Apakah SOP mengacu pada peta proses bisnis instansi?',1),('Q.014','P.005','Apakah Prosedur operasional tetap (SOP) telah diterapkan?',1),('Q.015','P.005','Apakah Prosedur operasional tetap (SOP) telah dievaluasi?',1),('Q.016','P.006','Apakah sistem pengukuran kinerja unit sudah menggunakan teknologi informasi?',1),('Q.017','P.006','Apakah operasionalisasi manajemen SDM sudah menggunakan teknologi informasi?',1),('Q.018','P.006','Apakah pemberian pelayanan kepada publik sudah menggunakan teknologi informasi?',1),('Q.019','P.006','Apakah telah dilakukan monitoring dan dan evaluasi terhadap pemanfaatan teknologi informasi dalam pengukuran kinerja unit, operasionalisasi SDM, dan pemberian layanan kepada publik?',1),('Q.020','P.007','Apakah Kebijakan tentang  keterbukaan informasi publik telah diterapkan?',1),('Q.021','P.007','Apakah telah dilakukan monitoring dan evaluasi pelaksanaan kebijakan keterbukaan informasi publik?',1),('Q.022','P.008','Apakah kebutuhan pegawai yang disusun oleh unit kerja mengacu kepada peta jabatan dan hasil analisis beban kerja untuk masing-masing jabatan?',1),('Q.023','P.008','Apakah penempatan pegawai hasil rekrutmen murni mengacu kepada kebutuhan pegawai yang telah disusun per jabatan?',1),('Q.024','P.008','Apakah telah dilakukan monitoring dan dan evaluasi terhadap penempatan pegawai rekrutmen untuk memenuhi kebutuhan jabatan dalam organisasi telah memberikan perbaikan terhadap kinerja unit kerja?',1),('Q.025','P.009','Dalam melakukan pengembangan karier pegawai, apakah telah dilakukan mutasi pegawai antar jabatan?',1),('Q.026','P.009','Apakah dalam melakukan mutasi pegawai antar jabatan telah memperhatikan kompetensi jabatan dan mengikuti pola mutasi yang telah ditetapkan?',1),('Q.027','P.009','Apakah telah dilakukan monitoring dan evaluasi terhadap kegiatan mutasi yang telah dilakukan dalam kaitannya dengan perbaikan kinerja?',1),('Q.028','P.010','Apakah Unit Kerja melakukan Training Need Analysis Untuk pengembangan kompetensi?',1),('Q.029','P.010','Dalam menyusun rencana pengembangan kompetensi pegawai, apakah mempertimbangkan hasil pengelolaan kinerja pegawai?',1),('Q.030','P.010','Apakah terdapat kesenjangan kompetensi pegawai yang ada dengan standar kompetensi yang ditetapkan untuk masing-masing jabatan?',1),('Q.031','P.010','Apakah pegawai di Unit Kerja telah memperoleh kesempatan/hak untuk mengikuti diklat maupun pengembangan kompetensi lainnya?',1),('Q.032','P.010','Dalam pelaksanaan pengembangan kompetensi, apakah unit kerja melakukan upaya pengembangan kompetensi kepada pegawai (dapat melalui pengikutsertaan pada lembaga pelatihan, in-house training, atau melalui coaching, atau mentoring, dll)?',1),('Q.033','P.010','Apakah telah dilakukan monitoring dan evaluasi terhadap hasil pengembangan kompetensi dalam kaitannya dengan perbaikan kinerja?',1),('Q.034','P.011','Apakah terdapat penetapan kinerja individu yang terkait dengan perjanjian kinerja organisasi?',1),('Q.035','P.011','Apakah ukuran kinerja individu telah memiliki kesesuaian dengan indikator kinerja individu level diatasnya?',1),('Q.036','P.011','Apakah Pengukuran kinerja individu dilakukan secara periodik?',1),('Q.037','P.011','Apakah hasil penilaian kinerja individu telah dijadikan dasar untuk pemberian reward (pengembangan karir individu, penghargaan dll)?',1),('Q.038','P.012','Apakah aturan disiplin/kode etik/kode perilaku telah dilaksanakan/diimplementasikan?',1),('Q.039','P.013','Apakah data informasi kepegawaian unit kerja telah dimutakhirkan secara berkala?',1),('Q.040','P.014','Apakah pimpinan terlibat secara langsung pada saat penyusunan Perencanaan?',1),('Q.041','P.014','Apakah pimpinan terlibat secara langsung pada saat penyusunan Perjanjian Kinerja?',1),('Q.042','P.014','Apakah pimpinan memantau pencapaian kinerja secara berkala?',1),('Q.043','P.015','Apakah dokumen perencanaan sudah ada?',1),('Q.044','P.015','Apakah dokumen perencanaan telah berorientasi hasil?',1),('Q.045','P.015','Apakah terdapat Indikator Kinerja Utama (IKU)?',1),('Q.046','P.015','Apakah indikator kinerja telah SMART?',1),('Q.047','P.015','Apakah laporan kinerja telah disusun tepat waktu?',1),('Q.048','P.015','Apakah pelaporan kinerja telah memberikan informasi tentang kinerja?',1),('Q.049','P.015','Apakah terdapat upaya peningkatan kapasitas SDM yang menangani akuntabilitas kinerja?',1),('Q.050','P.015','Apakah pengelolaan akuntabilitas kinerja dilaksanakan oleh SDM yang kompeten?',1),('Q.051','P.016','Apakah telah dilakukan public campaign tentang pengendalian gratifikasi?',1),('Q.052','P.016','Apakah pengendalian gratifikasi telah diimplementasikan?',1),('Q.053','P.017','Apakah telah dibangun lingkungan pengendalian? ',1),('Q.054','P.017','Apakah telah dilakukan penilaian risiko atas pelaksanaan kebijakan? ',1),('Q.055','P.017','Apakah telah dilakukan kegiatan pengendalian untuk meminimalisir risiko yang telah diidentifikasi? ',1),('Q.056','P.017','Apakah SPI telah diinformasikan dan dikomunikasikan kepada seluruh pihak terkait?',1),('Q.057','P.018','Apakah kebijakan Pengaduan masyarakat telah diimplementasikan? ',1),('Q.058','P.018','Apakah telah dilakukan monitoring dan evaluasi atas penanganan pengaduan masyarakat?',1),('Q.059','P.018','Apakah hasil evaluasi atas penanganan pengaduan masyarakat telah ditindaklanjuti?',1),('Q.060','P.019','Apakah Whistle Blowing System sudah di internalisasi?',1),('Q.061','P.019','Apakah Whistle Blowing System telah diterapkan?',1),('Q.062','P.019','Apakah telah dilakukan evaluasi atas penerapan Whistle Blowing System?',1),('Q.063','P.019','Apakah hasil evaluasi atas penerapan Whistle Blowing System telah ditindaklanjuti?',1),('Q.064','P.020','Apakah telah terdapat identifikasi/pemetaan benturan kepentingan dalam tugas fungsi utama?',1),('Q.065','P.020','Apakah penanganan Benturan Kepentingan telah disosialisasikan/internalisasi?',1),('Q.066','P.020','Apakah penanganan Benturan Kepentingan telah diimplementasikan?',1),('Q.067','P.020','Apakah telah dilakukan evaluasi atas Penanganan Benturan Kepentingan?',1),('Q.068','P.020','Apakah hasil evaluasi atas Penanganan Benturan Kepentingan telah ditindaklanjuti?',1),('Q.069','P.021','Apakah terdapat kebijakan standar pelayanan?',1),('Q.070','P.021','Apakah standar pelayanan telah dimaklumatkan?',1),('Q.071','P.021','Apakah terdapat SOP bagi pelaksanaan standar pelayanan?',1),('Q.072','P.021','Apakah telah dilakukan reviu dan perbaikan atas standar pelayanan dan SOP?',1),('Q.073','P.022','Apakah telah dilakukan sosialisasi/pelatihan dalam upaya penerapan Budaya Pelayanan Prima?',1),('Q.074','P.022','Apakah informasi tentang pelayanan mudah diakses melalui berbagai media?',1),('Q.075','P.022','Apakah telah terdapat sistem punishment(sanksi)/reward bagi pelaksana layanan serta pemberian kompensasi kepada penerima layanan bila layanan tidak sesuai standar?',1),('Q.076','P.022','Apakah telah terdapat sarana layanan terpadu/terintegrasi?',1),('Q.077','P.022','Apakah terdapat inovasi pelayanan?',1),('Q.078','P.023','Apakah telah dilakukan survey kepuasan masyarakat terhadap pelayanan?',1),('Q.079','P.023','Apakah hasil survey kepuasan masyarakat dapat diakses secara terbuka?',1),('Q.080','P.023','Apakah dilakukan tindak lanjut atas hasil survey kepuasan masyarakat?',1),('Q.081','P.024','Masukkan Nilai Survey (0-4)',4),('Q.082','P.025','Masukkan Nilai Persentase (0-100)',100),('Q.083','P.026','Masukkan Nilai Persepsi (0-4)',4);
/*!40000 ALTER TABLE `tb_pertanyaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_satker`
--

DROP TABLE IF EXISTS `tb_satker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_satker` (
  `id_satker` varchar(10) NOT NULL DEFAULT '',
  `nama_satker` varchar(255) NOT NULL DEFAULT '',
  `kategori_satker` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id_satker`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_satker`
--

LOCK TABLES `tb_satker` WRITE;
/*!40000 ALTER TABLE `tb_satker` DISABLE KEYS */;
INSERT INTO `tb_satker` VALUES ('SAT.001','POLDA METRO JAYA','admin','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.002','SPRIPIM POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.003','ITWASDA POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.004','RORENA POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.005','ROOPS POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.006','ROSDM POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.007','ROLOG POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.008','BIDPROPAM POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.009','BID TIK POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.010','BIDDOKKES POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.011','BIDKEU POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.012','SPN POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.013','YANMA POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.014','DITINTELKAM POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.015','DITRESKRIMUM POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.016','DITRESKRIMSUS POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.017','DITRESNARKOBA POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.018','DITSAMAPTA POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.019','DITLANTAS POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.020','DITPAM OBVIT POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.021','DITPOLAIRUD POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.022','DITBINMAS POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.023','SATBRIMOB POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.024','POLRES METRO JAKARTA PUSAT','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.025','POLRES METRO JAKARTA UTARA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.026','POLRES METRO JAKARTA SELATAN','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.027','POLRES METRO JAKARTA BARAT','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.028','POLRES METRO JAKARTA TIMUR','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.029','POLRES METRO TANGERANG KOTA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.030','POLRESTA BANDARA SOETTA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.031','POLRES METRO BEKASI KOTA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.032','POLRES METRO BEKASI','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.033','POLRES METRO DEPOK','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.034','POLRES PELABUHAN TANJUNG PRIOK','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.035','POLRES KEPULAUAN SERIBU','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.036','POLRES TANGERANG SELATAN','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.037','BIDKUM POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01'),('SAT.038','BIDHUMAS POLDA METRO JAYA','user','VW52aUxYRlRRMEpLeVZ4bE54WE1wQT09','2021-10-18 13:55:01');
/*!40000 ALTER TABLE `tb_satker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_survey`
--

DROP TABLE IF EXISTS `tb_survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_survey` (
  `komponen_id` varchar(5) NOT NULL DEFAULT '',
  `satker_id` varchar(10) NOT NULL DEFAULT '',
  `pertanyaan_id` varchar(6) NOT NULL DEFAULT '',
  `jawaban_id` varchar(6) NOT NULL DEFAULT '',
  `nama_dokumen` varchar(255) NOT NULL DEFAULT '',
  `dokumen_pendukung` varchar(255) NOT NULL DEFAULT '',
  `catatan_revisi` text NOT NULL,
  `lastupdate` datetime NOT NULL,
  PRIMARY KEY (`komponen_id`,`satker_id`,`pertanyaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_survey`
--

LOCK TABLES `tb_survey` WRITE;
/*!40000 ALTER TABLE `tb_survey` DISABLE KEYS */;
INSERT INTO `tb_survey` VALUES ('C.001','SAT.002','Q.001','A.001','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.002','A.003','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.003','A.006','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.004','A.008','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.005','A.011','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.006','A.014','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.007','A.018','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.008','A.022','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.009','A.026','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.010','A.028','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.011','A.031','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.002','Q.012','A.034','sample_upload.pdf','ZM51H39PKETQFBW.pdf','','2021-10-18 23:50:21'),('C.001','SAT.037','Q.001','A.001','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.002','A.003','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.003','A.006','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.004','A.008','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.005','A.011','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.006','A.014','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.007','A.018','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.008','A.022','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.009','A.026','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.010','A.028','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.011','A.031','','','','2021-10-25 00:48:29'),('C.001','SAT.037','Q.012','A.034','','','','2021-10-25 00:48:29'),('C.002','SAT.002','Q.013','A.038','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.014','A.042','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.015','A.047','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.016','A.052','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.017','A.055','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.018','A.058','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.019','A.061','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.020','A.064','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.002','SAT.002','Q.021','A.067','','','Upload dokumen pendukung !','2021-10-21 09:58:32'),('C.003','SAT.002','Q.022','A.070','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.023','A.072','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.024','A.076','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.025','A.078','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.026','A.080','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.027','A.085','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.028','A.087','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.029','A.089','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.030','A.093','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.031','A.097','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.032','A.101','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.033','A.105','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.034','A.108','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.035','A.112','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.036','A.116','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.037','A.121','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.038','A.123','','','','2021-10-21 10:20:42'),('C.003','SAT.002','Q.039','A.127','','','','2021-10-21 10:20:42'),('C.004','SAT.002','Q.040','A.130','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.041','A.133','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.042','A.136','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.043','A.140','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.044','A.142','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.045','A.144','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.046','A.146','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.047','A.150','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.048','A.152','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.049','A.155','','','','2021-10-21 10:21:09'),('C.004','SAT.002','Q.050','A.157','','','','2021-10-21 10:21:09'),('C.005','SAT.002','Q.051','A.160','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.052','A.163','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.053','A.167','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.054','A.172','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.055','A.177','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.056','A.180','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.057','A.183','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.058','A.186','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.059','A.189','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.060','A.192','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.061','A.194','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.062','A.197','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.063','A.200','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.064','A.203','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.065','A.207','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.066','A.211','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.067','A.215','','','','2021-10-21 10:21:50'),('C.005','SAT.002','Q.068','A.218','','','','2021-10-21 10:21:50'),('C.006','SAT.002','Q.069','A.221','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.070','A.224','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.071','A.229','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.072','A.234','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.073','A.237','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.074','A.241','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.075','A.244','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.076','A.247','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.077','A.251','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.078','A.256','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.079','A.259','','','','2021-10-21 10:22:20'),('C.006','SAT.002','Q.080','A.262','','','','2021-10-21 10:22:20'),('C.007','SAT.002','Q.081','4','','','','2021-10-21 10:22:43'),('C.007','SAT.002','Q.082','100','','','','2021-10-21 10:22:43'),('C.008','SAT.002','Q.083','4','','','','2021-10-21 10:22:55');
/*!40000 ALTER TABLE `tb_survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_verifikasi`
--

DROP TABLE IF EXISTS `tb_verifikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_verifikasi` (
  `komponen_id` varchar(5) NOT NULL DEFAULT '',
  `satker_id` varchar(10) NOT NULL DEFAULT '',
  `status_verifikasi` varchar(10) NOT NULL DEFAULT '',
  `user_id` varchar(10) NOT NULL DEFAULT '',
  `lastupdate` datetime NOT NULL,
  PRIMARY KEY (`komponen_id`,`satker_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_verifikasi`
--

LOCK TABLES `tb_verifikasi` WRITE;
/*!40000 ALTER TABLE `tb_verifikasi` DISABLE KEYS */;
INSERT INTO `tb_verifikasi` VALUES ('C.001','SAT.002','complete','SAT.001','2021-10-18 23:50:21'),('C.001','SAT.037','submission','SAT.001','2021-10-25 00:48:29'),('C.002','SAT.002','revision','SAT.001','2021-10-21 09:58:32'),('C.003','SAT.002','submission','SAT.001','2021-10-21 10:20:42'),('C.004','SAT.002','submission','SAT.001','2021-10-21 10:21:09'),('C.005','SAT.002','submission','SAT.001','2021-10-21 10:21:50'),('C.006','SAT.002','submission','SAT.001','2021-10-21 10:22:20'),('C.007','SAT.002','submission','SAT.001','2021-10-21 10:22:43'),('C.008','SAT.002','submission','SAT.001','2021-10-21 10:22:55');
/*!40000 ALTER TABLE `tb_verifikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_point_kategori`
--

DROP TABLE IF EXISTS `view_point_kategori`;
/*!50001 DROP VIEW IF EXISTS `view_point_kategori`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_point_kategori` AS SELECT 
 1 AS `satker_id`,
 1 AS `id_kategori`,
 1 AS `nama_kategori`,
 1 AS `point_kategori`,
 1 AS `total_point_kategori`,
 1 AS `persentase_point_kategori`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_point_komponen`
--

DROP TABLE IF EXISTS `view_point_komponen`;
/*!50001 DROP VIEW IF EXISTS `view_point_komponen`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_point_komponen` AS SELECT 
 1 AS `satker_id`,
 1 AS `terjawab`,
 1 AS `jumlah_soal`,
 1 AS `id_komponen`,
 1 AS `kategori_id`,
 1 AS `nama_komponen`,
 1 AS `point_komponen`,
 1 AS `total_point_komponen`,
 1 AS `status_verifikasi`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_point_penilaian`
--

DROP TABLE IF EXISTS `view_point_penilaian`;
/*!50001 DROP VIEW IF EXISTS `view_point_penilaian`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_point_penilaian` AS SELECT 
 1 AS `id_penilaian`,
 1 AS `nama_penilaian`,
 1 AS `komponen_id`,
 1 AS `terjawab`,
 1 AS `jumlah_soal`,
 1 AS `satker_id`,
 1 AS `pertanyaan_id`,
 1 AS `jawaban_id`,
 1 AS `dokumen_pendukung`,
 1 AS `point_pertanyaan`,
 1 AS `point_jawaban`,
 1 AS `point_survey`,
 1 AS `point_penilaian`,
 1 AS `total_point_survey`,
 1 AS `total_point_penilaian`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_point_survey`
--

DROP TABLE IF EXISTS `view_point_survey`;
/*!50001 DROP VIEW IF EXISTS `view_point_survey`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_point_survey` AS SELECT 
 1 AS `satker_id`,
 1 AS `pertanyaan_id`,
 1 AS `jawaban_id`,
 1 AS `dokumen_pendukung`,
 1 AS `point_pertanyaan`,
 1 AS `point_jawaban`,
 1 AS `point_survey`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_soal_survey`
--

DROP TABLE IF EXISTS `view_soal_survey`;
/*!50001 DROP VIEW IF EXISTS `view_soal_survey`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_soal_survey` AS SELECT 
 1 AS `id_pertanyaan`,
 1 AS `penilaian_id`,
 1 AS `nama_pertanyaan`,
 1 AS `point_pertanyaan`,
 1 AS `id_penilaian`,
 1 AS `komponen_id`,
 1 AS `nama_penilaian`,
 1 AS `point_penilaian`,
 1 AS `id_komponen`,
 1 AS `kategori_id`,
 1 AS `nama_komponen`,
 1 AS `point_komponen`,
 1 AS `id_kategori`,
 1 AS `nama_kategori`,
 1 AS `point_kategori`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_point_kategori`
--

/*!50001 DROP VIEW IF EXISTS `view_point_kategori`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `view_point_kategori` AS select `v_komponen`.`satker_id` AS `satker_id`,`kategori`.`id_kategori` AS `id_kategori`,`kategori`.`nama_kategori` AS `nama_kategori`,`kategori`.`point_kategori` AS `point_kategori`,sum(`v_komponen`.`total_point_komponen`) AS `total_point_kategori`,round(((sum(`v_komponen`.`total_point_komponen`) / `kategori`.`point_kategori`) * 100),2) AS `persentase_point_kategori` from (`view_point_komponen` `v_komponen` join `tb_kategori` `kategori` on((`v_komponen`.`kategori_id` = `kategori`.`id_kategori`))) group by `kategori`.`id_kategori`,`v_komponen`.`satker_id` order by `kategori`.`id_kategori` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_point_komponen`
--

/*!50001 DROP VIEW IF EXISTS `view_point_komponen`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `view_point_komponen` AS select `v_penilaian`.`satker_id` AS `satker_id`,sum(`v_penilaian`.`terjawab`) AS `terjawab`,(select count(0) from `view_soal_survey` where (`view_soal_survey`.`id_komponen` = `komponen`.`id_komponen`)) AS `jumlah_soal`,`komponen`.`id_komponen` AS `id_komponen`,`komponen`.`kategori_id` AS `kategori_id`,`komponen`.`nama_komponen` AS `nama_komponen`,`komponen`.`point_komponen` AS `point_komponen`,if((`verifikasi`.`status_verifikasi` = 'complete'),sum(`v_penilaian`.`total_point_penilaian`),0) AS `total_point_komponen`,`verifikasi`.`status_verifikasi` AS `status_verifikasi` from ((`view_point_penilaian` `v_penilaian` join `tb_komponen` `komponen` on((`v_penilaian`.`komponen_id` = `komponen`.`id_komponen`))) join `tb_verifikasi` `verifikasi` on(((`verifikasi`.`komponen_id` = `komponen`.`id_komponen`) and (`verifikasi`.`satker_id` = `v_penilaian`.`satker_id`)))) group by `komponen`.`id_komponen`,`v_penilaian`.`satker_id` order by `komponen`.`id_komponen` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_point_penilaian`
--

/*!50001 DROP VIEW IF EXISTS `view_point_penilaian`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `view_point_penilaian` AS select `penilaian`.`id_penilaian` AS `id_penilaian`,`penilaian`.`nama_penilaian` AS `nama_penilaian`,`penilaian`.`komponen_id` AS `komponen_id`,count(`survey`.`pertanyaan_id`) AS `terjawab`,(select count(0) from `tb_pertanyaan` where (`tb_pertanyaan`.`penilaian_id` = `penilaian`.`id_penilaian`)) AS `jumlah_soal`,`survey`.`satker_id` AS `satker_id`,`survey`.`pertanyaan_id` AS `pertanyaan_id`,`survey`.`jawaban_id` AS `jawaban_id`,`survey`.`dokumen_pendukung` AS `dokumen_pendukung`,`survey`.`point_pertanyaan` AS `point_pertanyaan`,`survey`.`point_jawaban` AS `point_jawaban`,`survey`.`point_survey` AS `point_survey`,`penilaian`.`point_penilaian` AS `point_penilaian`,sum(`survey`.`point_survey`) AS `total_point_survey`,((sum(`survey`.`point_survey`) / (select `jumlah_soal`)) * `penilaian`.`point_penilaian`) AS `total_point_penilaian` from ((`view_point_survey` `survey` join `tb_pertanyaan` `pertanyaan` on((`survey`.`pertanyaan_id` = `pertanyaan`.`id_pertanyaan`))) join `tb_penilaian` `penilaian` on((`pertanyaan`.`penilaian_id` = `penilaian`.`id_penilaian`))) group by `penilaian`.`id_penilaian`,`survey`.`satker_id` order by `penilaian`.`id_penilaian` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_point_survey`
--

/*!50001 DROP VIEW IF EXISTS `view_point_survey`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `view_point_survey` AS select `survey`.`satker_id` AS `satker_id`,`survey`.`pertanyaan_id` AS `pertanyaan_id`,`survey`.`jawaban_id` AS `jawaban_id`,`survey`.`dokumen_pendukung` AS `dokumen_pendukung`,`pertanyaan`.`point_pertanyaan` AS `point_pertanyaan`,`jawaban`.`point_jawaban` AS `point_jawaban`,ifnull(`jawaban`.`point_jawaban`,(`survey`.`jawaban_id` / `pertanyaan`.`point_pertanyaan`)) AS `point_survey` from ((`tb_survey` `survey` join `tb_pertanyaan` `pertanyaan` on((`survey`.`pertanyaan_id` = `pertanyaan`.`id_pertanyaan`))) left join `tb_jawaban` `jawaban` on((`survey`.`jawaban_id` = `jawaban`.`id_jawaban`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_soal_survey`
--

/*!50001 DROP VIEW IF EXISTS `view_soal_survey`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 */
/*!50001 VIEW `view_soal_survey` AS select `pertanyaan`.`id_pertanyaan` AS `id_pertanyaan`,`pertanyaan`.`penilaian_id` AS `penilaian_id`,`pertanyaan`.`nama_pertanyaan` AS `nama_pertanyaan`,`pertanyaan`.`point_pertanyaan` AS `point_pertanyaan`,`penilaian`.`id_penilaian` AS `id_penilaian`,`penilaian`.`komponen_id` AS `komponen_id`,`penilaian`.`nama_penilaian` AS `nama_penilaian`,`penilaian`.`point_penilaian` AS `point_penilaian`,`komponen`.`id_komponen` AS `id_komponen`,`komponen`.`kategori_id` AS `kategori_id`,`komponen`.`nama_komponen` AS `nama_komponen`,`komponen`.`point_komponen` AS `point_komponen`,`kategori`.`id_kategori` AS `id_kategori`,`kategori`.`nama_kategori` AS `nama_kategori`,`kategori`.`point_kategori` AS `point_kategori` from (((`tb_pertanyaan` `pertanyaan` join `tb_penilaian` `penilaian` on((`pertanyaan`.`penilaian_id` = `penilaian`.`id_penilaian`))) join `tb_komponen` `komponen` on((`penilaian`.`komponen_id` = `komponen`.`id_komponen`))) join `tb_kategori` `kategori` on((`komponen`.`kategori_id` = `kategori`.`id_kategori`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-24 17:55:23
