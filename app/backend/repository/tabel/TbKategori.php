<?php 
namespace app\backend\repository\tabel;
use core\database\Mysql;

class TbKategori extends Mysql {

    private $format_id = 'K.##';

    public function __construct() {
        parent::__construct('mysqldb', 'tb_kategori', 'id_kategori');
        parent::setModel([
            'id_kategori' => $this->setSequenceID($this->format_id),
            'nama_kategori' => '',
            'point_kategori' => '0',
        ]);
    }

    public function getPoint() {
        $result = [];
        foreach ($this->getAll() as $key => $value) {
            $result[$value['id_kategori']] = $value['point_kategori'];
        }

        return $result;
    }

    public function createData() {
        $data = [
            ['nama_kategori' => 'Komponen Pengungkit', 'point_kategori' => 60],
            ['nama_kategori' => 'Komponen Hasil', 'point_kategori' => 40],
        ];

        $results = [];
        $number = 1;
        foreach ($data as $key => $value) {
            // $value['id_kategori'] = $this->setSequenceID($this->format_id);
            $value['id_kategori'] = $this->setSequenceID($this->format_id, $number++);
            $result = $this->save($value);
            array_push($results, $result);
        }

        return $results;
    }

}
?>