<?php 
namespace app\backend\repository\tabel;
use core\database\Mysql;

class TbKomponen extends Mysql {

    private $format_id = 'C.###';

    public function __construct() {
        parent::__construct('mysqldb', 'tb_komponen', 'id_komponen');
        parent::setModel([
            'id_komponen' => $this->setSequenceID($this->format_id),
            'kategori_id' => '',
            'nama_komponen' => '',
            'point_komponen' => '0',
        ]);
    }

    public function getPoint() {
        $result = [];
        foreach ($this->getAll() as $key => $value) {
            $result[$value['id_komponen']] = $value['point_komponen'];
        }

        return $result;
    }

    public function createData() {
        $data = [
            ['kategori_id' => 'K.01', 'nama_komponen' => 'Manajemen Perubahan', 'point_komponen' => 8],
            ['kategori_id' => 'K.01', 'nama_komponen' => 'Penataan Tatalaksana', 'point_komponen' => 7],
            ['kategori_id' => 'K.01', 'nama_komponen' => 'Penataan Sistem Manajemen SDM', 'point_komponen' => 10],
            ['kategori_id' => 'K.01', 'nama_komponen' => 'Penguatan Akuntabilitas', 'point_komponen' => 10],
            ['kategori_id' => 'K.01', 'nama_komponen' => 'Penguatan Pengawasan', 'point_komponen' => 15],
            ['kategori_id' => 'K.01', 'nama_komponen' => 'Peningkatan Kualitas Pelayanan', 'point_komponen' => 10],
            ['kategori_id' => 'K.02', 'nama_komponen' => 'Pemerintah Yang Bersih dan Bebas KKN', 'point_komponen' => 20],
            ['kategori_id' => 'K.02', 'nama_komponen' => 'Kualitas Pelayanan Publik', 'point_komponen' => 20],
        ];

        $results = [];
        $number = 1;
        foreach ($data as $key => $value) {
            // $value['id_komponen'] = $this->setSequenceID($this->format_id);
            $value['id_komponen'] = $this->setSequenceID($this->format_id, $number++);
            $result = $this->save($value);
            array_push($results, $result);
        }

        return $results;
    }

}
?>