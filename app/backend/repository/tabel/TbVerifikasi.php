<?php 
namespace app\backend\repository\tabel;
use core\database\Mysql;

class TbVerifikasi extends Mysql {

    public function __construct() {
        parent::__construct('mysqldb', 'tb_verifikasi', 'komponen_id');
        parent::setModel([
            'komponen_id' => '',
            'satker_id' => '',
            'status_verfikasi' => '', // submission|revision|complete
            'lastupdate' => $this->getDateTime(),
        ]);
    }

    public function getPilihanStatus() { 
        return [
            '' => ['text' => ''],
            'submission' => ['text' => 'Pengajuan', 'color' => 'primary'],
            'revision' => ['text' => 'Revisi', 'color' => 'warning'],
            'complete' => ['text' => 'Lengkap', 'color' => 'success'],
        ];
    }

    public function getVerifikasi($komponen, $satker) {
        $result = $this->getQuery('SELECT * FROM `tb_komponen` komponen JOIN `tb_verifikasi` verif ON (verif.`komponen_id`=komponen.`id_komponen`) WHERE (verif.`komponen_id` = ?) AND (verif.`satker_id` = ?)', [$komponen, $satker]);
        return ($result['count'] > 0) ? current($result['value']) : null;
    }

}
?>