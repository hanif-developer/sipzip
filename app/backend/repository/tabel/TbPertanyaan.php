<?php 
namespace app\backend\repository\tabel;
use core\database\Mysql;

class TbPertanyaan extends Mysql {

    private $format_id = 'Q.###';

    public function __construct() {
        parent::__construct('mysqldb', 'tb_pertanyaan', 'id_pertanyaan');
        parent::setModel([
            'id_pertanyaan' => $this->setSequenceID($this->format_id),
            'penilaian_id' => '',
            'nama_pertanyaan' => '',
            'point_pertanyaan' => '1',
        ]);
    }

    public function createData() {
        $data = [
            // Manajemen Perubahan
            ['penilaian_id' => 'P.001', 'nama_pertanyaan' => 'Apakah unit kerja telah membentuk tim untuk melakukan pembangunan Zona Integritas?'],
            ['penilaian_id' => 'P.001', 'nama_pertanyaan' => 'Apakah penentuan anggota Tim dipilih melalui prosedur/mekanisme yang jelas?'],
            ['penilaian_id' => 'P.002', 'nama_pertanyaan' => 'Apakah terdapat dokumen rencana kerja pembangunan Zona Integritas menuju WBK/WBBM?'],
            ['penilaian_id' => 'P.002', 'nama_pertanyaan' => 'Apakah dalam dokumen pembangunan terdapat target-target prioritas yang relevan dengan tujuan pembangunan WBK/WBBM?'],
            ['penilaian_id' => 'P.002', 'nama_pertanyaan' => 'Apakah terdapat mekanisme atau media untuk mensosialisasikan pembangunan WBK/WBBM?'],
            ['penilaian_id' => 'P.003', 'nama_pertanyaan' => 'Apakah seluruh kegiatan pembangunan sudah dilaksanakan sesuai dengan rencana?'],
            ['penilaian_id' => 'P.003', 'nama_pertanyaan' => 'Apakah terdapat monitoring dan evaluasi terhadap pembangunan Zona Integritas?'],
            ['penilaian_id' => 'P.003', 'nama_pertanyaan' => 'Apakah hasil Monitoring dan Evaluasi telah ditindaklanjuti?'],
            ['penilaian_id' => 'P.004', 'nama_pertanyaan' => 'Apakah pimpinan berperan sebagai role model dalam pelaksanaan Pembangunan WBK/WBBM?'],
            ['penilaian_id' => 'P.004', 'nama_pertanyaan' => 'Apakah sudah ditetapkan agen perubahan?'],
            ['penilaian_id' => 'P.004', 'nama_pertanyaan' => 'Apakah telah dibangun budaya kerja dan pola pikir di lingkungan organisasi?'],
            ['penilaian_id' => 'P.004', 'nama_pertanyaan' => 'Apakah anggota organisasi terlibat dalam pembangunan Zona Integritas menuju WBK/WBBM?'],
            // Penataan Tatalaksana
            ['penilaian_id' => 'P.005', 'nama_pertanyaan' => 'Apakah SOP mengacu pada peta proses bisnis instansi?'],
            ['penilaian_id' => 'P.005', 'nama_pertanyaan' => 'Apakah Prosedur operasional tetap (SOP) telah diterapkan?'],
            ['penilaian_id' => 'P.005', 'nama_pertanyaan' => 'Apakah Prosedur operasional tetap (SOP) telah dievaluasi?'],
            ['penilaian_id' => 'P.006', 'nama_pertanyaan' => 'Apakah sistem pengukuran kinerja unit sudah menggunakan teknologi informasi?'],
            ['penilaian_id' => 'P.006', 'nama_pertanyaan' => 'Apakah operasionalisasi manajemen SDM sudah menggunakan teknologi informasi?'],
            ['penilaian_id' => 'P.006', 'nama_pertanyaan' => 'Apakah pemberian pelayanan kepada publik sudah menggunakan teknologi informasi?'],
            ['penilaian_id' => 'P.006', 'nama_pertanyaan' => 'Apakah telah dilakukan monitoring dan dan evaluasi terhadap pemanfaatan teknologi informasi dalam pengukuran kinerja unit, operasionalisasi SDM, dan pemberian layanan kepada publik?'],
            ['penilaian_id' => 'P.007', 'nama_pertanyaan' => 'Apakah Kebijakan tentang  keterbukaan informasi publik telah diterapkan?'],
            ['penilaian_id' => 'P.007', 'nama_pertanyaan' => 'Apakah telah dilakukan monitoring dan evaluasi pelaksanaan kebijakan keterbukaan informasi publik?'],
            // Penataan Sistem Manajemen SDM
            ['penilaian_id' => 'P.008', 'nama_pertanyaan' => 'Apakah kebutuhan pegawai yang disusun oleh unit kerja mengacu kepada peta jabatan dan hasil analisis beban kerja untuk masing-masing jabatan?'],
            ['penilaian_id' => 'P.008', 'nama_pertanyaan' => 'Apakah penempatan pegawai hasil rekrutmen murni mengacu kepada kebutuhan pegawai yang telah disusun per jabatan?'],
            ['penilaian_id' => 'P.008', 'nama_pertanyaan' => 'Apakah telah dilakukan monitoring dan dan evaluasi terhadap penempatan pegawai rekrutmen untuk memenuhi kebutuhan jabatan dalam organisasi telah memberikan perbaikan terhadap kinerja unit kerja?'],
            ['penilaian_id' => 'P.009', 'nama_pertanyaan' => 'Dalam melakukan pengembangan karier pegawai, apakah telah dilakukan mutasi pegawai antar jabatan?'],
            ['penilaian_id' => 'P.009', 'nama_pertanyaan' => 'Apakah dalam melakukan mutasi pegawai antar jabatan telah memperhatikan kompetensi jabatan dan mengikuti pola mutasi yang telah ditetapkan?'],
            ['penilaian_id' => 'P.009', 'nama_pertanyaan' => 'Apakah telah dilakukan monitoring dan evaluasi terhadap kegiatan mutasi yang telah dilakukan dalam kaitannya dengan perbaikan kinerja?'],
            ['penilaian_id' => 'P.010', 'nama_pertanyaan' => 'Apakah Unit Kerja melakukan Training Need Analysis Untuk pengembangan kompetensi?'],
            ['penilaian_id' => 'P.010', 'nama_pertanyaan' => 'Dalam menyusun rencana pengembangan kompetensi pegawai, apakah mempertimbangkan hasil pengelolaan kinerja pegawai?'],
            ['penilaian_id' => 'P.010', 'nama_pertanyaan' => 'Apakah terdapat kesenjangan kompetensi pegawai yang ada dengan standar kompetensi yang ditetapkan untuk masing-masing jabatan?'],
            ['penilaian_id' => 'P.010', 'nama_pertanyaan' => 'Apakah pegawai di Unit Kerja telah memperoleh kesempatan/hak untuk mengikuti diklat maupun pengembangan kompetensi lainnya?'],
            ['penilaian_id' => 'P.010', 'nama_pertanyaan' => 'Dalam pelaksanaan pengembangan kompetensi, apakah unit kerja melakukan upaya pengembangan kompetensi kepada pegawai (dapat melalui pengikutsertaan pada lembaga pelatihan, in-house training, atau melalui coaching, atau mentoring, dll)?'],
            ['penilaian_id' => 'P.010', 'nama_pertanyaan' => 'Apakah telah dilakukan monitoring dan evaluasi terhadap hasil pengembangan kompetensi dalam kaitannya dengan perbaikan kinerja?'],
            ['penilaian_id' => 'P.011', 'nama_pertanyaan' => 'Apakah terdapat penetapan kinerja individu yang terkait dengan perjanjian kinerja organisasi?'],
            ['penilaian_id' => 'P.011', 'nama_pertanyaan' => 'Apakah ukuran kinerja individu telah memiliki kesesuaian dengan indikator kinerja individu level diatasnya?'],
            ['penilaian_id' => 'P.011', 'nama_pertanyaan' => 'Apakah Pengukuran kinerja individu dilakukan secara periodik?'],
            ['penilaian_id' => 'P.011', 'nama_pertanyaan' => 'Apakah hasil penilaian kinerja individu telah dijadikan dasar untuk pemberian reward (pengembangan karir individu, penghargaan dll)?'],
            ['penilaian_id' => 'P.012', 'nama_pertanyaan' => 'Apakah aturan disiplin/kode etik/kode perilaku telah dilaksanakan/diimplementasikan?'],
            ['penilaian_id' => 'P.013', 'nama_pertanyaan' => 'Apakah data informasi kepegawaian unit kerja telah dimutakhirkan secara berkala?'],
            // Penguatan Akuntabilitas
            ['penilaian_id' => 'P.014', 'nama_pertanyaan' => 'Apakah pimpinan terlibat secara langsung pada saat penyusunan Perencanaan?'],
            ['penilaian_id' => 'P.014', 'nama_pertanyaan' => 'Apakah pimpinan terlibat secara langsung pada saat penyusunan Perjanjian Kinerja?'],
            ['penilaian_id' => 'P.014', 'nama_pertanyaan' => 'Apakah pimpinan memantau pencapaian kinerja secara berkala?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah dokumen perencanaan sudah ada?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah dokumen perencanaan telah berorientasi hasil?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah terdapat Indikator Kinerja Utama (IKU)?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah indikator kinerja telah SMART?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah laporan kinerja telah disusun tepat waktu?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah pelaporan kinerja telah memberikan informasi tentang kinerja?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah terdapat upaya peningkatan kapasitas SDM yang menangani akuntabilitas kinerja?'],
            ['penilaian_id' => 'P.015', 'nama_pertanyaan' => 'Apakah pengelolaan akuntabilitas kinerja dilaksanakan oleh SDM yang kompeten?'],
            // Penguatan Pengawasan
            ['penilaian_id' => 'P.016', 'nama_pertanyaan' => 'Apakah telah dilakukan public campaign tentang pengendalian gratifikasi?'],
            ['penilaian_id' => 'P.016', 'nama_pertanyaan' => 'Apakah pengendalian gratifikasi telah diimplementasikan?'],
            ['penilaian_id' => 'P.017', 'nama_pertanyaan' => 'Apakah telah dibangun lingkungan pengendalian? '],
            ['penilaian_id' => 'P.017', 'nama_pertanyaan' => 'Apakah telah dilakukan penilaian risiko atas pelaksanaan kebijakan? '],
            ['penilaian_id' => 'P.017', 'nama_pertanyaan' => 'Apakah telah dilakukan kegiatan pengendalian untuk meminimalisir risiko yang telah diidentifikasi? '],
            ['penilaian_id' => 'P.017', 'nama_pertanyaan' => 'Apakah SPI telah diinformasikan dan dikomunikasikan kepada seluruh pihak terkait?'],
            ['penilaian_id' => 'P.018', 'nama_pertanyaan' => 'Apakah kebijakan Pengaduan masyarakat telah diimplementasikan? '],
            // ['penilaian_id' => 'P.018', 'nama_pertanyaan' => 'Penanganan Pengaduan Masyarakat '], // Design Jawaban Berbeda
            ['penilaian_id' => 'P.018', 'nama_pertanyaan' => 'Apakah telah dilakukan monitoring dan evaluasi atas penanganan pengaduan masyarakat?'],
            ['penilaian_id' => 'P.018', 'nama_pertanyaan' => 'Apakah hasil evaluasi atas penanganan pengaduan masyarakat telah ditindaklanjuti?'],
            ['penilaian_id' => 'P.019', 'nama_pertanyaan' => 'Apakah Whistle Blowing System sudah di internalisasi?'],
            ['penilaian_id' => 'P.019', 'nama_pertanyaan' => 'Apakah Whistle Blowing System telah diterapkan?'],
            ['penilaian_id' => 'P.019', 'nama_pertanyaan' => 'Apakah telah dilakukan evaluasi atas penerapan Whistle Blowing System?'],
            ['penilaian_id' => 'P.019', 'nama_pertanyaan' => 'Apakah hasil evaluasi atas penerapan Whistle Blowing System telah ditindaklanjuti?'],
            ['penilaian_id' => 'P.020', 'nama_pertanyaan' => 'Apakah telah terdapat identifikasi/pemetaan benturan kepentingan dalam tugas fungsi utama?'],
            ['penilaian_id' => 'P.020', 'nama_pertanyaan' => 'Apakah penanganan Benturan Kepentingan telah disosialisasikan/internalisasi?'],
            ['penilaian_id' => 'P.020', 'nama_pertanyaan' => 'Apakah penanganan Benturan Kepentingan telah diimplementasikan?'],
            ['penilaian_id' => 'P.020', 'nama_pertanyaan' => 'Apakah telah dilakukan evaluasi atas Penanganan Benturan Kepentingan?'],
            ['penilaian_id' => 'P.020', 'nama_pertanyaan' => 'Apakah hasil evaluasi atas Penanganan Benturan Kepentingan telah ditindaklanjuti?'],
            // Peningkatan Kualitas Pelayanan
            ['penilaian_id' => 'P.021', 'nama_pertanyaan' => 'Apakah terdapat kebijakan standar pelayanan?'],
            ['penilaian_id' => 'P.021', 'nama_pertanyaan' => 'Apakah standar pelayanan telah dimaklumatkan?'],
            ['penilaian_id' => 'P.021', 'nama_pertanyaan' => 'Apakah terdapat SOP bagi pelaksanaan standar pelayanan?'],
            ['penilaian_id' => 'P.021', 'nama_pertanyaan' => 'Apakah telah dilakukan reviu dan perbaikan atas standar pelayanan dan SOP?'],
            ['penilaian_id' => 'P.022', 'nama_pertanyaan' => 'Apakah telah dilakukan sosialisasi/pelatihan dalam upaya penerapan Budaya Pelayanan Prima?'],
            ['penilaian_id' => 'P.022', 'nama_pertanyaan' => 'Apakah informasi tentang pelayanan mudah diakses melalui berbagai media?'],
            ['penilaian_id' => 'P.022', 'nama_pertanyaan' => 'Apakah telah terdapat sistem punishment(sanksi)/reward bagi pelaksana layanan serta pemberian kompensasi kepada penerima layanan bila layanan tidak sesuai standar?'],
            ['penilaian_id' => 'P.022', 'nama_pertanyaan' => 'Apakah telah terdapat sarana layanan terpadu/terintegrasi?'],
            ['penilaian_id' => 'P.022', 'nama_pertanyaan' => 'Apakah terdapat inovasi pelayanan?'],
            ['penilaian_id' => 'P.023', 'nama_pertanyaan' => 'Apakah telah dilakukan survey kepuasan masyarakat terhadap pelayanan?'],
            ['penilaian_id' => 'P.023', 'nama_pertanyaan' => 'Apakah hasil survey kepuasan masyarakat dapat diakses secara terbuka?'],
            ['penilaian_id' => 'P.023', 'nama_pertanyaan' => 'Apakah dilakukan tindak lanjut atas hasil survey kepuasan masyarakat?'],
            // Pemerintah Yang Bersih dan Bebas KKN
            ['penilaian_id' => 'P.024', 'nama_pertanyaan' => 'Masukkan Nilai Survey (0-4)', 'point_pertanyaan' => 4],
            ['penilaian_id' => 'P.025', 'nama_pertanyaan' => 'Masukkan Nilai Persentase (0-100)', 'point_pertanyaan' => 100],
            // Kualitas Pelayanan Publik
            ['penilaian_id' => 'P.026', 'nama_pertanyaan' => 'Masukkan Nilai Persepsi (0-4)', 'point_pertanyaan' => 4],
        ];

        $results = [];
        $number = 1;
        foreach ($data as $key => $value) {
            // $value['id_pertanyaan'] = $this->setSequenceID($this->format_id);
            $value['id_pertanyaan'] = $this->setSequenceID($this->format_id, $number++);
            $result = $this->save($value);
            array_push($results, $result);
        }

        return $results;
    }

}
?>