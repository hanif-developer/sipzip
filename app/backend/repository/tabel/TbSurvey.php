<?php 
namespace app\backend\repository\tabel;
use core\database\Mysql;

class TbSurvey extends Mysql {

    private $format_id = 'S.###';

    public function __construct() {
        parent::__construct('mysqldb', 'tb_survey', 'komponen_id');
        parent::setModel([
            'komponen_id' => '',//$this->setDateID(),
            'satker_id' => '',
            'pertanyaan_id' => '',
            'jawaban_id' => '',
            'nama_pendukung' => '',
            'dokumen_pendukung' => '',
            'catatan_revisi' => '',
            // 'catatan_revisi' => 'Upload dokumen pendukung !',
            'lastupdate' => $this->getDateTime(),
        ]);
    }

}
?>