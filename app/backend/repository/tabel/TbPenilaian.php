<?php 
namespace app\backend\repository\tabel;
use core\database\Mysql;

class TbPenilaian extends Mysql {

    private $format_id = 'P.###';

    public function __construct() {
        parent::__construct('mysqldb', 'tb_penilaian', 'id_penilaian');
        parent::setModel([
            'id_penilaian' => $this->setSequenceID($this->format_id),
            'komponen_id' => '',
            'nama_penilaian' => '',
            'point_penilaian' => '0',
        ]);
    }

    public function getPoint() {
        $result = [];
        foreach ($this->getAll() as $key => $value) {
            $result[$value['id_penilaian']] = $value['point_penilaian'];
        }

        return $result;
    }

    public function createData() {
        $data = [
            ['komponen_id' => 'C.001', 'nama_penilaian' => 'Tim Kerja', 'point_penilaian' => 1],
            ['komponen_id' => 'C.001', 'nama_penilaian' => 'Rencana Pembangunan Zona Integritas', 'point_penilaian' => 2],
            ['komponen_id' => 'C.001', 'nama_penilaian' => 'Pemantauan dan Evaluasi Pembangunan WBK/WBBM', 'point_penilaian' => 2],
            ['komponen_id' => 'C.001', 'nama_penilaian' => 'Perubahan pola pikir dan budaya kerja', 'point_penilaian' => 3],
            ['komponen_id' => 'C.002', 'nama_penilaian' => 'Prosedur operasional tetap (SOP) kegiatan utama', 'point_penilaian' => 2],
            ['komponen_id' => 'C.002', 'nama_penilaian' => 'E-Office', 'point_penilaian' => 4],
            ['komponen_id' => 'C.002', 'nama_penilaian' => 'Keterbukaan Informasi Publik', 'point_penilaian' => 1],
            ['komponen_id' => 'C.003', 'nama_penilaian' => 'Perencanaan kebutuhan pegawai sesuai dengan kebutuhan organisasi', 'point_penilaian' => 0.5],
            ['komponen_id' => 'C.003', 'nama_penilaian' => 'Pola Mutasi Internal', 'point_penilaian' => 1],
            ['komponen_id' => 'C.003', 'nama_penilaian' => 'Pengembangan pegawai berbasis kompetensi', 'point_penilaian' => 2.5],
            ['komponen_id' => 'C.003', 'nama_penilaian' => 'Penetapan kinerja individu', 'point_penilaian' => 4],
            ['komponen_id' => 'C.003', 'nama_penilaian' => 'Penegakan aturan disiplin/kode etik/kode perilaku pegawai', 'point_penilaian' => 1.5],
            ['komponen_id' => 'C.003', 'nama_penilaian' => 'Sistem Informasi Kepegawaian', 'point_penilaian' => 0.5],
            ['komponen_id' => 'C.004', 'nama_penilaian' => 'Keterlibatan pimpinan', 'point_penilaian' => 5],
            ['komponen_id' => 'C.004', 'nama_penilaian' => 'Pengelolaan Akuntabilitas Kinerja', 'point_penilaian' => 5],
            ['komponen_id' => 'C.005', 'nama_penilaian' => 'Pengendalian Gratifikasi', 'point_penilaian' => 3],
            ['komponen_id' => 'C.005', 'nama_penilaian' => 'Penerapan SPIP', 'point_penilaian' => 3],
            ['komponen_id' => 'C.005', 'nama_penilaian' => 'Pengaduan Masyarakat', 'point_penilaian' => 3],
            ['komponen_id' => 'C.005', 'nama_penilaian' => 'Whistle-Blowing System', 'point_penilaian' => 3],
            ['komponen_id' => 'C.005', 'nama_penilaian' => 'Penanganan Benturan Kepentingan', 'point_penilaian' => 3],
            ['komponen_id' => 'C.006', 'nama_penilaian' => 'Standar Pelayanan', 'point_penilaian' => 3],
            ['komponen_id' => 'C.006', 'nama_penilaian' => 'Budaya Pelayanan Prima', 'point_penilaian' => 4],
            ['komponen_id' => 'C.006', 'nama_penilaian' => 'Penilaian kepuasan terhadap pelayanan', 'point_penilaian' => 3],
            ['komponen_id' => 'C.007', 'nama_penilaian' => 'Nilai Survey Persepsi Korupsi (Survei Eksternal)', 'point_penilaian' => 15],
            ['komponen_id' => 'C.007', 'nama_penilaian' => 'Persentase temuan hasil pemeriksaan (Internal dan eksternal) yang ditindaklanjuti', 'point_penilaian' => 5],
            ['komponen_id' => 'C.008', 'nama_penilaian' => 'Nilai Persepsi Kualitas Pelayanan (Survei Eksternal)', 'point_penilaian' => 20],
        ];

        $results = [];
        $number = 1;
        foreach ($data as $key => $value) {
            // $value['id_penilaian'] = $this->setSequenceID($this->format_id);
            $value['id_penilaian'] = $this->setSequenceID($this->format_id, $number++);
            $result = $this->save($value);
            array_push($results, $result);
        }

        return $results;
    }

}
?>