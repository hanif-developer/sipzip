<?php 
namespace app\backend\repository\tabel;
use core\database\Mysql;

class TbSatker extends Mysql {

    private $format_id = 'SAT.###';

    public function __construct() {
        parent::__construct('mysqldb', 'tb_satker', 'id_satker');
        parent::setModel([
            // 'id_satker' => $this->setRandomID(),
            'id_satker' => $this->setSequenceID($this->format_id),
            'nama_satker' => '',
            'kategori_satker' => 'user', // user | admin
            'urutan_satker' => '1',
            'password' => $this->encrypt('sipzip'),
            'datetime' => $this->getDateTime(),
        ]);
    }

    public function getPilihanSatker() { 
        $result = ['' => ['text' => '-- Semua Satker --']];
        foreach ($this->getAll() as $key => $value) {
            $result[$value['id_satker']] = [
                'text' => $value['nama_satker'],
            ];
        }

        return $result;
    }

    public function createData() {
        $data = [
            'POLDA METRO JAYA',
            'SPRIPIM POLDA METRO JAYA',
            'ITWASDA POLDA METRO JAYA',
            'RORENA POLDA METRO JAYA',
            'ROOPS POLDA METRO JAYA',
            'ROSDM POLDA METRO JAYA',
            'ROLOG POLDA METRO JAYA',
            'BIDPROPAM POLDA METRO JAYA',
            'BID TIK POLDA METRO JAYA',
            'BIDDOKKES POLDA METRO JAYA',
            'BIDKEU POLDA METRO JAYA',
            'SPN POLDA METRO JAYA',
            'YANMA POLDA METRO JAYA',
            'DITINTELKAM POLDA METRO JAYA',
            'DITRESKRIMUM POLDA METRO JAYA',
            'DITRESKRIMSUS POLDA METRO JAYA',
            'DITRESNARKOBA POLDA METRO JAYA',
            'DITSAMAPTA POLDA METRO JAYA',
            'DITLANTAS POLDA METRO JAYA',
            'DITPAM OBVIT POLDA METRO JAYA',
            'DITPOLAIRUD POLDA METRO JAYA',
            'DITBINMAS POLDA METRO JAYA',
            'SATBRIMOB POLDA METRO JAYA',
            'POLRES METRO JAKARTA PUSAT',
            'POLRES METRO JAKARTA UTARA',
            'POLRES METRO JAKARTA SELATAN',
            'POLRES METRO JAKARTA BARAT',
            'POLRES METRO JAKARTA TIMUR',
            'POLRES METRO TANGERANG KOTA',
            'POLRESTA BANDARA SOETTA',
            'POLRES METRO BEKASI KOTA',
            'POLRES METRO BEKASI',
            'POLRES METRO DEPOK',
            'POLRES PELABUHAN TANJUNG PRIOK',
            'POLRES KEPULAUAN SERIBU',
            'POLRES TANGERANG SELATAN',
            'BIDKUM POLDA METRO JAYA',
            'BIDHUMAS POLDA METRO JAYA',
        ];

        $results = [];
        $number = 1;
        foreach ($data as $key => $value) {
            $form = $this->getModel();
            // $form['id_satker'] = $this->setRandomID();
            $form['id_satker'] = $this->setSequenceID($this->format_id, $number);
            $form['nama_satker'] = $value;
            $form['kategori_satker'] = ($number == 1) ? 'admin' : 'user';
            $form['password'] = ($number == 1) ? $this->encrypt('presisi123') : $this->encrypt('sipzip');
            $result = $this->save($form);
            $number++;
            array_push($results, $result);
        }

        return $results;
    }


}
?>