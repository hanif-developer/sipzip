<?php 
namespace app\backend\service;
use core\exception\{NotFoundException, ConnectionException, ValidationException, DatabaseException};
use core\{Config, Helper};
use app\backend\repository\tabel\{
    TbKategori,
    TbKomponen,
    TbPenilaian,
    TbPertanyaan,
    TbJawaban,
    TbSatker,
    TbSurvey,
    TbVerifikasi,
};

class SipzipService {

    public function __construct($user = null) {
        $this->user = $user;
        $this->configMain = Config::Load('main');

        // Init Repository
        $this->tbKategori = new TbKategori();
        $this->tbKomponen = new TbKomponen();
        $this->tbPenilaian = new TbPenilaian();
        $this->tbPertanyaan = new TbPertanyaan();
        $this->tbJawaban = new TbJawaban();
        $this->tbSatker = new TbSatker();
        $this->tbSurvey = new TbSurvey();
        $this->tbVerifikasi = new TbVerifikasi();
    }

    public function getById($cat, $id) {
        switch ($cat) {
            case 'kategori':
                $data = $this->tbKategori->getById($id);
                if (is_null($data)) {
                    throw new NotFoundException('ID '.strtoupper($cat).': '.$id.' Not Found');
                }
                break;

            case 'komponen':
                $data = $this->tbKomponen->getById($id);
                if (is_null($data)) {
                    throw new NotFoundException('ID '.strtoupper($cat).': '.$id.' Not Found');
                }
                break;

            case 'satker':
                $data = $this->tbSatker->getById($id);
                if (is_null($data)) {
                    throw new NotFoundException('ID '.strtoupper($cat).': '.$id.' Not Found');
                }
                break;

            case 'survey':
                $data = $this->tbSurvey->getById($id);
                if (is_null($data)) {
                    throw new NotFoundException('ID '.strtoupper($cat).': '.$id.' Not Found');
                }
                break;
            
            default:
                throw new NotFoundException('Modul: '.$cat.' Not Found');
                break;
        }

        return $data;
    }

    public function getPage($cat, $params, $page, $size) {
        switch ($cat) {
            case 'kategori':
                list($conditions, $params) = $this->tbKategori->createQueryParams($params, 'LIKE');
                $where = ($params) ? ' WHERE ('.implode(' OR ', $conditions).')' : '';
                $query = 'SELECT {fields} FROM `tb_kategori` kategori';
                $query .= $where;
                $query .= ' ORDER BY id_kategori ASC';
                $query = [
                    'value' => str_replace('{fields}', '*', $query),
                    'total' => str_replace('{fields}', 'COUNT(*) AS total', $query)
                ];

                $result = $this->tbKategori->getPage($query, $params, $page, $size);
                foreach ($result['contents'] as $key => $value) {
                    $result['contents'][$key] = $this->formatContentsKategori($value);
                }
                break;
            
            default:
                throw new NotFoundException('Modul: '.$cat.' Not Found');
                break;
        }

        return $result;
    }

    public function getForm($cat, $id, $satker = '') {
        switch ($cat) {
            case 'kategori':
                $result['title'] = 'Edit Kategori';
                $result['form'] = $this->tbKategori->getById($id);
                if (is_null($result['form'])) {
                    $result['title'] = 'Input Kategori';
                    $result['form'] = $this->tbKategori->getModel();
                }
                break;

            case 'survey':
                $satker = empty($satker) ? $this->user['id_satker'] : $satker;
                $form_survey = [];
                $komponen = $this->getById('komponen', $id);
                // echo $satker;die;
                $satuan_kerja = $this->getById('satker', $satker);
                $penilaians = $this->tbPenilaian->getAllByFields(['komponen_id' => $id]);
                $point_jawaban = $this->tbJawaban->getPoint();
                foreach ($penilaians as $key => $penilaian) {
                    $survey = [];
                    $pertanyaans = $this->tbPertanyaan->getAllByFields(['penilaian_id' => $penilaian['id_penilaian']]);
                    foreach ($pertanyaans as $key1 => $pertanyaan) {
                        $hasil_survey = $this->tbSurvey->getByFields(['satker_id' => $satker, 'pertanyaan_id' => $pertanyaan['id_pertanyaan']]);
                        $hasil_survey = !is_null($hasil_survey) ? $hasil_survey : $this->tbSurvey->getModel();
                        
                        // Nilai Survey (Point Jawaban)
                        $nilai_survey = isset($point_jawaban[$hasil_survey['jawaban_id']]) ? $point_jawaban[$hasil_survey['jawaban_id']] : $hasil_survey['jawaban_id'];
                        $hasil_survey['nilai_survey'] = $nilai_survey;

                        // Check dokumen pendukung, jika nilai survey lebih dari 1
                        // $required_upload_file = (($nilai_survey) > 0 && empty($hasil_survey['dokumen_pendukung'])) ? true : false;

                        $nama_dokumen = !empty($hasil_survey['nama_dokumen']) ? explode(',', $hasil_survey['nama_dokumen']) : [];
                        $dokumen_pendukung = !empty($hasil_survey['dokumen_pendukung']) ? explode(',', $hasil_survey['dokumen_pendukung']) : [];

                        $list_dokumen_pendukung = [];
                        foreach ($nama_dokumen as $key => $value) {
                            $file = isset($dokumen_pendukung[$key]) ? $dokumen_pendukung[$key] : '';
                            array_push($list_dokumen_pendukung, [
                                'name' => $value,
                                'file' => $file,
                            ]);
                        }

                        array_push($survey, [
                            'hasil_survey' => $hasil_survey,
                            'pertanyaan' => $pertanyaan,
                            // 'required_upload_file' => $required_upload_file,
                            'list_dokumen_pendukung' => $list_dokumen_pendukung,
                            'pilihan_jawaban' => $this->tbJawaban->getAllByFields(['pertanyaan_id' => $pertanyaan['id_pertanyaan']]),
                        ]);
                    }
                    $form_survey[$key]['penilaian'] = $penilaian;
                    $form_survey[$key]['survey'] = $survey;
                }
                
                $result['title'] = $komponen['nama_komponen'].' ('.$komponen['point_komponen'].')';
                $result['satker'] = $satuan_kerja['nama_satker'];
                $result['form']['id_satker'] = $satuan_kerja['id_satker'];
                $result['form']['id_komponen'] = $komponen['id_komponen'];
                $result['form']['nama_komponen'] = $komponen['nama_komponen'];
                $result['form']['status_komponen'] = '';
                $result['form']['status_verifikasi'] = 'submission';
                
                $verifikasi = $this->tbVerifikasi->getVerifikasi($id, $satker);
                // $result['form']['verifikasi'] = $verifikasi;
                if ($verifikasi) {
                    if (in_array($verifikasi['status_verifikasi'], ['revision', 'complete'])) {
                        $result['form']['status_komponen'] = 'disabled';
                        $result['form']['status_verifikasi'] = $verifikasi['status_verifikasi'];
                    }
                }

                $result['form']['survey'] = $form_survey;

                break;

            default:
                throw new NotFoundException('Modul: '.$cat.' Not Found');
                break;
        }

        return $result;
    }

    public function create($cat, $data) {
        switch ($cat) {
            case 'kategori':
                $result = $this->tbKategori->save($data);
                if (!$result['success']) {
                    // throw new DatabaseException($result['query']);
                    throw new DatabaseException($result['message']);
                    // throw new DatabaseException('Data gagal disimpan');
                }

                $result['message'] = 'Kategori '.$data['nama_kategori'].' berhasil disimpan';
                break;

            case 'survey':
                $success = [];
                $id_satker = empty($data['id_satker']) ? $this->user['id_satker'] : $data['id_satker'];
                // $survey = $this->tbSurvey->getModel();
                /** Kiriman User */
                foreach ($data['soal'] as $key => $value) {
                    $survey = $this->tbSurvey->getByFields(['komponen_id' => $data['id_komponen'], 'satker_id' => $id_satker, 'pertanyaan_id' => $key]);
                    $survey = !is_null($survey) ? $survey : $this->tbSurvey->getModel();
                    $survey['komponen_id'] = $data['id_komponen'];
                    $survey['satker_id'] = $id_satker;
                    $survey['pertanyaan_id'] = $key;
                    $survey['jawaban_id'] = $value;
                    $result = $this->tbSurvey->save($survey);
                    if (!$result['success']) {
                        // throw new DatabaseException($result['query']);
                        throw new DatabaseException($result['message']);
                        // throw new DatabaseException('Data gagal disimpan');
                    }

                    array_push($success, $survey);
                }

                /** Kiriman Moderator */
                foreach ($data['catatan'] as $key => $value) {
                    $survey = $this->tbSurvey->getByFields(['komponen_id' => $data['id_komponen'], 'satker_id' => $id_satker, 'pertanyaan_id' => $key]);
                    $survey = !is_null($survey) ? $survey : $this->tbSurvey->getModel();
                    $survey['komponen_id'] = $data['id_komponen'];
                    $survey['satker_id'] = $id_satker;
                    $survey['pertanyaan_id'] = $key;
                    $survey['catatan_revisi'] = $value;
                    $result = $this->tbSurvey->save($survey);
                    if (!$result['success']) {
                        // throw new DatabaseException($result['query']);
                        throw new DatabaseException($result['message']);
                        // throw new DatabaseException('Data gagal disimpan');
                    }

                    array_push($success, $survey);
                }

                // Simpan data verifikasi survey
                $verifikasi = $this->tbVerifikasi->getByFields(['komponen_id' => $data['id_komponen'], 'satker_id' => $id_satker]);
                $verifikasi = !is_null($verifikasi) ? $verifikasi : $this->tbVerifikasi->getModel();
                $verifikasi['komponen_id'] = $data['id_komponen'];
                $verifikasi['satker_id'] = $id_satker;
                $verifikasi['user_id'] = 'SAT.001';
                $verifikasi['status_verifikasi'] = $data['status_verifikasi'];
                $resultVerifikasi = $this->tbVerifikasi->save($verifikasi);
                if (!$resultVerifikasi['success']) {
                    // throw new DatabaseException($result['query']);
                    throw new DatabaseException($resultVerifikasi['message']);
                    // throw new DatabaseException('Data gagal disimpan');
                }

                $result['data']['survey'] = $success;
                $result['data']['verifikasi'] = $resultVerifikasi;
                $result['message'] = 'Survey '.$data['nama_komponen'].' berhasil disimpan';
                break;

            case 'upload':
                $result = $this->tbSurvey->save($data);
                if (!$result['success']) {
                    // throw new DatabaseException($result['query']);
                    throw new DatabaseException($result['message']);
                    // throw new DatabaseException('Data gagal disimpan');
                }

                $result['message'] = 'File Pendukung telah diupload';
                break;
            
            default:
                throw new NotFoundException('Modul: '.$cat.' Not Found');
                break;
        }

        return $result;
    }

    public function update($cat, $id, $update) {
        $data = $this->getById($cat, $id);
        $data = Helper::filterParams($data, $update);
        $result = $this->create($cat, $data);
        return $result;
    }

    public function delete($cat, $id) {
        $data = $this->getById($cat, $id);
        switch ($cat) {
            case 'kategori':
                $result = $this->tbKategori->delete($data);
                if (!$result['success']) {
                    // throw new DatabaseException($result['query']);
                    throw new DatabaseException($result['message']);
                    // throw new DatabaseException('Data gagal dihapus');
                }

                $result['message'] = 'Kategori '.$data['nama_kategori'].' berhasil dihapus';
                break;

            default:
                throw new NotFoundException('Modul: '.$cat.' Not Found');
                break;
        }

        return $result;
    }

    /**
     * Custom Function
     */
    private function formatContentsKategori($content) {
        $result = $content;
        return $result;
    }

    public function getTableVerifikasi() {
        $pilihan_status = $this->tbVerifikasi->getPilihanStatus();
        $query = 'SELECT * FROM `tb_verifikasi` verif 
                    JOIN `tb_satker` satker ON (verif.`satker_id`=satker.`id_satker`)
                    JOIN `tb_komponen` komponen ON (verif.`komponen_id`=komponen.`id_komponen`)
                    WHERE (satker.`kategori_satker` <> "admin")';
                    
        $result = $this->tbVerifikasi->getQuery($query);
        $data = [];
        foreach ($result['value'] as $key => $value) {
            $data[$key]['id_satker'] = $value['satker_id'];
            $data[$key]['id_komponen'] = $value['komponen_id'];
            $data[$key]['nama_satker'] = $value['nama_satker'];
            $data[$key]['nama_komponen'] = $value['nama_komponen'];
            $data[$key]['status_verifikasi'] = $value['status_verifikasi'];
            $data[$key]['status_verifikasi_text'] = $pilihan_status[$value['status_verifikasi']]['text'];
            $data[$key]['status_verifikasi_color'] = $pilihan_status[$value['status_verifikasi']]['color'];
        }
        
        return $data;
    }

    public function getDataLKE($id_satker = '') {
        $id_satker = empty($id_satker) ? $this->user['id_satker'] : $id_satker;
        $summary = [];
        $survey = [];
        $nilai_zi = 0;
        $category = $this->tbKategori->getAll();
        $pilihan_status = $this->tbVerifikasi->getPilihanStatus();

        foreach ($category as $cat) {
            $view_point_kategori = $this->tbKategori->getQuery('SELECT * FROM `view_point_kategori` WHERE (satker_id = ?) AND (id_kategori = ?)', [$id_satker, $cat['id_kategori']]);
            $data_point_kategori = ($view_point_kategori['count'] > 0) ? current($view_point_kategori['value']) : [];
            $point = !empty($data_point_kategori) ? $data_point_kategori['total_point_kategori'] : 0;
            $point = round($point, 2);
            // $point = rand(0, floatval($cat['point_kategori']));
            $point_kategori = floatval($cat['point_kategori']);
            $persentase_point = round(($point/$point_kategori) * 100);
            $nilai_zi += $point;
            array_push($summary, [
                'categori' => 'Total '.$cat['nama_kategori'],
                'point' => $point,
                'point_kategori' => $point_kategori,
                'persentase_point' => $persentase_point,
            ]);
            
            $result['category'] = $cat['nama_kategori'].' ('.$cat['point_kategori'].')';
            $result['data'] = [];
            $komponen = $this->tbKomponen->getAllByFields(['kategori_id' => $cat['id_kategori']]);
            foreach ($komponen as $komp) {
                $view_point_komponen = $this->tbKomponen->getQuery('SELECT * FROM `view_point_komponen` WHERE (satker_id = ?) AND (id_komponen = ?)', [$id_satker, $komp['id_komponen']]);
                $view_soal_survey = $this->tbKomponen->getQuery('SELECT COUNT(*) AS jumlah_soal FROM `view_soal_survey` WHERE (id_komponen = ?)', [$komp['id_komponen']]);
                $data_point_komponen = ($view_point_komponen['count'] > 0) ? current($view_point_komponen['value']) : [];
                $data_soal_survey = ($view_soal_survey['count'] > 0) ? current($view_soal_survey['value']) : [];
                $point = !empty($data_point_komponen) ? $data_point_komponen['total_point_komponen'] : 0;
                $point = round($point, 2);
                $terjawab = !empty($data_point_komponen) ? $data_point_komponen['terjawab'] : 0;
                $jumlah_soal = !empty($data_soal_survey) ? $data_soal_survey['jumlah_soal'] : 0;
                // $point = rand(0, floatval($cat['point_kategori']));
                // $terjawab = rand(0, 10);
                // $jumlah_soal = rand(10, 15);
                $persentase_terjawab = round(($terjawab/$jumlah_soal) * 100);

                $verifikasi = $this->tbVerifikasi->getByFields(['komponen_id' => $komp['id_komponen'], 'satker_id' => $id_satker]);
                $data['verifikasi'] = [];
                if ($verifikasi) {
                    $data['verifikasi']['status_verifikasi'] = $verifikasi['status_verifikasi'];
                    $data['verifikasi']['status_verifikasi_text'] = $pilihan_status[$verifikasi['status_verifikasi']]['text'];
                    $data['verifikasi']['status_verifikasi_color'] = $pilihan_status[$verifikasi['status_verifikasi']]['color'];
                }

                $data['id'] = $komp['id_komponen'];
                $data['komponen'] = $komp['nama_komponen'].' ('.$komp['point_komponen'].')';
                $data['point_komponen'] = $komp['point_komponen'];
                $data['point'] = $point;
                $data['terjawab'] = $terjawab;
                $data['jumlah_soal'] = $jumlah_soal;
                $data['persentase_terjawab'] = $persentase_terjawab;
                array_push($result['data'], $data);
            }

            
            array_push($survey, $result);
        }

        return [
            'satker' => $this->getById('satker', $id_satker),
            'summary' => $summary,
            'survey' => $survey,
            'nilai_zi' => $nilai_zi,
        ];
    }

    public function getGrafikNilaiZI() {
        $result = [];
        $categories = [];
        $series = [];
        foreach ($this->tbKategori->getAll() as $key => $kat) {
            // $total_point = 0;
            foreach ($this->tbSatker->getAll() as $sat) {
                $view_point_kategori = $this->tbKategori->getQuery('SELECT * FROM `view_point_kategori` WHERE (satker_id = ?) AND (id_kategori = ?)', [$sat['id_satker'], $kat['id_kategori']]);
                $data_point_kategori = ($view_point_kategori['count'] > 0) ? current($view_point_kategori['value']) : [];
                $point = !empty($data_point_kategori) ? floatval($data_point_kategori['total_point_kategori']) : 0;
                // $point = rand(0, floatval($kat['point_kategori']));
                $result[$sat['id_satker']]['satker'] = $sat['nama_satker'];
                $result[$sat['id_satker']]['total_point'] += $point;
                $result[$sat['id_satker']]['point_kategori'][$key]['kategori'] = $kat['nama_kategori'];
                $result[$sat['id_satker']]['point_kategori'][$key]['nilai_point'] = round($point, 2);
                $result[$sat['id_satker']]['point_kategori'][$key]['query'] = $view_point_kategori['query'];
            }
        }

        // Urutkan data berdasarkan total nilai tertinggi
        $order_by_total = array_column($result, 'total_point');
        array_multisort($order_by_total, SORT_DESC, $result);

        // Buat data grafik
        $index = 0;
        foreach ($result as $id => $value) {
            array_push($categories, $value['satker']);
            foreach ($value['point_kategori'] as $key => $val) {
                $series[$key]['name'] = 'Total '.$val['kategori'];
                // $series[$key]['data'][$index] = $val['nilai_point'];
                $series[$key]['data'][$index]['url'] = '/lke/satker/'.$id;
                $series[$key]['data'][$index]['name'] = $value['satker'];
                $series[$key]['data'][$index]['y'] = $val['nilai_point'];
            }

            $index++;
        }

        $result = [
            'categories' => $categories,
            'series' => $series,
        ];

        return $result;
    }

    /**
     * #view_soal_survey
     * view untuk mendapatkan jumlah soal/pertanyaan dari tiap2 penilaian/komponen/kategori
     * 
     * #view_point_survey
     * view untuk mendapatkan nilai survey sesuai point jawaban, jika jawaban adalah input text
     * maka nilai survey sama dengan nilai inputan dibagi dengan point pertanyaan
     * nilai_survey = point_jawaban | nilai_inputan / point_pertanyaan
     * 
     * #view_point_penilaian
     * view untuk mendapatkan soal terjawab dan jumlah soal yang ada pada group penilaian serta
     * menghitung nilai penilaian
     * nilai_penilaian = jumlah_nilai_survey / jumlah_soal * point_penilaian
     * 
     * #view_point_komponen
     * view untuk mendapatkan soal terjawab dan jumlah soal yang ada pada group komponen serta
     * menghitung nilai komponen
     * nilai_komponen = jumlah_nilai_penilaian
     * 
     * #view_point_kategori
     * view untuk menghitung nilai kategori
     * nilai_kategori = jumlah_nilai_komponen / point_kategori
     * 
     */
    public function createView() {
//         $query_old = <<<EOF
// --SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
// SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

// CREATE OR REPLACE VIEW view_soal_survey AS 
// SELECT * FROM `tb_pertanyaan` pertanyaan
// JOIN `tb_penilaian` penilaian ON (pertanyaan.`penilaian_id`=penilaian.`id_penilaian`)
// JOIN `tb_komponen` komponen ON (penilaian.`komponen_id`=komponen.`id_komponen`)
// JOIN `tb_kategori` kategori ON (komponen.`kategori_id`=kategori.`id_kategori`);

// CREATE OR REPLACE VIEW view_point_survey AS 
// SELECT
// survey.`satker_id`,
// survey.`pertanyaan_id`,
// survey.`jawaban_id`,
// survey.`dokumen_pendukung`,
// pertanyaan.`point_pertanyaan`,
// jawaban.`point_jawaban`,
// IFNULL(jawaban.`point_jawaban`, (survey.`jawaban_id` / pertanyaan.`point_pertanyaan`)) AS jawaban_survey,
// IF((SELECT(jawaban_survey) > 0 ) AND (`dokumen_pendukung` = ""), 0, (SELECT(jawaban_survey))) AS point_survey
// FROM `tb_survey` survey
// JOIN `tb_pertanyaan` pertanyaan ON (survey.`pertanyaan_id`=pertanyaan.`id_pertanyaan`)
// LEFT JOIN `tb_jawaban` jawaban ON (survey.`jawaban_id`=jawaban.`id_jawaban`);

// CREATE OR REPLACE VIEW view_point_penilaian AS 
// SELECT
// penilaian.`id_penilaian`,
// penilaian.`nama_penilaian`,
// penilaian.`komponen_id`,
// COUNT(survey.`pertanyaan_id`) AS terjawab,
// (SELECT COUNT(*) FROM `view_soal_survey` WHERE (`penilaian_id` = penilaian.`id_penilaian`)) AS jumlah_soal,
// survey.*,
// penilaian.`point_penilaian`,
// SUM(survey.`point_survey`) AS total_point_survey,
// (SUM(survey.`point_survey`) / (SELECT(jumlah_soal)) * penilaian.`point_penilaian`) AS total_point_penilaian
// FROM `view_point_survey` survey
// JOIN `tb_pertanyaan` pertanyaan ON (survey.`pertanyaan_id`=pertanyaan.`id_pertanyaan`)
// JOIN `tb_penilaian` penilaian ON (pertanyaan.`penilaian_id`=penilaian.`id_penilaian`)
// GROUP BY penilaian.`id_penilaian`, survey.`satker_id`
// ORDER BY penilaian.`id_penilaian`;

// CREATE OR REPLACE VIEW view_point_komponen AS 
// SELECT
// v_penilaian.`satker_id`,
// SUM(v_penilaian.`terjawab`) AS terjawab,
// (SELECT COUNT(*) FROM `view_soal_survey` WHERE (`id_komponen` = komponen.`id_komponen`)) AS jumlah_soal,
// komponen.*,
// SUM(v_penilaian.`total_point_penilaian`) AS total_point_komponen
// FROM `view_point_penilaian` AS v_penilaian
// JOIN `tb_komponen` komponen ON (v_penilaian.`komponen_id`=komponen.`id_komponen`)
// GROUP BY komponen.`id_komponen`, v_penilaian.`satker_id`
// ORDER BY komponen.`id_komponen`;

// CREATE OR REPLACE VIEW view_point_kategori AS 
// SELECT
// v_komponen.`satker_id`,
// kategori.*,
// SUM(v_komponen.`total_point_komponen`) AS total_point_kategori,
// ROUND((SUM(v_komponen.`total_point_komponen`) / kategori.`point_kategori` * 100), 2) AS persentase_point_kategori
// FROM `view_point_komponen` AS v_komponen
// JOIN `tb_kategori` kategori ON (v_komponen.`kategori_id`=kategori.`id_kategori`)
// GROUP BY kategori.`id_kategori`, v_komponen.`satker_id`
// ORDER BY kategori.`id_kategori`;
// EOF;

        $query = <<<EOF
--SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

CREATE OR REPLACE VIEW view_soal_survey AS 
SELECT * FROM `tb_pertanyaan` pertanyaan
JOIN `tb_penilaian` penilaian ON (pertanyaan.`penilaian_id`=penilaian.`id_penilaian`)
JOIN `tb_komponen` komponen ON (penilaian.`komponen_id`=komponen.`id_komponen`)
JOIN `tb_kategori` kategori ON (komponen.`kategori_id`=kategori.`id_kategori`);

CREATE OR REPLACE VIEW view_point_survey AS 
SELECT
survey.`satker_id`,
survey.`pertanyaan_id`,
survey.`jawaban_id`,
survey.`dokumen_pendukung`,
pertanyaan.`point_pertanyaan`,
jawaban.`point_jawaban`,
IFNULL(jawaban.`point_jawaban`, (survey.`jawaban_id` / pertanyaan.`point_pertanyaan`)) AS point_survey
FROM `tb_survey` survey
JOIN `tb_pertanyaan` pertanyaan ON (survey.`pertanyaan_id`=pertanyaan.`id_pertanyaan`)
LEFT JOIN `tb_jawaban` jawaban ON (survey.`jawaban_id`=jawaban.`id_jawaban`);

CREATE OR REPLACE VIEW view_point_penilaian AS 
SELECT
penilaian.`id_penilaian`,
penilaian.`nama_penilaian`,
penilaian.`komponen_id`,
COUNT(survey.`pertanyaan_id`) AS terjawab,
(SELECT COUNT(*) FROM `tb_pertanyaan` WHERE (`penilaian_id` = penilaian.`id_penilaian`)) AS jumlah_soal,
survey.*,
penilaian.`point_penilaian`,
SUM(survey.`point_survey`) AS total_point_survey,
(SUM(survey.`point_survey`) / (SELECT(jumlah_soal)) * penilaian.`point_penilaian`) AS total_point_penilaian
FROM `view_point_survey` survey
JOIN `tb_pertanyaan` pertanyaan ON (survey.`pertanyaan_id`=pertanyaan.`id_pertanyaan`)
JOIN `tb_penilaian` penilaian ON (pertanyaan.`penilaian_id`=penilaian.`id_penilaian`)
GROUP BY penilaian.`id_penilaian`, survey.`satker_id`
ORDER BY penilaian.`id_penilaian`;

CREATE OR REPLACE VIEW view_point_komponen AS 
SELECT
v_penilaian.`satker_id`,
SUM(v_penilaian.`terjawab`) AS terjawab,
(SELECT COUNT(*) FROM `view_soal_survey` WHERE (`id_komponen` = komponen.`id_komponen`)) AS jumlah_soal,
komponen.*,
IF((verifikasi.`status_verifikasi` = "complete"), SUM(v_penilaian.`total_point_penilaian`), 0) AS total_point_komponen,
verifikasi.`status_verifikasi`
FROM `view_point_penilaian` AS v_penilaian
JOIN `tb_komponen` komponen ON (v_penilaian.`komponen_id`=komponen.`id_komponen`)
JOIN `tb_verifikasi` verifikasi ON (verifikasi.`komponen_id`=komponen.`id_komponen`) AND (verifikasi.`satker_id`=v_penilaian.`satker_id`)
GROUP BY komponen.`id_komponen`, v_penilaian.`satker_id`
ORDER BY komponen.`id_komponen`;

CREATE OR REPLACE VIEW view_point_kategori AS 
SELECT
v_komponen.`satker_id`,
kategori.*,
SUM(v_komponen.`total_point_komponen`) AS total_point_kategori,
ROUND((SUM(v_komponen.`total_point_komponen`) / kategori.`point_kategori` * 100), 2) AS persentase_point_kategori
FROM `view_point_komponen` AS v_komponen
JOIN `tb_kategori` kategori ON (v_komponen.`kategori_id`=kategori.`id_kategori`)
GROUP BY kategori.`id_kategori`, v_komponen.`satker_id`
ORDER BY kategori.`id_kategori`;
EOF;

        $result = $this->tbSurvey->getQuery($query);
        return $result;
    }

}
?>