<?php 
namespace app\backend\controller;
use core\{RestApi, Helper, Fcm};
use core\exception\{NotFoundException, ValidationException};
use oauth\{UserAuth};

class v1 extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->sipzip = new sipzip();

        $this->dashboardMapping();
        $this->SurveyMapping();
        // $this->requestMapping('GET', '/', function() {
        //     $this->getRoute();
        // });
    }

    /**
     * Modul Dashboard
     */
    private function dashboardMapping() {
        $this->requestMapping('GET', '/grafik/nilaizi', function() {
            $this->sipzip->getGrafikNilaiZI();
        }, 'Grafik Total Nilai ZI');
    }

    /**
     * Modul Survey
     */
    private function SurveyMapping() {
        $this->requestMapping('POST', '/survey', function() {
            $this->sipzip->create('survey');
        }, 'Simpan Data Survey');

        $this->requestMapping('POST', '/survey/upload', function() {
            $this->sipzip->upload('survey');
        }, 'Upload Dokumen Pendukung Survey');
    }


}

?>