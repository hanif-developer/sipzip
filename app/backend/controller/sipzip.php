<?php 
namespace app\backend\controller;
use core\{RestApi, Helper};
use core\exception\{NotFoundException, ValidationException};
use oauth\UserAuth;
use app\backend\service\SipzipService;

class Sipzip extends RestApi {
    
    public function __construct() {
        parent::__construct();
        $this->auth = UserAuth::getInstance();
        list($token, $user) = $this->auth->getAuthorization();
        $this->user = $user;
        $this->sipzipService = new SipzipService($user);
        $this->settings = $this->getSettingList();
    }

    public function getGrafikNilaiZI() {
        Helper::$responseMessage['data'] = $this->sipzipService->getGrafikNilaiZI();
        $this->showResponse(Helper::$responseMessage);
    }

    public function create($cat) {
        $data = $this->getFields();
        $result = $this->sipzipService->create($cat, $data);
        Helper::$responseMessage['message'] = $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function upload($cat) {
        $data = $this->getFields();
        $form_survey = $this->sipzipService->tbSurvey->getByFields(['satker_id' => $this->user['id_satker'], 'pertanyaan_id' => $data['pertanyaan_id']]);
        $form_survey = !is_null($form_survey) ? $form_survey : $this->sipzipService->tbSurvey->getModel();
        $form_survey['satker_id'] = $this->user['id_satker'];
        $form_survey['pertanyaan_id'] = $data['pertanyaan_id'];
        $form_survey['komponen_id'] = $data['komponen_id'];
        $file_pendukung = $this->uploadFile('file_pendukung', [
            'path_upload' => $this->settings['upload']['path_upload'],
            'file_type' => $this->settings['upload']['file_type'],
            'max_size' => $this->settings['upload']['max_size'],
        ]);

        $nama_dokumen = $form_survey['nama_dokumen'];
        $dokumen_pendukung = $form_survey['dokumen_pendukung'];
        $nama_dokumen = !empty($nama_dokumen) ? explode(',', $nama_dokumen) : [];
        $dokumen_pendukung = !empty($dokumen_pendukung) ? explode(',', $dokumen_pendukung) : [];
        if ($file_pendukung['file_upload']) {
            array_push($dokumen_pendukung, $file_pendukung['file_upload']);
            array_push($nama_dokumen, $file_pendukung['data']['name']);
            $form_survey['dokumen_pendukung'] = implode(',', $dokumen_pendukung);
            $form_survey['nama_dokumen'] = implode(',', $nama_dokumen);
        }

        $result = $this->sipzipService->create('upload', $form_survey);
        Helper::$responseMessage['message'] = $result['message'];
        Helper::$responseMessage['data'] = $file_pendukung;

        // Helper::$responseMessage['message'] = 'TES';
        // Helper::$responseMessage['data'] = $form_survey;
        // Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function update($cat, $id) {
        $data = $this->getBody();
        $result = $this->sipzipService->update($cat, $id, $data);
        Helper::$responseMessage['message'] = $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

    public function delete($cat, $id) {
        $result = $this->sipzipService->delete($cat, $id);
        Helper::$responseMessage['message'] = $result['message'];
        Helper::$responseMessage['data'] = $result['data'];
        $this->showResponse(Helper::$responseMessage);
    }

}

?>