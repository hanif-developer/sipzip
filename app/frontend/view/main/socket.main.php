console.log("Socket Server ...");
let socket = null;
const $_socket = JSON.parse(window.atob("<?= $this->settings->socket; ?>"));
connectSocket = function() {
    $.getScript($_socket.script, function(data, status, xhr) {
        // console.log(status);
        socket = io.connect($_socket.server, {
            query: {token: $_socket.token}
        });
        socket.on("connected", (client) => {
            console.clear();
            console.log("Connected", client);
            socket.emit("joinRoom", $_socket.room);

            // Hubungkan user ke socket
            socket.emit("userConnect", "user_"+client);
        });
        socket.on("disconnect", (client) => {
            console.clear();
            // // Saat koneksi socket terputus
        });
        socket.on("userOnline", (users) => {
            // console.log("userOnline", users);
        });
        socket.on("chatMessage", (message) => {
            // console.log("chatMessage", message);
            if (message.text === "monitor_laporan") {
                $(document).find(".btn-reload").trigger("click");
            }
        });
    });
}

sendMessage = function(textMessage) {
    const room = $_socket.room;
    socket.emit("sendMessage", { room, textMessage });
}

connectSocket();