<!-- BEGIN: Top Menu -->
<nav class="top-nav">
    <ul>
        <?php createSidebar($this, $this->settings->navbar); ?>
        <!-- <li>
            <a href="javascript:;.html" class="top-menu top-menu--active">
                <div class="top-menu__icon"> <i data-feather="home"></i> </div>
                <div class="top-menu__title"> Dashboard <i data-feather="chevron-down" class="top-menu__sub-icon"></i> </div>
            </a>
            <ul class="top-menu__sub-open">
                <li>
                    <a href="top-menu-light-dashboard-overview-1.html" class="top-menu">
                        <div class="top-menu__icon"> <i data-feather="activity"></i> </div>
                        <div class="top-menu__title"> Overview 1 </div>
                    </a>
                </li>
            </ul>
        </li> -->
    </ul>
</nav>
<!-- END: Top Menu -->
<?php
function createSidebar($object, $navbar) {
    foreach ($navbar as $key => $nav) {
        $nav['url'] = is_array($nav['url']) ? $object->createLink($nav['url']) : $nav['url'];
        $active = ($object->getActiveUrl() == $nav['url'].'/') ? 'top-menu--active' : '';
        $rule = isset($nav['rule']) ? $nav['rule'] : [];
        $regional = isset($nav['regional']) ? $nav['regional'] : [];
        
        // check rule petugas|operator|administrator|superadmin
        $allow = !empty($rule) ? in_array($object->session->data->level, $rule) : true;
        
        if ($allow) {
            echo '<li>';
            if (isset($nav['sub']) && !empty($nav['sub'])) {
                echo '<a href="javascript:;" class="top-menu '.$active.'">';
                echo '<div class="top-menu__icon"> '.$nav['icon'].' </div>';
                echo '<div class="top-menu__title"> '.$nav['text'].' <i data-feather="chevron-down" class="menu__sub-icon"></i> </div>';
                echo '</a>';
                echo '<ul class="">';
                createSidebar($object, $nav['sub']);
                echo '</ul>';	
            }
            else {
                echo '<a href="'.$nav['url'].'" class="top-menu '.$active.'">';
                echo '<div class="top-menu__icon"> '.$nav['icon'].' </div>';
                echo '<div class="top-menu__title"> '.$nav['text'].' </div>';
                echo '</a>';
            }
            echo '</li>';
        }
    }
}
?>