<?php $this->getHeader() ?>
<div class="wrapper">
	<div class="container-fluid">

		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<div class="page-title-box">
					<div class="btn-group float-right">
						<?= $breadcrumb ?>
					</div>
				</div>
			</div>
		</div>
		<!-- end page title end breadcrumb -->

		<div class="row">
			<div class="col-sm-6 offset-sm-3">
				<div class="text-center mt-5">
					<h1 class="text-error"><?= $code ?></h1>
					<h4 class="text-uppercase text-danger mt-3">Page Not Found</h4>
					<p class="text-muted mt-3"><?= $message ?></p>
					<a class="btn btn-md btn-custom waves-effect waves-light mt-3" href="javascript:history.back()"> Return Home</a>
				</div>

			</div><!-- end col -->
		</div>
		<!-- end row -->

	</div> <!-- end container -->
</div>
<!-- end wrapper -->
<?php $this->getFooter() ?>