<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo" style="margin-left: -30px;">
                <!-- Text Logo -->
                <a href="javascript:;" class="logo">
                    <img src="<?= $this->settings->web['ic_logo'] ?>" alt="" height="50" class="logo-small">
                    <img src="<?= $this->settings->web['ic_logo'] ?>" alt="" height="50" class="logo-large">
                </a>
            </div>
            <!-- End Logo container-->

            <div class="menu-extras topbar-custom">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>
                    
                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="false" aria-expanded="false">
                            <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name"><?= $this->session->data->user ?> <i class="mdi mdi-chevron-down"></i> </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                            <!-- item-->
                            <a href="<?= $this->main_url.'/logout'; ?>" class="dropdown-item notify-item">
                                <i class="fi-power"></i> <span>Logout</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <?php createSidebar($this, $this->settings->navbar); ?>
                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->
<?php
function createSidebar($object, $navbar) {
    foreach ($navbar as $key => $nav) {
        $nav['url'] = is_array($nav['url']) ? $object->createLink($nav['url']) : $nav['url'];
        $active = ($object->getActiveUrl() == $nav['url'].'/') ? 'active' : '';
        $rule = isset($nav['rule']) ? $nav['rule'] : [];
        
        // check rule petugas|operator|administrator|superadmin
        $allow = !empty($rule) ? in_array($object->session->data->level, $rule) : true;
        
        if ($allow) {
            echo '<li class="has-submenu">';
            if (isset($nav['sub']) && !empty($nav['sub'])) {
                echo '<a href="javascript:;">'.$nav['icon'].' '.$nav['text'].'</a>';
                echo '<ul class="submenu">';
                createSidebar($object, $nav['sub']);
                echo '</ul>';	
            }
            else {
                echo '<a href="'.$nav['url'].'">'.$nav['icon'].' '.$nav['text'].'</a>';
            }
            echo '</li>';
        }
    }
}
?>