<!-- Begin page -->
<style>
    html {
        overflow: hidden;
    }
    body.login {
        height: 100vh;
        background-image: url('<?= $this->settings->web['bg_login'] ?>');
        background-size: cover;
        background-position: center;
    }

    .fzprogress {
        background-color: transparent;
    }

    .center-element {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .account-page-login {
        /* border: 1px solid red; */
        width: 800px;
        height: 400px;
    }

    .img-login {
        height: 120px;
        margin-top: 15%;
    }


    @media (max-width: 480px) {
        .img-login {
            margin-top: 0;
        }
    }
</style>
<div id="login" class="account-page-login">
    <div class="row">
        <div class="col-md-6">
            <div class="center-element"><img src="<?= $this->settings->web['ic_logo'] ?>" alt="" class="img-login"></div>
        </div>
        <div class="col-md-6">
            <div class="center-element" style="height: 250px;">
                <form id="form-login" method="post" action="<?= $this->getBaseUrl().'/oauth/user/auth' ?>" onsubmits="return false;">
                    <div class="card p-4" style="width: 350px;filter: opacity(.7);background-color: #f1f1f1;">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <!-- <label for="username">Satker</label> -->
                                <?= $this->html::inputSelect('username', $pilihan_satker, '', 'class="form-control select2" autofocus required') ?>
                            </div>
                        </div>
                        <div class="form-group row m-b-5">
                            <div class="col-md-12">
                                <!-- <label for="password">Password</label> -->
                                <?= $this->html::inputText('password', 'password', '', 'class="form-control" placeholder="Enter your password" required') ?>
                            </div>
                        </div>
                        <div class="form-group row text-center m-t-10">
                            <div class="col-md-12 p-15">
                                <button class="btn btn-block btn-rounded btn-primary waves-effect waves-light" type="submit">Sign In</button>
                            </div>
                        </div>
                        <div class="row m-t-5">
                            <div class="col-md-12 p-15 error-message"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center"><img src="<?= $this->settings->web['ic_presisi'] ?>" alt="" class="img-login"></div>
        </div>
    </div>
</div>