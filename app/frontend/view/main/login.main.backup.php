<!-- Begin page -->
<style>
    body.login {
        overflow: hidden;
    }

    .fzprogress {
        background-color: transparent;
    }

    .background-page-login {
        position: absolute;
        background-image: url('<?= $this->settings->web['bg_login'] ?>');
        background-size: cover;
        background-position: center;
        height: 100%;
        width: 100%;
        top: 0;
    }

    .account-page-login {
        /* border: 1px solid red; */
        position: absolute;
        width: 800px;
        height: 400px;
        z-index: 15;
        top: 50%;
        left: 50%;
        margin-top: -200px;
        margin-left: -400px;
        background-image: url('<?= $this->settings->web['bg_form_login'] ?>');
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>
<div class="background-page-login"></div>
<div id="login" class="account-page-login">
    <div class="row">
        <div class="col-6 p-3">
            <span><img src="<?= $this->settings->web['ic_presisi'] ?>" alt="" style="position: absolute;bottom: -15px;left: 60px;height: 100px;"></span>
        </div>
        <div class="col-6 p-3">
            <h2 class="text-uppercase text-center mb-4">
                <span><img src="<?= $this->settings->web['ic_logo'] ?>" alt="" height="60"></span>
            </h2>
            <form id="form-login" method="post" action="<?= $this->getBaseUrl().'/oauth/user/auth' ?>" onsubmits="return false;">
                <div class="form-group row">
                    <div class="col-12 pr-5 pl-5">
                        <label for="username">Satker</label>
                        <?= $this->html::inputSelect('username', $pilihan_satker, '', 'class="form-control select2" autofocus required') ?>
                    </div>
                </div>
                <div class="form-group row m-b-5">
                    <div class="col-12 pr-5 pl-5">
                        <label for="password">Password</label>
                        <?= $this->html::inputText('password', 'password', '', 'class="form-control" placeholder="Enter your password" required') ?>
                    </div>
                </div>
                <div class="form-group row text-center m-t-10">
                    <div class="col-8 offset-2 p-15">
                        <button class="btn btn-block btn-rounded btn-primary waves-effect waves-light" type="submit">Sign In</button>
                    </div>
                </div>
                <div class="row m-t-5">
                    <div class="col-8 offset-2 p-15 error-message"></div>
                </div>
            </form>
        </div>
    </div>
</div>