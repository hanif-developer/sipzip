<?php $this->getHeader() ?>
<div class="wrapper">
    <div id="dashboard" class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <?= $breadcrumb ?>
                    </div>
                    <h4 class="page-title"><?= $page_title ?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div id="chart-lke" class="card-box">
                    <div class="error-message"></div>
                    <div id="chart-progress" style="height: 300px;"></div>
                </div>
            </div>
        </div>

    </div> <!-- end container -->
</div>
<!-- end wrapper -->
<?php $this->getFooter() ?>