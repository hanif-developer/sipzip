if ($("#dashboard").length > 0) {
	const dashboard = new Frameduz("#dashboard");
	const progress = dashboard.createProgress(dashboard.modul.find("#chart-progress"));
	setTimeout(function () {
		dashboard.sendRequest("GET", {
			url: api_url + "/grafik/nilaizi",
			header: requestHeader,
			onSuccess: function (response) {
				// console.log(response);
				createChart(response.data);
			},
			onError: function (error) {
				// console.log(error);
				progress.remove();
				showMessage(dashboard.modul.find(".error-message"), error);
			}
		});
	}, 1000);

	function createChart(data) {
		// console.log(data.series);
		Highcharts.chart("chart-lke", {
			chart: {
				type: "column",
			},
			title: {
				text: "Nilai ZI"
			},
			xAxis: {
				// categories: data.categories,
				type: 'category',
				min: 0,
				max: 10,
				scrollbar: {
					enabled: true
				},
			},
			yAxis: {
				min: 0,
				title: {
					text: "Total Point"
				},
				stackLabels: {
					enabled: true,
					style: {
						fontWeight: "bold",
						color: ( // theme
							Highcharts.defaultOptions.title.style &&
							Highcharts.defaultOptions.title.style.color
						) || "gray"
					}
				},
			},
			legend: {
				align: "right",
				x: -30,
				verticalAlign: "bottom",
				y: -25,
				floating: false,
				backgroundColor:
					Highcharts.defaultOptions.legend.backgroundColor || "white",
				borderColor: "#CCC",
				borderWidth: 1,
				shadow: false
			},
			tooltip: {
				headerFormat: "<b>{point.x}</b><br/>",
				// pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
				pointFormat: "{series.name}: {point.y}"
			},
			plotOptions: {
				column: {
					stacking: "normal",
					dataLabels: {
						enabled: true
					},
					cursor: "pointer",
					point:{
                        events:{
                            click:function(e){
								// window.location = this.options.url
								// console.log(this.options.url);
								const link = document.createElement("a");
								link.href = this.options.url;
								link.target = "_blank";
								link.click();
                            }
                        }
                    }
				},
			},
			series: data.series,
			credits: {
				enabled: false
			},
		});
	}
	
}

if ($("#login").length > 0) {
	const login = new Frameduz("#login");
    form_login = login.modul.find("#form-login");
	form_login.on("submit", function(e) {
		e.preventDefault();
		const auth_url = $(this).attr("action");
		const params = $(this).serializeObject();
		const progress = login.createProgress(form_login);
		setTimeout(function(){
			login.sendRequest("POST", {
				url: auth_url, 
				data: params,
				onSuccess: function(response) {
					// console.log(response);
					progress.remove();
					window.location.reload();
				},
				onError: function(error) {
					// console.log(error);
					progress.remove();
					alert(error.message);
				}
			});
		}, 1000);

        return false;
	});

}