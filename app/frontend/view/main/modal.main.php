<div id="modal-fade" class="modal fade" data-backdrop="static" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal__content">
            <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
                <h2 class="font-medium text-base mr-auto modal-title"><strong>Modal</strong></h2> 
            </div>
            <div class="p-5 modal-body">
                Content..
            </div>
            <div class="px-5 py-3 text-right border-t border-gray-200 modal-footer"> 
                <button type="button" class="btn btn-outline-secondary w-20 mr-1" data-dismiss="modal">Close</button> 
                <button type="submit" class="btn btn-primary w-20">Save</button> 
            </div>
        </div>
    </div>
</div>