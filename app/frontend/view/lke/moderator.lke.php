<?php $this->getHeader() ?>
<div class="wrapper">
    <div id="lke" class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <?= $breadcrumb ?>
                    </div>
                    <h4 class="page-title"><?= $page_title ?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Satuan Kerja</th>
                            <th>Komponen</th>
                            <th>Status</th>
                            <th width="100px">Detail</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($table_verifikasi as $key => $value): ?>
                            <tr>
                                <td><?= $value['nama_satker'] ?></td>
                                <td><?= $value['nama_komponen'] ?></td>
                                <td><span class="badge badge-<?= $value['status_verifikasi_color'] ?>"><?= $value['status_verifikasi_text'] ?></span></td>
                                <td><a href="<?= $this->getActiveUrl().'/survey/'.$value['id_komponen'].'/satker/'.$value['id_satker'] ?>" target="_blank" class="btn btn-sm btn-custom"><i class="mdi mdi-eye"></i> Detail</a></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <?php //$this->showArray($table_verifikasi) ?>
            </div>
        </div> <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->
<?php $this->getFooter() ?>