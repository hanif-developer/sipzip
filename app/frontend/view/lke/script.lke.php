if ($("#lke").length > 0) {
    const path_url = "<?= $this->createLink(['lke']) ?>";
	const lke = new Frameduz("#lke");
    const m_documents = lke.createDialog("#modal-documents");
    // console.log(m_documents);

    const loadTableDokumen = function(pertanyaan) {
        m_documents.footer.find("#tabel-upload").html("");
        $.get(path_url+"/survey/document/"+pertanyaan, function(data) {
            m_documents.footer.find("#tabel-upload").html(data);
        });
    }
    
	form_upload = lke.modul.find("#form-upload");
    // console.log(form_upload);
    form_upload.on("submit", function(e) {
        e.preventDefault();
        const params = $(this)[0];
        const progress = lke.createProgress(m_documents.content);
        const pertanyaan = form_upload.find("#pertanyaan_id").val();
        // console.log(params);
        setTimeout(function(){
            lke.sendMultipart("POST", {
                url: api_url + "/survey/upload",
                data: params,
                header: requestHeader,
                onSuccess: function(response) {
                    // console.log(response);
                    loadTableDokumen(pertanyaan);
                    progress.remove();
                    $.toast({
                        heading: "Sukses",
                        text: response.message,
                        position: "top-right",
                        loaderBg: "#3b98b5",
                        icon: "info",
                        hideAfter: 2000,
                        stack: 1
                    });
                },
                onError: function(error) {
                    // console.log(error);
                    progress.remove();
                    swal({
                        type: "error",
                        text: error.message,
                        confirmButtonClass: 'btn btn-confirm mt-2'
                    });
                }
            });
        }, 1000);
        return false;
    });

	form_survey = lke.modul.find("#form-survey");
    // console.log(form_survey);
	form_survey.on("submit", function(e) {
		e.preventDefault();
		const params = $(this).serialize();
		const text = "Lembar Kerja Evaluasi "+$(this).find("#nama_komponen").val();
        swal({
            title: "Kirim Survey",
            text: text,
            showCancelButton: true,
            confirmButtonText: 'Kirim',
            showLoaderOnConfirm: true,
            confirmButtonClass: 'btn btn-confirm mt-2',
            cancelButtonClass: 'btn btn-cancel ml-2 mt-2',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    setTimeout(function(){
                        lke.sendRequest("POST", {
                            url: api_url + "/survey",
                            data: params,
                            header: requestHeader,
                            onSuccess: function(response) {
                                // console.log(response);
                                resolve(response);
                            },
                            onError: function(error) {
                                // console.log(error);
                                reject(error.message);
                            }
                        });
                    }, 1000);
                });
            },
            allowOutsideClick: false
        }).then(function (result) {
            // console.log(result);
            swal({
                type: "success",
                text: result.message,
                confirmButtonClass: 'btn btn-confirm mt-2'
            }).then(function() {
                // window.history.back();
                // window.location.reload();
                window.location = path_url;
            });
        })

        return false;
	});

    lke.modul.find(".btn-documents").on("click", function (e) {
		e.preventDefault();
        // console.log(this);
        const pertanyaan = this.getAttribute("data-pertanyaan");
        form_upload.find("#pertanyaan_id").val(pertanyaan);
        form_upload.find("#soal_survey").html(this.getAttribute("data-survey"));
        form_upload[0].reset();
        loadTableDokumen(pertanyaan);
        m_documents.modal("show");
	});	

    lke.modul.find(".btn-verifikasi").on("click", function (e) {
        e.preventDefault();
        const verifikasi = this.getAttribute("data-verifikasi");
        form_survey.find("#status_verifikasi").val(verifikasi);
        form_survey.submit();
        // console.log(verifikasi);
    });

    $("#datatable").DataTable();
    
	
}