<?php $this->getHeader() ?>
<div class="wrapper">
    <div id="lke" class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <?= $breadcrumb ?>
                    </div>
                    <h4 class="page-title"><?= $page_title ?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="header-title"><?= $data_lke['satker']['nama_satker'] ?></h4>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card-box mb-0 text-center">
                                <div class="float-rights">
                                    <!-- <input data-plugin="knob" data-width="100" data-height="100" data-linecap=round data-fgColor="#0acf97" value="<?= $data_lke['summary']['persentase_nilai'] ?>" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".1"/> -->
                                </div>
                                <div class="">
                                    <h4 class="text-muted mb-0 mt-2">Total Nilai ZI</h4>
                                    <h3 class="display-2"><?= $data_lke['nilai_zi'] ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card-box mb-0">
                                <?php foreach ($data_lke['summary'] as $summary): ?>
                                <div class="project-member mb-3">
                                    <h4 class="mr-3"><?= $summary['categori'] ?></h4>
                                </div>
                                <label class="">Point : <span class="text-custom"><?= $summary['point'] ?>/<?= $summary['point_kategori'] ?></span> point</label>
                                <div class="progress mb-1" style="height: 7px;">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?= $summary['persentase_point'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $summary['persentase_point'] ?>%;"></div>
                                </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div>
            </div>
        </div>

        <?php foreach ($data_lke['survey'] as $cat): ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="alert alert-primary bg-primary text-white border-0">
                        <h4><?= $cat['category'] ?></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($cat['data'] as $data): ?>
            <div class="col-md-4">
                <a href="<?= $this->getActiveUrl().'/survey/'.$data['id'] ?>" class="text-dark">
                    <div class="card-box project-box ribbon-box">
                        <div class="ribbon ribbon-custom"><?= $data['komponen'] ?></div>
                        <?php if ($data['verifikasi']) {
                            echo '<span class="badge badge-'.$data['verifikasi']['status_verifikasi_color'].'">'.$data['verifikasi']['status_verifikasi_text'].'</span>';
                        } ?>
                        <!-- <h4 class="mt-0 mb-3"><a href="" class="text-dark"><?= $data['komponen'] ?></a></h4> -->
                        <div class="project-member mb-3">
                            <h4 class="mr-3">Point : <?= $data['point'] ?>/<?= $data['point_komponen'] ?></h4>
                        </div>
                        <label class="">Menjawab : <span class="text-custom"><?= $data['terjawab'] ?>/<?= $data['jumlah_soal'] ?></span> soal</label>
                        <div class="progress mb-1" style="height: 7px;">
                            <div class="progress-bar" role="progressbar" aria-valuenow="<?= $data['persentase_terjawab'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $data['persentase_terjawab'] ?>%;">
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach ?>
        </div>
        <?php endforeach ?>
        <?php //$this->showArray($data_lke) ?>

    </div> <!-- end container -->
</div>
<!-- end wrapper -->
<?php $this->getFooter() ?>
