<table class="table table-sm mb-0">
    <thead>
        <tr>
            <th style="padding: 10px 25px !important; width: 50px;">#</th>
            <th style="padding: 10px 25px !important;">Lampiran</th>
        </tr>
    </thead>
    <tbody>
        <?php if (empty($dokumen)): ?>
        <tr>
            <td style="padding: 10px 25px !important; text-align: center;" colspan="2">Data masih kosong</td>
        </tr>
        <?php else: ?>
        <?php $number = 1; ?>
        <?php foreach ($dokumen as $key => $value): ?>
        <tr>
            <td style="padding: 10px 25px !important;"><?= $number++; ?></td>
            <td style="padding: 10px 25px !important;"><a href="<?= $this->getActiveUrl().'/download/'.$value['file'] ?>" target="_blank"><?= $value['name'] ?></a></td>
        </tr>
        <?php endforeach ?>
        <?php endif ?>
    </tbody>
</table>