<?php $this->getHeader() ?>
<div class="wrapper">
    <div id="lke" class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group float-right">
                        <?= $breadcrumb ?>
                    </div>
                    <h4 class="page-title"><?= $page_title ?></h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div id="modal-documents" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centereds">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="mySmallModalLabel">Dokumen Pendukung</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <form id="form-upload" class="form-horizontal" role="form" autocomplete="false">
                                    <div class="form-group">
                                        <label>File Pendukung</label>
                                        <p id="soal_survey" class="text-muted font-14 m-b-10"></p>
                                        <?= $this->html::inputKey('komponen_id', $data_survey['form']['id_komponen']) ?>
                                        <?= $this->html::inputKey('pertanyaan_id', '') ?>
                                        <input name="file_pendukung" type="file" class="form-control" required />
                                    </div>
                                    <button type="submit" class="btn btn-primary mb-2">Upload</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="justify-content: start; padding: 0;">
                        <div id="tabel-upload" class="table-responsive"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><div class="alert alert-custom bg-custom text-white border-0" role="alert"><?= strtoupper($data_survey['title']) ?></div></h4>
                    <div class="row">
                        <div class="col-12">
                            <form id="form-survey" class="form-horizontal" role="form" autocomplete="false">
                                <?= $this->html::inputKey('id_satker', $data_survey['form']['id_satker']) ?>
                                <?= $this->html::inputKey('id_komponen', $data_survey['form']['id_komponen']) ?>
                                <?= $this->html::inputKey('nama_komponen', $data_survey['form']['nama_komponen']) ?>
                                <?= $this->html::inputKey('status_verifikasi', $data_survey['form']['status_verifikasi']) ?>
                                <div class="p-20">
                                    <ol>
                                        <?php foreach ($data_survey['form']['survey'] as $form_survey): ?>
                                        <li>
                                            <strong><?= $form_survey['penilaian']['nama_penilaian'] ?> (<?= $form_survey['penilaian']['point_penilaian'] ?>)</strong>
                                            <ol class="mt-3">
                                                <?php foreach ($form_survey['survey'] as $survey): ?>
                                                <li>
                                                    <?= $survey['pertanyaan']['nama_pertanyaan'] ?>
                                                    <div class="m-3">
                                                        <?php if (empty($survey['pilihan_jawaban'])): ?>
                                                            <div class="col-md-5">
                                                                <input id="soal[<?= $survey['pertanyaan']['id_pertanyaan'] ?>]" name="soal[<?= $survey['pertanyaan']['id_pertanyaan'] ?>]" type="number" step="any" class="form-control" value="<?= $survey['hasil_survey']['jawaban_id'] ?>">
                                                                <div class="alert alert-info mt-3" role="alert">
                                                                    <strong>Catatan:</strong> gunakan titik (.), untuk bilangan desimal.
                                                                </div>
                                                            </div>
                                                        <?php endif ?>
                                                        <?php foreach ($survey['pilihan_jawaban'] as $pilihan): ?>
                                                            <?php $checked = ($pilihan['id_jawaban'] == $survey['hasil_survey']['jawaban_id']) ? 'checked' : ''; ?>
                                                            <div class="custom-control custom-radio" style="display: inline; margin-right:1rem;">
                                                            <input type="radio" id="<?= $pilihan['id_jawaban'] ?>" name="soal[<?= $survey['pertanyaan']['id_pertanyaan'] ?>]" value="<?= $pilihan['id_jawaban'] ?>" <?= $checked ?> <?= $data_survey['form']['status_komponen'] ?> class="custom-control-input">
                                                            <label class="custom-control-label" for="<?= $pilihan['id_jawaban'] ?>"><?= $pilihan['nama_jawaban'] ?></label>
                                                        </div>
                                                        <?php endforeach ?>
                                                    </div>

                                                    <?php if (!empty($survey['pilihan_jawaban'])): ?>
                                                    <div class="m-3">
                                                        <div class="alert alert-success" role="alert">
                                                            <strong>Informasi Tambahan:</strong><br>
                                                            <?php foreach ($survey['pilihan_jawaban'] as $pilihan): ?>
                                                            <?= $pilihan['keterangan_jawaban'] ?><br>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                    <?php endif ?>

                                                    <div class="m-3">
                                                        <button type="button" class="btn btn-info waves-effect waves-light btn-documents" data-pertanyaan="<?= $survey['pertanyaan']['id_pertanyaan'] ?>" data-survey="<?= $survey['pertanyaan']['nama_pertanyaan'] ?>"> <i class="fa fa-paperclip m-r-5"></i> <span>Dokumen Pendukung</span> </button>
                                                        <?php if ($data_survey['form']['status_verifikasi'] == 'revision'): ?>
                                                        <div class="alert alert-danger mt-3" role="alert">
                                                            <strong>Catatan Revisi:</strong><br>
                                                            <?= $survey['hasil_survey']['catatan_revisi'] ?>
                                                        </div>
                                                        <?php endif ?>
                                                    </div>
                                                </li>
                                                <?php endforeach ?>
                                            </ol>
                                            <?php //$this->showArray($form_survey) ?>
                                        </li>
                                        <?php endforeach ?>
                                    </ol>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end row -->

                </div> <!-- end card-box -->
            </div><!-- end col -->
        </div>

        <div class="row">
            <div class="col-12">
                <div class="pull-left m-b-30">
                    <a href="javascript:history.back()" class="btn btn-danger btn-lg waves-effect waves-light"><i class="mdi mdi-arrow-left"></i> Kembali</a>
                </div>
                <div class="pull-right m-b-30">
                    <button type="submit" form="form-survey" class="btn btn-primary btn-lg waves-effect waves-light"><i class="mdi mdi-content-save"></i> Simpan</button>
                </div>
            </div>
        </div>
        <?php //$this->showArray($data_survey) ?>

    </div> <!-- end container -->
</div>
<!-- end wrapper -->
<?php $this->getFooter() ?>
