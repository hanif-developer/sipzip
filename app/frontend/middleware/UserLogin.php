<?php 
namespace app\frontend\middleware;
use core\{WebApp, Helper};
use oauth\{UserAuth};
use core\Middleware;
use app\backend\service\{
    SipzipService,
};

class UserLogin extends WebApp implements Middleware {

    public function __construct($session) {
        parent::__construct();
        $this->session = $session;
        // $this->showArray($this->session);die;

        // Load Service
        $this->sipzipService = new SipzipService((array) $this->session->data);
        $this->data['pilihan_satker'] = ['' => ['text' => '-- Pilih Satker --']] + $this->sipzipService->tbSatker->getPilihanSatker();
        // $this->showArray($this->data);die;
    }

    public function before() {
        if (!$this->session->isValid) {
            $this->showView([
                'view' => 'login', 
                'data' => $this->data, 
                'template' => 'highdmin', 
                'body' => 'class="login center-element"',
                'controller' => 'main', 
                'script' => [
                    '/main/config',
                    '/main/script',
                ]
            ]);
            die;
        }
    }

    public function after() {
        // echo 'after';
    }

}
?>