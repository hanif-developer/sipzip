<?php 
namespace app\frontend\controller;
use core\{WebApp, Helper, SSH2};
use oauth\{UserAuth};
use app\frontend\middleware\UserLogin;
use app\backend\service\{
    SipzipService,
};

class main extends WebApp {

    public function __construct() {
        parent::__construct();
        $this->auth = UserAuth::getInstance();
        $this->session = $this->auth->getSession();
        $this->template = 'highdmin';   
        $this->userLogin = new UserLogin($this->session);
        // $this->showArray($this->session);die;

        // Load Service
        $this->sipzipService = new SipzipService((array) $this->session->data);

        // // Generated Data
        // $result = $this->sipzipService->tbKategori->createData();
        // $result = $this->sipzipService->tbKomponen->createData();
        // $result = $this->sipzipService->tbPenilaian->createData();
        // $result = $this->sipzipService->tbPertanyaan->createData();
        // $result = $this->sipzipService->tbJawaban->createData();
        // $result = $this->sipzipService->tbSatker->createData();
        // $this->showArray($result);die;

        // Global Variable
        $this->api_url = $this->createLink(['api', 'v1']);
        $this->main_url = $this->createLink([$this->getProject(), $this->getClassName()]);
        $this->base_url = $this->createLink([$this->getProject(), $this->getController()]);
        $this->no_image = $this->createLink(['/upload/default/no-user-image.png']);
        
        // Default Value
        $this->data['page_title'] = 'Zona Integritas Polri';
		$this->data['breadcrumb'] = $this->getBreadcrumb(['Dashboard']);
		$this->data['page'] = 1; // default number page
		$this->data['size'] = 10; // default size contents
		$this->data['start_date'] = date('Y-m-d');
		$this->data['end_date'] = date('Y-m-d');
		$this->data['page_size'] = [
            '10' => ['text' => '10'],
            '50' => ['text' => '50'],
            '100' => ['text' => '100'],
        ];

        // Common Value
        $this->data['pilihan_satker'] = $this->sipzipService->tbSatker->getPilihanSatker();

        $this->requestMapping('GET', '/', function() {
            // // Grafik Nilai ZI
            // $this->data['grafik_nilai_zi'] = $this->sipzipService->getGrafikNilaiZI();
            // $this->showArray($this->data['grafik_nilai_zi']);die;
            // // echo 'class: '.$this->session->data->kategori_satker;die;

            $this->data['data_lke'] = $this->sipzipService->getDataLKE();
            // $this->showArray($this->data['data_lke']); die;
            
            $view = 'index';
            if ($this->getController() == 'lke' && $this->session->data->kategori_satker == 'admin') {
                $tableVerifikasi = $this->sipzipService->getTableVerifikasi();
                $this->data['table_verifikasi'] = $tableVerifikasi;
                // $this->showArray($tableVerifikasi);die;
                $view = 'moderator';
            }

            $this->showView([
                'view' => $view, 
                'data' => $this->data, 
                'template' => $this->template, 
                'body' => 'class="main"',
                'script' => [
                    $this->main_url.'/config',
                    // $this->base_url.'/socket',
                    $this->base_url.'/script',
                ]
            ]);
            // $this->showArray($this->session);
            // echo 'Cookie: '.isset($_COOKIE['UserAuth']);
            // $_SESSION['test'] = 'sasa';
            // session_destroy();
            
            // $this->showArray($_SESSION);
        // });
        }, '', [$this->userLogin]);

        $this->requestMapping('GET', '/forbidden', function() {
            $data = [
                'message' => 'ACCESS DENIED !!', 
                'code' => $code
            ];
            $this->showView([
                'view' => 'error', 
                'code' => $code, 
                'data' => $data, 
                'template' => $this->template, 
                'controller' => $this->getClassName(), 
            ]);
        });

        $this->requestMapping('GET', '/config', function() {
            $token = $this->session->token;
            $code = <<<EOF
            const api_url = "$this->api_url";
            const token = "$token";
            const requestHeader = {
                "Authorization" : "Bearer ${token}"
            };

            const showMessage = function(obj, data) {
                $(".alert-message").remove();
                // const alert = $("<div>").addClass("intro-y alert alert-message mt-2 mb-2 show");
                const alert = $("<div>").addClass("alert alert-message text-white border-0");
                if (data.status == "success") {
                    alert.addClass("alert-success bg-success");
                    alert.html("<i class='icon-info mr-2'></i>"+data.message);
                }
                else {
                    alert.addClass("alert-danger bg-danger");
                    alert.html("<i class='icon-close mr-2'></i>"+data.message);
                }
            
                obj.prepend(alert);
            };

            EOF;
            $this->showView(['code' => $code], Helper::$contentType['js']);
        });

        $this->requestMapping('GET', '/script', function() {
            $this->showView(['view' => 'script', 'data' => $this->data], Helper::$contentType['js']);
        });

        $this->requestMapping('GET', '/socket', function() {
            // $this->showView(['view' => 'socket', 'data' => $this->data, 'controller' => $this->getClassName()], Helper::$contentType['js']);
        });

        $this->requestMapping('GET', '/logout', function() {
            $this->auth->checkout();
            $this->redirect([$this->getProject()]);
        });
    }

    private function getClassName() {
        $path = explode('\\', __CLASS__);
        return end($path);
    }

    protected function getBreadcrumb(array $page_title) {
        $page_root = $this->settings->web['title'];
        $page_root = 'Application';
        array_unshift($page_title, $page_root);
        $last_index = count($page_title) - 1;
        $curr_index = 0;
        $page_title = array_map(function($v) use (&$curr_index, &$last_index) {
            if ($curr_index == $last_index) {
                $result = '<li class="breadcrumb-item'.$active.'">'.ucwords(strtolower($v)).'</li>';
            }
            else {
                $result = '<li class="breadcrumb-item"><a href="javascript:;">'.ucwords(strtolower($v)).'</a></li>';
            }
            
            $curr_index++;
            return $result;
        }, $page_title);
        
        return '<ol class="breadcrumb hide-phone p-0 m-0">'.implode('', $page_title).'</ol>';
    }

    public function getHeader() {
        $this->data['logo'] = $this->createLink(['/asset/images/ic_pkl.png']);
        $this->showView(['view' => 'header', 'data' => $this->data, 'controller' => $this->getClassName()]);
    }

    public function getFooter() {
        $this->showView(['view' => 'footer', 'data' => $this->data, 'controller' => $this->getClassName()]);
    }

    public function getModal() {
        $this->showView(['view' => 'modal', 'data' => $this->data, 'controller' => $this->getClassName()]);
    }

    public function showError(string $message, $code = 400) {
        $data = [
            'message' => 'Halaman '.$this->getActiveUrl().', tidak ditemukan', 
            'message' => $message, 
            'code' => $code
        ];
        $this->showView([
            'view' => 'error', 
            'code' => $code, 
            'data' => $data, 
            'template' => $this->template, 
            'controller' => $this->getClassName(), 
        ]);
    }

}

?>