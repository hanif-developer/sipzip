<?php 
namespace app\frontend\controller;
use core\exception\{NotFoundException};
use core\{Helper};

class lke extends main {

    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = 'Lembar Kerja Evaluasi';
        $this->data['breadcrumb'] = $this->getBreadcrumb(['Lembar Kerja Evaluasi']);
        // $this->showArray($this->session);die;

        // User
        $this->requestMapping('GET', '/survey/{id}', function($id) {
            $data_survey = $this->sipzipService->getForm('survey', $id);
            // $this->showArray($data_survey);die;
            $this->data['data_survey'] = $data_survey;
            $this->showView([
                'view' => 'survey', 
                'data' => $this->data, 
                'template' => $this->template, 
                'body' => 'class="main"',
                'script' => [
                    $this->main_url.'/config',
                    // $this->base_url.'/socket',
                    $this->base_url.'/script',
                ]
            ]);
        // });
        }, '', [$this->userLogin]);

        // Moderator
        $this->requestMapping('GET', '/satker/{id}', function($id) {
            $this->data['data_lke'] = $this->sipzipService->getDataLKE($id);
            $this->showView([
                'view' => 'satker', 
                'data' => $this->data, 
                'template' => $this->template, 
                'body' => 'class="main"',
                'script' => [
                    $this->main_url.'/config',
                    // $this->base_url.'/socket',
                    $this->base_url.'/script',
                ]
            ]);
        // });
        }, '', [$this->userLogin]);

        // Moderator
        $this->requestMapping('GET', '/satker/{satker}/survey/{id}', function($satker, $id) {
            $data_survey = $this->sipzipService->getForm('survey', $id, $satker);
            // $this->showArray($data_survey);die;
            $this->data['data_survey'] = $data_survey;
            $this->showView([
                'view' => 'survey-view', 
                'data' => $this->data, 
                'template' => $this->template, 
                'body' => 'class="main"',
                'script' => [
                    $this->main_url.'/config',
                    // $this->base_url.'/socket',
                    $this->base_url.'/script',
                ]
            ]);
        // });
        }, '', [$this->userLogin]);

        // Moderator
        $this->requestMapping('GET', '/survey/{id}/satker/{satker}', function($id, $satker) {
            $data_survey = $this->sipzipService->getForm('survey', $id, $satker);
            // $this->showArray($data_survey);die;
            $this->data['data_survey'] = $data_survey;
            $this->showView([
                'view' => 'survey-moderator', 
                'data' => $this->data, 
                'template' => $this->template, 
                'body' => 'class="main"',
                'script' => [
                    $this->main_url.'/config',
                    // $this->base_url.'/socket',
                    $this->base_url.'/script',
                ]
            ]);
        // });
        }, '', [$this->userLogin]);

        // User
        $this->requestMapping('GET', '/survey/document/{id}', function($id) {
            $dokumen = $this->getDocumentUpload($id);
            // $this->showArray($dokumen);die;
            $this->data['dokumen'] = $dokumen;
            $this->showView(['view' => 'table-upload', 'data' => $this->data]);
        }, '', [$this->userLogin]);

        // User
        $this->requestMapping('GET', '/survey/document/{id}/download/{file}', function($id, $file) {
            $path_upload = $this->getSettingList()['upload'];
            $dokumen = $this->getDocumentUpload($id);
            foreach ($dokumen as $key => $value) {
                if ($value['file'] == $file) {
                    $file_upload = ltrim($path_upload['path_upload'].$file, '/');
                    $this->downloadFile($file_upload, $value['name']);
                    break;
                }
            }
        });

        // Moderator
        $this->requestMapping('GET', '/survey/{comp}/satker/{satker}/document/{id}/download/{file}', function($comp, $satker, $id, $file) {
            $path_upload = $this->getSettingList()['upload'];
            $dokumen = $this->getDocumentUpload($id, $satker);
            foreach ($dokumen as $key => $value) {
                if ($value['file'] == $file) {
                    $file_upload = ltrim($path_upload['path_upload'].$file, '/');
                    $this->downloadFile($file_upload, $value['name']);
                    break;
                }
            }
        });
    }

    function getDocumentUpload($id, $satker = '') {
        $satker = empty($satker) ? $this->session->data->id_satker : $satker;
        $hasil_survey = $this->sipzipService->tbSurvey->getByFields(['satker_id' => $satker, 'pertanyaan_id' => $id]);
        $hasil_survey = !is_null($hasil_survey) ? $hasil_survey : $this->sipzipService->tbSurvey->getModel();
        $nama_dokumen = !empty($hasil_survey['nama_dokumen']) ? explode(',', $hasil_survey['nama_dokumen']) : [];
        $dokumen_pendukung = !empty($hasil_survey['dokumen_pendukung']) ? explode(',', $hasil_survey['dokumen_pendukung']) : [];
        
        $result = [];
        foreach ($nama_dokumen as $key => $value) {
            $file = isset($dokumen_pendukung[$key]) ? $dokumen_pendukung[$key] : '';
            array_push($result, [
                'name' => $value,
                'file' => $file,
            ]);
        }

        return $result;
    }

}

?>