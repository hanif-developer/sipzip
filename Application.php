<?php
/**
 * frameduzPHP v8
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 8.0.0
 * @Date		: 18 Maret 2021
 * @package 	: core system
 * @Description : Application.php
 */


class Application {
    
    public function __construct() {
        session_start();
        spl_autoload_register([$this, 'getResources']);
        $this->runController();
    }

    private function getResources($file) {
        if (file_exists($file = ROOT . str_ireplace('\\', '/', $file) . '.php')) {
            require_once $file;
        }
    }

    private function runController() {
        $this->Url = new core\Url();
        $PathController = $this->Url->getPathController();
		$PathController = 'app\\' . $PathController . '\\controller\\';
        $Controller = $this->Url->getControllerName();
		$ctrl = $PathController . $Controller;
		$ctrl = $PathController . $this->Url->getControllerName();
        if (class_exists($ctrl)) {
            try {
                $ctrl = $ctrl::getInstance();
                $ctrl->execute();
            } catch (core\exception\NotFoundException $err) {
                $ctrl = $ctrl::getInstance();
                $ctrl->showError($err->getMessage());
            } catch (core\exception\AuthException $err) {
                $ctrl = $ctrl::getInstance();
                // $ctrl->showError('Unauthorized: '.$err->getMessage(), 401); // Unauthorized
                $ctrl->showError($err->getMessage(), 401); // Unauthorized
            } catch (core\exception\ValidationException $err) {
                $ctrl = $ctrl::getInstance();
                // $ctrl->showError('Validation: '.$err->getMessage(), 400); // Bad Request
                $ctrl->showError($err->getMessage(), 400); // Bad Request
            } catch (core\exception\RequestException $err) {
                $ctrl = $ctrl::getInstance();
                // $ctrl->showError('Request: '.$err->getMessage(), 400); // Bad Request
                $ctrl->showError($err->getMessage(), 400); // Bad Request
            } catch (core\exception\DatabaseException $err) {
                // core\Helper::errorResponse('Database Connection: '.$err->getMessage(), 503); // Internal Server Error
                core\Helper::errorResponse($err->getMessage(), 503); // Internal Server Error
            } catch (\Throwable $err) {
                // echo $err->getMessage();
                core\Helper::errorResponse($err->getMessage(), 503); // Internal Server Error
            }
        }
        else {
            if ($Controller == 'oauth') {
                $this->Config = $this->Url->getConfig();
                $oauthConfig = $this->Config['oauth'];
                foreach ($oauthConfig as $key => $value) {
                    if ($this->Url->getReqUrl() == '/'.$this->Url->getController().$value['endpoint']) {
                        $service = 'oauth\\' . $value['service'];
                        if (class_exists($service)) {
                            $oauth = $service::getInstance();
                            $oauth->setValidate();
                        }
                        else {
                            echo 'service '.$service.' not found';
                        }
                    }
                }
            }
            else {
                // $ctrl = $PathController . $this->Url->getDefaultController();
                // $ctrl = $ctrl::getInstance();
                // $ctrl->showError('Controller: '.$Controller.' not found', 404); // Not Found

                // Masih error jika di project tersebut tidak ada controller default
                core\Helper::errorResponse('Resource Not Found', 404); // Internal Server Error
            }
        }
    }

}

new Application();
?>
