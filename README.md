# Web Aplikasi SIPZIP
## Asset:
- [File Resource](https://drive.google.com/drive/folders/1qDa4C9T0Ms8lfJeEj6CLW9X3O1UdTyI_?usp=sharing)
- [Mockup]()
## Sample Website
host: epzi.polri.go.id
user: poldametrojaya
pass: epzirbp1994

## Dokumen Pendukung: 
Tutorial Dashboard (https://www.youtube.com/watch?v=DYZX3YmDJP8)
Upload Data Pendukung LKE (https://www.youtube.com/watch?v=jkaxthfEXIs)
Form LKE (https://docs.google.com/spreadsheets/d/18qc25tCXmWBunR1qpKSGM84oPIRUU88M/edit?usp=drive_web&ouid=115951901001040039334&rtpof=true)

Reference Same Site Cookie (https://newbedev.com/php-setcookie-samesite-strict)
``

## Konfigurasi PHP
```sh
sudo apt-get install php-curl -y
sudo apt-get install php-mysql -y
sudo apt-get install php-gd -y
sudo systemctl reload apache2
```
## Konfigurasi Mysql
```sh
#Install Mysql Server
sudo apt-get install mysql-server

#Masuk ke mysql, dan ubah password root
sudo mysql -uroot

USE mysql;
SELECT user, host, plugin, authentication_string FROM user;
UPDATE user SET plugin = 'mysql_native_password' WHERE User='root';
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
FLUSH PRIVILEGES;
```

## Create Database
```sh
#via mysql-server
mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS db_sipzip CHARACTER SET utf8 COLLATE utf8_general_ci"

#via docker
docker exec -i mysql.server /usr/bin/mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS db_sipzip CHARACTER SET utf8 COLLATE utf8_general_ci"
```

## Backup Database
```sh
#via mysql-server
mysqldump -u root --password=root db_sipzip | sed -e 's/DEFINER=[^*]*\*/\*/'> asset/database.sql

#via docker
docker exec mysql.server /usr/bin/mysqldump -u root --password=root db_sipzip | sed -e 's/DEFINER=[^*]*\*/\*/'> asset/database.sql
```

## Restore Database
```sh
#via mysql-server
cat asset/database.sql | mysql -u root --password=root db_sipzip

#via docker
cat asset/database.sql | docker exec -i mysql.server /usr/bin/mysql -u root --password=root db_sipzip
```

## Create Vhost
```
pico /etc/apache2/sites-available/webapss-sipzip.conf

<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/webapss-sipzip
    <Directory /var/www/html/webapss-sipzip>
            Options Indexes FollowSymLinks
            AllowOverride All
            Order allow,deny
            Allow from all
            Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.webapss-sipzip.log
    CustomLog ${APACHE_LOG_DIR}/access.webapss-sipzip.log combined

</VirtualHost>

a2dissite 000-default.conf 
a2ensite webapss-sipzip.conf 
systemctl reload apache2

```

## Setting Folder Upload & Composer
```
sudo chown -R $USER:www-data .
sudo find upload -type d -exec chmod 770 {} \;
sudo find upload -type d -exec chmod g+s {} \;
sudo find upload -type f -exec chmod 660 {} \; 2>&1 | grep -v "Operation not permitted"

sudo find vendor -type d -exec chmod 770 {} \;
sudo find vendor -type d -exec chmod g+s {} \;
```

## Remove .DS_Store
```
find . -name ".DS_Store" -delete
```

## Remove History Shell & Mysql
```
rm ~/.bash_history
rm ~/.mysql_history
```

## Permanently authenticating with Git repositories
```sh
git config credential.helper store
git pull

ALTER TABLE `triwayat_kepangkatan_pegawai` AUTO_INCREMENT = 17704;

```